﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for AddNewUserWindow.xaml
    /// </summary>
    public partial class AddNewUserWindow : Window
    {
        UserService us = new UserService();
        ObservableCollection<User> users;
        public AddNewUserWindow(UserFormViewModel viewModel)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            users = viewModel.userList;
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try { 
                string userName = txtUserName.Text;
                string password = txtPassword.Text;
                string phoneNo = txtPhoneNo.Text;
                string email = txtEmail.Text;

                User newUser = new User(userName, password, phoneNo, email);
                us.addUser(newUser);

                users.Add(newUser);
                this.Close();
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            txtUserName.Clear();
            txtPassword.Clear();
            txtPhoneNo.Clear();
            txtEmail.Clear();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
