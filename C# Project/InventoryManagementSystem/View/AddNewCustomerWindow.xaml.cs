﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for AddNewCustomerWindow.xaml
    /// </summary>
    public partial class AddNewCustomerWindow : Window
    {
        /* //CustomerFormViewModel viewModel;
         ObservableCollection<Customer> customers;

         CustomerService cs = new CustomerService();

         public AddNewCustomerWindow(ObservableCollection<Customer> customerList)
         {
             InitializeComponent();
             customers = customerList;
         }
         */

        CustomerService cs = new CustomerService();        
        ObservableCollection<Customer> customers;
        public AddNewCustomerWindow(CustomerFormViewModel viewModel)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            customers = viewModel.customerList;
        }
        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            txtFirstName.Clear();
            txtLastName.Clear();
            txtPhoneNo.Clear();
            txtEmail.Clear();
            txtStreet.Clear();
            txtCity.Clear();
            txtProvince.Clear();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string firstName = txtFirstName.Text;
                string lastName = txtLastName.Text;
                string phoneNo = txtPhoneNo.Text;
                string email = txtEmail.Text;
                string street = txtStreet.Text;
                string city = txtCity.Text;
                string province = txtProvince.Text;

                Customer customer = new Customer(firstName, lastName, phoneNo, email, street, city, province);
                cs.addCustomer(customer);

                customers.Add(customer);
                this.Close();
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            //dgCustomer.ItemsSource = null;
            //dgCustomer.ItemsSource = customerList;
            //var bindingList = new BindingList<Customer>(customerList);
            //var source = new BindingSource(bindingList, null);
            //dgCustomer.ItemsSource = source;
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
