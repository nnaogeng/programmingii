﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>

    public partial class CustomerManagementWindow : Window
    {
        //public ObservableCollection<Customer> customerList = new ObservableCollection<Customer>();

        CustomerService cs = new CustomerService();

        CustomerFormViewModel customerFormViewModel = new CustomerFormViewModel();
        public CustomerManagementWindow()
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }

        private void customerManagementWindow_Loaded(object sender, RoutedEventArgs e)
        {           
            /* customerList = new ObservableCollection<Customer>(cs.queryCustomer());
             dgCustomer.ItemsSource = customerList; */
            //customerFormViewModel.customerList = cs.queryCustomer();
            customerFormViewModel.customerList = new ObservableCollection<Customer>(cs.queryCustomer());
            dgCustomer.SelectedIndex = customerFormViewModel.customerList.Count - 1;
            customerFormViewModel.selectedCustomer = (Customer)dgCustomer.SelectedItem;
            DataContext = customerFormViewModel;

            //customerFormViewModel.customerList = new ObservableCollection<Customer>(customerQuery);
            //customerFormViewModel.customer = (Customer)dgCustomer.SelectedItem;
            //DataContext = customerFormViewModel;
           /*try
            {
            
                    
            }
            catch (SystemException ex)  // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine("Database operation failed: " + ex.Message);
            }*/

            //dgCustomer.ItemsSource = customerList;

            //var customerBinding = new Binding();
            //customerBinding.Source = bindingSource;
            //dgCustomer.SetBinding(DataGrid.ItemsSourceProperty, customerBinding);
        }
        private void ButtonAddNewCustomer_Click(object sender, RoutedEventArgs e)
        {
            AddNewCustomerWindow addNewCustomerWindow = new AddNewCustomerWindow(customerFormViewModel);
            addNewCustomerWindow.ShowDialog();
        }

        private void ButtonUpdateCustomer_Click(object sender, RoutedEventArgs e)
        {
            try {
                int id = ((Customer)dgCustomer.SelectedItem).customerID;

                string firstName = txtFirstName.Text;
                string lastName = txtLastName.Text;
                string phoneNo = txtPhoneNo.Text;
                string email = txtEmail.Text;
                string street = txtStreet.Text;
                string city = txtCity.Text;
                string province = txtProvince.Text;

                Customer item = new Customer(firstName, lastName, phoneNo, email, street, city, province);
                cs.updateCustomer(id, item);
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
}

        private void ButtonDeleteCustomer_Click(object sender, RoutedEventArgs e)
        {
            int id = ((Customer)dgCustomer.SelectedItem).customerID;
            cs.deleteCustomer(id);

            if (dgCustomer.SelectedItem != null)
            {
                int index = dgCustomer.SelectedIndex;
                customerFormViewModel.customerList.Remove(dgCustomer.SelectedItem as Customer);
                dgCustomer.SelectedIndex = index - 1;
            }
        }

        private void dgCustomer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            customerFormViewModel.selectedCustomer = (Customer)dgCustomer.SelectedItem;
            txtFirstName.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            txtFirstName.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            txtLastName.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            txtPhoneNo.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            txtEmail.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            txtStreet.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            txtCity.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            txtProvince.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
        }
    }
}
    
     