﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for UserManagementWindow.xaml
    /// </summary>
    public partial class UserManagementWindow : Window
    {
        InventoryDBContext ctx = new InventoryDBContext();
        UserFormViewModel userFormViewModel = new UserFormViewModel();
        UserService us = new UserService();

        //public ObservableCollection<User> userList = new ObservableCollection<User>();
        public UserManagementWindow()
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }

        private void userManagementWindow_Loaded(object sender, RoutedEventArgs e)
        {            
            //var userQuery = (from user in ctx.Users select user).ToList<User>();  
            //var userQuery = ctx.Users.Select(u => u).ToList();            

            //userList = new ObservableCollection<User>(userQuery);
            //dgUser.ItemsSource = userList;

            userFormViewModel.userList = new ObservableCollection<User>(us.queryUser());
            dgUser.SelectedIndex = userFormViewModel.userList.Count - 1;
            userFormViewModel.selectedUser = (User)dgUser.SelectedItem;
            DataContext = userFormViewModel;
        }

        private void ButtonAddNewUser_Click(object sender, RoutedEventArgs e)
        {
            AddNewUserWindow userWindow = new AddNewUserWindow(userFormViewModel);
            userWindow.ShowDialog();
        }

        private void ButtonUpdateUsr_Click(object sender, RoutedEventArgs e)
        {
            try {
                int id = ((User)dgUser.SelectedItem).userID;

                string userName = txtUserName.Text;
                string password = txtPassword.Text;
                string phoneNo = txtPhoneNo.Text;
                string email = txtEmail.Text;

                User item = new User(userName, password, phoneNo, email);
                us.updateUser(id, item);
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
}

        private void ButtonDeleteUser_Click(object sender, RoutedEventArgs e)
        {
            int id = ((User)dgUser.SelectedItem).userID;
            us.deleteUser(id);

            if (dgUser.SelectedItem != null)
            {
                int index = dgUser.SelectedIndex;
                userFormViewModel.userList.Remove(dgUser.SelectedItem as User);
                dgUser.SelectedIndex = index - 1;
            }
        }

        private void dgUser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {                      
            userFormViewModel.selectedUser = (User)dgUser.SelectedItem;
            txtUserName.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            txtPassword.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            txtPhoneNo.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            txtEmail.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
        }
    }
}
