﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        UserService us = new UserService();
        public LoginWindow()
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }

        private void buttonLogin_Click(object sender, RoutedEventArgs e)
        {
            string userName = txtUserName.Text;
            string password = txtPassword.Text;

            if (us.queryUserByUserName(userName).Equals(password))
            {
                MainWindow mainWindow = new MainWindow(userName);
                mainWindow.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("User name and password doesn't match. Please try again.","Warning",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }
    }
}
