﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for OrderManagementWindow.xaml
    /// </summary>
    /// 
    
    public partial class OrderManagementWindow : Window
    {
        OrderService os = new OrderService(); 
        OrderItemService ois = new OrderItemService();
        OrderFormViewModel orderFormViewModel = new OrderFormViewModel();

        public OrderManagementWindow()
        {
            InitializeComponent();
        }

        private void orderManagementWindow_Loaded(object sender, RoutedEventArgs e)
        {
            orderFormViewModel.orderList = new ObservableCollection<Order>(os.queryOrder());            
            orderFormViewModel.selectedOrder = (Order)dgOrder.SelectedItem;
            if (dgOrder.SelectedIndex != -1) {
                int orderId = orderFormViewModel.selectedOrder.orderID;
                if (orderId > 0)
                {
                    orderFormViewModel.orderItemList = new ObservableCollection<OrderItem>(ois.queryOrderItem(orderId));
                }
            }
            DataContext = orderFormViewModel;            
        }

        private void ButtonAddNewOrder_Click(object sender, RoutedEventArgs e)
        {
            //AddNewOrderWindow orderWindow = new AddNewOrderWindow();
            //orderWindow.ShowDialog();
        }

        private void ButtonUpdateOrder_Click(object sender, RoutedEventArgs e)
        {
            int id = ((Order)dgOrder.SelectedItem).orderID;

            string orderDate = txtOrderDate.Text;
            string customerID = txtCustomer.Text;
            
            //Order item = new Order();
            //os.updateOrder(id, item);
        }

        private void ButtonDeleteOrder_Click(object sender, RoutedEventArgs e)
        {
            int id = ((Order)dgOrder.SelectedItem).orderID;
            os.deleteOrder(id);

            if (dgOrder.SelectedItem != null)
            {
                int index = dgOrder.SelectedIndex;
                orderFormViewModel.orderList.Remove(dgOrder.SelectedItem as Order);
                dgOrder.SelectedIndex = index - 1;
            }
        }

        private void dgOrder_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            orderFormViewModel.selectedOrder = (Order)dgOrder.SelectedItem;
            txtOrderID.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            txtCustomer.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            txtOrderDate.GetBindingExpression(TextBox.TextProperty).UpdateTarget();

            if (dgOrder.SelectedIndex != -1)
            {
                int orderId = orderFormViewModel.selectedOrder.orderID;
                if (orderId > 0)
                {
                    orderFormViewModel.orderItemList = new ObservableCollection<OrderItem>(ois.queryOrderItem(orderId));
                }
                dgOrderItem.GetBindingExpression(DataGrid.ItemsSourceProperty).UpdateTarget();
            }
        }
    }
}
