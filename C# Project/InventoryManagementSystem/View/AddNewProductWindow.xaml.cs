﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for NewProductWindow.xaml
    /// </summary>
    public partial class AddNewProductWindow : Window
    {
        ProductService ps = new ProductService();      
        ObservableCollection<Product> products;
        public AddNewProductWindow(ProductFormViewModel viewModel)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            products = viewModel.productList;
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string productName = txtProductName.Text;
                string brand = txtBrand.Text;
                string modelNumber = txtModelNumber.Text;
                string description = txtDescription.Text;
                double price = Double.Parse(txtPrice.Text);
                int quantity = Int32.Parse(txtStockQuantity.Text);

                Product newProduct = new Product(productName, brand, modelNumber, quantity, price, description);
                ps.addProduct(newProduct);
                products.Add(newProduct);
                this.Close();
            }
            
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (FormatException)
            {
                MessageBox.Show("Quantity and Price must be numbers.", "Warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            txtProductName.Clear();
            txtBrand.Clear();
            txtModelNumber.Clear();
            txtDescription.Clear();
            txtPrice.Clear();
            txtStockQuantity.Clear();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
