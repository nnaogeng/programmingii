﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for ProductManagementWindow.xaml
    /// </summary>
    public partial class ProductManagementWindow : Window
    {
        ProductService ps = new ProductService();

        ProductFormViewModel productFormViewModel = new ProductFormViewModel();
        public ProductManagementWindow()
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            DataContext = productFormViewModel;
        }

        private void ProductManegementWindow_Loaded(object sender, RoutedEventArgs e)
        {
            productFormViewModel.productList = new ObservableCollection<Product>(ps.queryProduct());          
            dgProduct.SelectedIndex = productFormViewModel.productList.Count - 1;
            productFormViewModel.selectedProduct = (Product)dgProduct.SelectedItem;
            
        }
        private void ButtonAddNewProduct_Click(object sender, RoutedEventArgs e)
        {
            AddNewProductWindow productWindow = new AddNewProductWindow(productFormViewModel);
            productWindow.ShowDialog();
        }

        private void ButtonDeleteoProduct_Click(object sender, RoutedEventArgs e)
        {
            int id = ((Product)dgProduct.SelectedItem).productID;
            ps.deleteProduct(id);

            if (dgProduct.SelectedItem != null)
            {
                int index = dgProduct.SelectedIndex;
                productFormViewModel.productList.Remove(dgProduct.SelectedItem as Product);
                dgProduct.SelectedIndex = index - 1;
            }
        }

        private void ButtonUpdateProduct_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int id = ((Product)dgProduct.SelectedItem).productID;

                string productName = txtProductName.Text;
                string brand = txtBrand.Text;
                string modelNumber = txtModelNumber.Text;
                int quantity = Int32.Parse(txtStockQuantity.Text);
                double price = Double.Parse(txtPrice.Text);
                string description = txtDescription.Text;

                Product item = new Product(productName, brand, modelNumber, quantity, price, description);
                ps.updateProduct(id, item);
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (FormatException)
            {
                MessageBox.Show("Quantity and Price must be numbers.", "Warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void dgProduct_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            productFormViewModel.selectedProduct = (Product)dgProduct.SelectedItem;
            txtProductName.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            txtDescription.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            txtPrice.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            txtStockQuantity.GetBindingExpression(TextBox.TextProperty).UpdateTarget();                     
        }
    }
}
