﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow(string userName)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            if (!userName.Equals("Administrator"))
            {
                menuItem_User.IsEnabled = false;
            }
        }

        private void MenuCustomer_Click(object sender, RoutedEventArgs e)
        {
            CustomerManagementWindow customerWindow = new CustomerManagementWindow();
            customerWindow.ShowDialog();
        }

        private void MenuProduct_Click(object sender, RoutedEventArgs e)
        {
            ProductManagementWindow productWindow = new ProductManagementWindow();
            productWindow.ShowDialog();
        }

        private void MenuUser_Click(object sender, RoutedEventArgs e)
        {
            UserManagementWindow userWindow = new UserManagementWindow();
            userWindow.ShowDialog();
        }

        private void MenuOrder_Click(object sender, RoutedEventArgs e)
        {
            OrderManagementWindow orderWindow = new OrderManagementWindow();
            orderWindow.ShowDialog();
        }
    }
}
