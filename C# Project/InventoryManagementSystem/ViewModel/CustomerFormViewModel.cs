﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagementSystem
{    
    public class CustomerFormViewModel : INotifyPropertyChanged
    {
        private Customer _selectedCustomer;
        public Customer selectedCustomer
        {
            get
            {
                return _selectedCustomer;
            }
            set
            {
                if (this._selectedCustomer != value)
                {
                    this._selectedCustomer = value;
                    this.NotifyPropertyChanged("selectedCustomer");
                }
            }
        }

        private ObservableCollection<Customer> _customerList;
        public ObservableCollection<Customer> customerList
        {
            get
            {
                return _customerList;
            }
            set
            {
                if (this._customerList != value)
                {
                    this._customerList = value;
                    this.NotifyPropertyChanged("customerList");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string changedProperty)
        {
            if (PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(changedProperty));
            }
        }
    }
}
