﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagementSystem
{    
    public class UserFormViewModel : INotifyPropertyChanged
    {
        private User _selectedUser;
        public User selectedUser
        {
            get
            {
                return _selectedUser;
            }
            set
            {
                if (this._selectedUser != value)
                {
                    this._selectedUser = value;
                    this.NotifyPropertyChanged("selectedUser");
                }
            }
        }

        private ObservableCollection<User> _userList;
        public ObservableCollection<User> userList
        {
            get
            {
                return _userList;
            }
            set
            {
                if (this._userList != value)
                {
                    this._userList = value;
                    this.NotifyPropertyChanged("userList");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string changedProperty)
        {
            if (PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(changedProperty));
            }
        }
    }
}
