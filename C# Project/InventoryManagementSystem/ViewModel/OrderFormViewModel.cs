﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagementSystem
{
    class OrderFormViewModel
    {
        private Order _selectedOrder;
        public Order selectedOrder
        {
            get
            {
                return _selectedOrder;
            }
            set
            {
                if (this._selectedOrder != value)
                {
                    this._selectedOrder = value;
                    this.NotifyPropertyChanged("selectedOrder");
                }
            }
        }

        private ObservableCollection<Order> _orderList;
        public ObservableCollection<Order> orderList
        {
            get
            {
                return _orderList;
            }
            set
            {
                if (this._orderList != value)
                {
                    this._orderList = value;
                    this.NotifyPropertyChanged("orderList");
                }
            }
        }

        private ObservableCollection<OrderItem> _orderItemList;
        public ObservableCollection<OrderItem> orderItemList
        {
            get
            {
                return _orderItemList;
            }
            set
            {
                if (this._orderItemList != value)
                {
                    this._orderItemList = value;
                    this.NotifyPropertyChanged("orderItemList");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string changedProperty)
        {
            if (PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(changedProperty));
            }
        }
    }
}
