﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagementSystem
{
    public class ProductFormViewModel : INotifyPropertyChanged
    {
        private Product _selectedProduct;
        public Product selectedProduct
        {
            get
            {
                return _selectedProduct;
            }
            set
            {
                if (this._selectedProduct != value)
                {
                    this._selectedProduct = value;
                    this.NotifyPropertyChanged("selectedProduct");
                }
            }
        }

        private ObservableCollection<Product> _productList;
        public ObservableCollection<Product> productList
        {
            get
            {
                return _productList;
            }
            set
            {
                if (this._productList != value)
                {
                    this._productList = value;
                    this.NotifyPropertyChanged("productList");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string changedProperty)
        {
            if (PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(changedProperty));
            }
        }
    }
}
