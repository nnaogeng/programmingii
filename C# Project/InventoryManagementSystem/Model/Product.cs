﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace InventoryManagementSystem
{
    public class Product : INotifyPropertyChanged
    {
        [Key]
        public int productID { get; set; }

        private string _productName;
        [MaxLength(30)]
        public string productName
        {
            get { return _productName; }
            set
            {
                if (this._productName != value)
                {
                    this._productName = value;
                    this.NotifyPropertyChanged("productName");
                }
                if(value.Equals(""))
                {                   
                    throw new InvalidOperationException("Product Name must not be empty!");
                }
            }
        }

        private string _brand;
        [MaxLength(15)]
        public string brand
        {
            get { return _brand; }
            set
            {
                if (this._brand != value)
                {
                    this._brand = value;
                    this.NotifyPropertyChanged("brand");
                }
            }
        }

        private string _modelNumber;
        [MaxLength(20)]
        public string modelNumber
        {
            get { return _modelNumber; }
            set
            {
                if (this._modelNumber != value)
                {
                    this._modelNumber = value;
                    this.NotifyPropertyChanged("modelNumber");
                }
            }
        }

        private int _quantity;
        public int quantity
        {
            get { return _quantity; }
            set
            {
                if (value.Equals(""))
                {
                    throw new InvalidOperationException("Quantity must not be empty!");
                }
                if (this._quantity != value)
                {
                    this._quantity = value;
                    this.NotifyPropertyChanged("quantity");
                }
            }
        }

        private double _price;
        public double price
        {
            get { return _price; }
            set
            {
                if (value.Equals(""))
                {
                    throw new InvalidOperationException("Price must not be empty!");
                }
                if (this._price != value)
                {
                    this._price = value;
                    this.NotifyPropertyChanged("price");
                }
            }
        }

     /*   
        private byte[] _image;

        [Column(TypeName = "image")]
        public byte[] image
        {
            get { return _image; }
            set
            {
                if (this._image != value)
                {
                    this._image = value;
                    this.NotifyPropertyChanged("image");
                }
            }
        }
*/
        private string _description;
        [MaxLength(200)]
        public string description
        {
            get { return _description; }
            set
            {
                if (this._description != value)
                {
                    this._description = value;
                    this.NotifyPropertyChanged("description");
                }
            }
        }
     
        public Product() { }
        public Product(string productName, string brand, string modelNumber, int quantity, double price, string description)
        {            
            this.productName = productName;
            this.brand = brand;
            this.modelNumber = modelNumber;
            this.quantity = quantity;
            this.price = price;
            this.description = description;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string changedProperty)
        {
            if (PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(changedProperty));
            }
        }
    }
}
