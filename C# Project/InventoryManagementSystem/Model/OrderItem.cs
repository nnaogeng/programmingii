﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagementSystem
{
    public class OrderItem : INotifyPropertyChanged
    {
        public int orderItemID { get; set; }

/*
        private Order _order;
        public Order order
        {
            get { return _order; }
            set
            {
                if (this._order != value)
                {
                    this._order = value;
                    this.NotifyPropertyChanged("order");
                }
            }
        }
*/
     
        private int _orderID;
        public int orderID
        {
            get { return _orderID; }
            set
            {
                if (this._orderID != value)
                {
                    this._orderID = value;
                    this.NotifyPropertyChanged("orderID");
                }
            }
        }
       
        private int _quantity;
        public int quantity
        {
            get { return _quantity; }
            set
            {
                if (this._quantity != value)
                {
                    this._quantity = value;
                    this.NotifyPropertyChanged("quantity");
                }
            }
        }

        /*        private Product _product;        
                public virtual Product product
                {
                    get { return _product; }
                    set
                    {
                        if (this._product != value)
                        {
                            this._product = value;
                            this.NotifyPropertyChanged("product");
                        }
                    }
                }
        */
 //       public virtual Product product { get; set; }
        private int _productID;
        public int productID
        {
            get { return _productID; }
            set
            {
                if (this._productID != value)
                {
                    this._productID = value;
                    this.NotifyPropertyChanged("productID");
                }
            }
        }

/*               private string _productName;
                [MaxLength(40)]
                public string productName
                {
                    get { return _productName; }
                    set
                    {
                        if (this._productName != value)
                        {
                            this._productName = value;
                            this.NotifyPropertyChanged("productName");
                        }
                    }
                }        

                private int _unitPrice;
                public int unitPrice
                {
                    get { return _unitPrice; }
                    set
                    {
                        if (this._unitPrice != value)
                        {
                            this._unitPrice = value;
                            this.NotifyPropertyChanged("unitPrice");
                        }
                    }
                }

                private string _subTotal;
                public string subTotal
                {
                    get { return _subTotal; }
                    set
                    {
                        if (this._subTotal != value)
                        {
                            this._subTotal = value;
                            this.NotifyPropertyChanged("subTotal");
                        }
                    }
                }
        */
        public OrderItem() { }
        public OrderItem(int orderItemID, int orderID, int quantity, int productID)
        {
            this.orderItemID = orderItemID;
            this.orderID = orderID;
            this.quantity = quantity;
            this.productID = productID;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string changedProperty)
        {
            if (PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(changedProperty));
            }
        }
    }
}
