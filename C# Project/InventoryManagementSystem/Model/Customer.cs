﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagementSystem
{
    public class Customer : INotifyPropertyChanged
    {
        [Key]
        public int customerID { get; set; }

        private string _firstName;
        [MaxLength(20)]
        public string firstName 
        {
            get { return _firstName; } 
            set
            {
                if (this._firstName != value)
                {
                    this._firstName = value;
                    this.NotifyPropertyChanged("firstName");
                }
                if (value.Equals(""))
                {
                    throw new InvalidOperationException("First Name must not be empty!");
                }
            }
        }

        private string _lastName;
        [MaxLength(20)]
        public string lastName
        {
            get { return _lastName; }
            set
            {
                if (this._lastName != value)
                {
                    this._lastName = value;
                    this.NotifyPropertyChanged("lastName");
                }
                if (value.Equals(""))
                {
                    throw new InvalidOperationException("Last Name must not be empty!");
                }
            }
        }

        private string _phoneNo;
        [MaxLength(15)]
        public string phoneNo
        {
            get { return _phoneNo; }
            set
            {
                if (this._phoneNo != value)
                {
                    this._phoneNo = value;
                    this.NotifyPropertyChanged("phoneNo");
                }
            }
        }

        private string _email;
        public string email
        {
            get { return _email; }
            set
            {
                if (this._email != value)
                {
                    this._email = value;
                    this.NotifyPropertyChanged("email");
                }
            }
        }

        private string _street;
        [MaxLength(60)]
        public string street
{
            get { return _street; }
            set
            {
                if (this._street != value)
                {
            this._street = value;
            this.NotifyPropertyChanged("street");
                }
            }
        }

        private string _city;
        [MaxLength(40)]
        public string city
        {
            get { return _city; }
            set
            {
                if (this._city != value)
                {
                    this._city = value;
                    this.NotifyPropertyChanged("city");
                }
            }
        }

        private string _province;
        [MaxLength(20)]
        public string province
        {
            get { return _province; }
            set
            {
                if (this._province != value)
                {
                    this._province = value;
                    this.NotifyPropertyChanged("province");
                }
            }
        }

        public Customer() { }

        public Customer(string firstName, string lastName, string phoneNo, string email, string street, string city, string province)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.phoneNo = phoneNo;
            this.email = email;
            this.street = street;
            this.city = city;
            this.province = province;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string changedProperty)
        {
            if (PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(changedProperty));
            }
        }
    }
}
