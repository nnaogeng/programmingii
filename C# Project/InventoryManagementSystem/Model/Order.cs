﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagementSystem
{
    public class Order : INotifyPropertyChanged
    {
        public int orderID { get; set; }

        private string _orderDate;
        public string orderDate
        {
            get { return _orderDate; }
            set
            {
                if (this._orderDate != value)
                {
                    this._orderDate = value;
                    this.NotifyPropertyChanged("orderDate");
                }
            }
        }

//        public virtual Customer customer { get; set; }
        private int _customerID;
        public int customerID
        {
            get { return _customerID; }
            set
            {
                if (this._customerID != value)
                {
                    this._customerID = value;
                    this.NotifyPropertyChanged("customerID");
                }
            }
        }
        /*        private Customer _customer;
                public Customer customer
                {
                    get { return _customer; }
                    set
                    {
                        if (this._customer != value)
                        {
                            this._customer = value;
                            this.NotifyPropertyChanged("customer");
                        }
                    }
                }
        */
/*
        public virtual ObservableCollection<OrderItem> orderItems
        {
            get; set;
        }
*/
        public Order() { }
        public Order(int orderID, string orderDate, int customerID)
        {
            this.orderID = orderID;
            this.orderDate = orderDate;
            this.customerID = customerID;
        }


        /*   private string _customerName;
           [MaxLength(40)]
           public string customerName
           {
               get { return _customerName; }
               set
               {
                   if (this._customerName != value)
                   {
                       this._customerName = value;
                       this.NotifyPropertyChanged("customerName");
                   }
               }
           }
  
        private string _totalAmount;
        public string totalAmount
        {
            get { return _totalAmount; }
            set
            {
                if (this._totalAmount != value)
                {
                    this._totalAmount = value;
                    this.NotifyPropertyChanged("totalAmount");
                }
            }
        }    
 */
        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string changedProperty)
        {
            if (PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(changedProperty));
            }
        }
    }
}
