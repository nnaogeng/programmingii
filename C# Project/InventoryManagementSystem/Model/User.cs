﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagementSystem 
{
    public class User : INotifyPropertyChanged
    {
        [Key]
        public int userID { get; set; }

        private string _userName;
        public string userName
        {
            get { return _userName; }
            set
            {
                if (this._userName != value)
                {
                    this._userName = value;
                    this.NotifyPropertyChanged("userName");
                }
                if (value.Equals(""))
                {
                    throw new InvalidOperationException("User Name must not be empty!");
                }
            }
        }

        private string _password;
        [MaxLength(15)]
        public string password
        {
            get { return _password; }
            set
            {
                if (this._password != value)
                {
                    this._password = value;
                    this.NotifyPropertyChanged("password");
                }
                if (value.Equals(""))
                {
                    throw new InvalidOperationException("Password must not be empty!");
                }
            }
        }

        private string _phoneNo;
        [MaxLength(15)]
        public string phoneNo
        {
            get { return _phoneNo; }
            set
            {
                if (this._phoneNo != value)
                {
                    this._phoneNo = value;
                    this.NotifyPropertyChanged("phoneNo");
                }
            }
        }

        private string _email;
        public string email
        {
            get { return _email; }
            set
            {
                if (this._email != value)
                {
                    this._email = value;
                    this.NotifyPropertyChanged("email");
                }
            }
        }

        public User() { }
        public User(string userName, string password, string phoneNo, string email)
        {
            this.userName = userName;
            this.password = password;
            this.phoneNo = phoneNo;
            this.email = email;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string changedProperty)
        {
            if (PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(changedProperty));
            }
        }
    }
}
