﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace InventoryManagementSystem
{
    class CustomerService
    {
        InventoryDBContext ctx = new InventoryDBContext();
        public void addCustomer(Customer customer)
        {
            ctx.Customers.Add(customer);
            ctx.SaveChanges();
            MessageBox.Show("Customer " + customer.firstName.ToString() + " " + customer.lastName.ToString() + " has been added successfully.",
                            "Add Customer", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public List<Customer> queryCustomer()
        {
            //var customerQuery = (from customer in ctx.Customers select customer).ToList<Customer>();  // equiv: SELECT * FROM Customers;
            var customerQuery = ctx.Customers.Select(c => c).ToList();
            return customerQuery;
        }

        public void deleteCustomer(int id)
        {
            Customer selectedCustomer = (Customer)ctx.Customers.Where(o => o.customerID == id).Select(c => c).FirstOrDefault();
            ctx.Customers.Remove(selectedCustomer);
            ctx.SaveChanges();
            MessageBox.Show("Customer " + selectedCustomer.firstName.ToString() + " " + selectedCustomer.lastName.ToString() + " has been deleted.",
                            "Delete Customer", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public void updateCustomer(int id, Customer customer)
        {
            Customer selectedCustomer = (Customer)ctx.Customers.Where(o => o.customerID == id).Select(c => c).FirstOrDefault();
            selectedCustomer.firstName = customer.firstName;
            selectedCustomer.lastName = customer.lastName;
            selectedCustomer.phoneNo = customer.phoneNo;
            selectedCustomer.email = customer.email;
            selectedCustomer.street = customer.street;
            selectedCustomer.city = customer.city;
            selectedCustomer.province = customer.province;
            ctx.SaveChanges();
            MessageBox.Show("Customer " + selectedCustomer.firstName.ToString() + " " + selectedCustomer.lastName.ToString() + " has been updated successfully.",
                            "Update Customer", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
