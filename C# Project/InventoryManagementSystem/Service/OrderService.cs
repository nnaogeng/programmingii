﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace InventoryManagementSystem
{
    public class OrderService
    {
        InventoryDBContext ctx = new InventoryDBContext();
        public void addOrder(Order order)
        {
            ctx.Orders.Add(order);
            ctx.SaveChanges();
            MessageBox.Show(order.orderID.ToString() + " has been added successfully.");
        }

        public List<Order> queryOrder()
        {
            //try
            //{
                var orderQuery = (from o in ctx.Orders select o).ToList();  // equiv: SELECT * FROM Orders;
                //var orderQuery = ctx.Orders.Select(c => c).ToList();
                
                //foreach (var item in orderQuery)
                //{
                //    MessageBox.Show(item.orderID.ToString() + "***" + item.customerID);                    
                //}
                return orderQuery;            
        }

        public void deleteOrder(int id)
        {
            Order selectedOrder = (Order)ctx.Orders.Where(o => o.orderID == id).Select(c => c).FirstOrDefault();
            ctx.Orders.Remove(selectedOrder);
            ctx.SaveChanges();
        }

        public void updateOrder(int id, Order order)
        {
            Order selectedOrder = (Order)ctx.Orders.Where(o => o.orderID == id).Select(c => c).FirstOrDefault();
            selectedOrder.orderDate = order.orderDate;            
            ctx.SaveChanges();
            MessageBox.Show(selectedOrder.orderID.ToString() + " has been updated successfully.");

        }
    }
}
