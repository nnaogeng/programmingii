﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace InventoryManagementSystem
{
    class UserService
    {
        InventoryDBContext ctx = new InventoryDBContext();
        public void addUser(User user)
        {
            ctx.Users.Add(user);
            ctx.SaveChanges();
            MessageBox.Show("User " + user.userName.ToString() + " has been added successfully.",
                            "Add User", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public string queryUserByUserName(string userName)
        {
            string  password = ctx.Users.Where( u => u.userName.Equals(userName)).Select(u => u.password).FirstOrDefault();
            return password;
        }
        public List<User> queryUser()
        {            
            var userQuery = ctx.Users.Select(u => u).ToList();
            return userQuery;
        }

        public void deleteUser(int id)
        {
            User selectedUser = (User)ctx.Users.Where(u => u.userID == id).Select(u => u).FirstOrDefault();
            ctx.Users.Remove(selectedUser);
            ctx.SaveChanges();
            MessageBox.Show("User " + selectedUser.userName.ToString() + " has been deleted.",
                            "Delete User", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public void updateUser(int id, User user)
        {
            User selectedUser = (User)ctx.Users.Where(u => u.userID == id).Select(u => u).FirstOrDefault();
            selectedUser.userName = user.userName;
            selectedUser.password = user.password;
            selectedUser.phoneNo = user.phoneNo;
            selectedUser.email = user.email;
            ctx.SaveChanges();
            MessageBox.Show("User " + selectedUser.userName.ToString() + " has been updated successfully.",
                            "Update User", MessageBoxButton.OK, MessageBoxImage.Information);

        }
    }
}
