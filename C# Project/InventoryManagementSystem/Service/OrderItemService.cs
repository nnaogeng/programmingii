﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagementSystem
{
    class OrderItemService
    {
        InventoryDBContext ctx = new InventoryDBContext();
        public List<OrderItem> queryOrderItem(int id)
        {            
            //var orderItemQuery = (from o in ctx.OrderItems where o.orderID == id select o).ToList();  // equiv: SELECT * FROM Orders;
            var orderItemQuery = ctx.OrderItems.Where(o => o.orderID == id).Select(o => o).ToList();            
            return orderItemQuery;
        }
    }
}
