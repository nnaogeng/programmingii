﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace InventoryManagementSystem
{
    class ProductService
    {
        InventoryDBContext ctx = new InventoryDBContext();
        public void addProduct(Product product)
        {
            ctx.Products.Add(product);
            ctx.SaveChanges();
            MessageBox.Show("Product " + product.productName.ToString() + " " + " has been added successfully.", 
                            "Add Product", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public List<Product> queryProduct()
        {               
                var productQuery = ctx.Products.Select(p => p).ToList();                                           
                return productQuery;                                    
        }

        public void deleteProduct(int id)
        {
            Product selectedProduct = (Product)ctx.Products.Where(o => o.productID == id).Select(c => c).FirstOrDefault();
            ctx.Products.Remove(selectedProduct);
            ctx.SaveChanges();
            MessageBox.Show("Product " + selectedProduct.productName.ToString() + " " + " has been deleted.",
                            "Delete Product", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public void updateProduct(int id, Product product)
        {
            Product selectedProduct = (Product)ctx.Products.Where(o => o.productID == id).Select(c => c).FirstOrDefault();
            selectedProduct.productName = product.productName;
            selectedProduct.brand= product.brand;
            selectedProduct.modelNumber = product.modelNumber;
            selectedProduct.description = product.description;
            selectedProduct.price = product.price;
            selectedProduct.quantity = product.quantity;            
            ctx.SaveChanges();
            MessageBox.Show("Product " + selectedProduct.productName.ToString() + " " + " has been updated successfully.",
                            "Update Product", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
