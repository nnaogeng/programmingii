<?php
require_once '_setup.php';

$app->get('/admin', function ($request, $response, $args) {
    //$carList = DB::query("SELECT c.*, l.locationName, cat.categoryName FROM cars AS c, locations AS l, categories AS cat WHERE c.defaultLocationId = l.id AND c.categoryId = cat.id");                        
    $list = DB::query("SELECT count(*) AS count, cat.categoryName FROM reservations AS r, cars AS c, categories AS cat WHERE c.id = r.carId AND cat.id = c.categoryId GROUP BY cat.categoryName");
    //print_r($list);
    $quantityList = array();
    $categoryList = array();

    foreach($list as $item ) {
        array_push($quantityList, $item['count']);
        array_push($categoryList, $item['categoryName']);
    }
    //print_r($categoryList);

/*  echo '<script>';
    echo 'var pieChartData = ' . json_encode($quantityList) . ';';
    echo '</script>';
*/
    return $this->view->render($response, '/admin/dashboard.html.twig', ['categoryList' => $categoryList, 'quantityList' => $quantityList]);    
});


// Attach middleware that verifies only Admin can access /admin... URLs
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

// Function to check string starting 
// with given substring 
function startsWith($string, $startString) 
{ 
    $len = strlen($startString); 
    return (substr($string, 0, $len) === $startString); 
} 

$app->add(function (ServerRequestInterface $request, ResponseInterface $response, callable $next) {
    $url = $request->getUri()->getPath();
    if (startsWith($url, "/admin")) {
        if (!isset($_SESSION['user']) || $_SESSION['user']['isAdmin'] == 0) { // refuse if user not logged in AS ADMIN
            $response = $response->withStatus(403);
            return $this->view->render($response, 'admin/error_access_denied.html.twig');
        }
    }
    return $next($request, $response);
});
