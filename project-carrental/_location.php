<?php
require_once '_setup.php';

$app->get('/admin/location/list[/{pageNo:[0-9]+}]', function ($request, $response, $args) {
    $pageNo = $args['pageNo'] ?? 1;
    $totalRecords = DB::queryFirstField("SELECT COUNT(*) AS COUNT FROM locations");
    $totalPages = ceil($totalRecords / ROWS_PER_PAGE);
    return $this->view->render($response, '/admin/location_list.html.twig', [
            'maxPages' => $totalPages,
            'pageNo' => $pageNo,
        ]);
});

$app->get('/admin/location/list/singlepage/{pageNo:[0-9]+}', function ($request, $response, $args) {
    $pageNo = $args['pageNo'] ?? 1;
    $recordList = DB::query("SELECT * FROM locations LIMIT %d OFFSET %d", ROWS_PER_PAGE, ($pageNo - 1) * ROWS_PER_PAGE);
    return $this->view->render($response, '/admin/location_singlepage.html.twig', ['locationList' => $recordList]);
});

$app->get('/admin/location/{operation:add|update}[/{id:[0-9]+}]', function ($request, $response, $args) {
    // either op is add and id is not given OR op is edit and id must be given
    $operation = $args['operation'];
    if (isset($args['id'])) {
        $id = $args['id'];
    }
    if ( ($operation == 'add' && !empty($id)) || ($operation == 'update' && empty($id)) ) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    if ($operation == 'update') {
        $selectedRecord = DB::queryFirstRow("SELECT * FROM locations WHERE id = %d", $id);
        if (!$selectedRecord) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }
    } else {
        $selectedRecord = [];
    }
    return $this->view->render($response, 'admin/location_add_update.html.twig', ['location' => $selectedRecord, 'operation' => $operation]);
});

$app->post('/admin/location/{operation:add|update}[/{id:[0-9]+}]', function ($request, $response, $args) {
    if (isset($_POST['submit'])) {
        $operation = $args['operation'];

        // either op is add and id is not given OR op is edit and id must be given
        if ( ($operation == 'add' && !empty($args['id'])) || ($operation == 'update' && empty($args['id'])) ) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }

        $locationName = $request->getParam('locationName');
        $street = $request->getParam('street');
        $city = $request->getParam('city');
        $province = $request->getParam('province');    
        $postalCode = $request->getParam('postalCode');

        $location['locationName'] = $locationName;
        $location['street'] = $street;
        $location['city'] = $city;
        $location['province'] = $province;
        $location['postalCode'] = $postalCode;

        $errorList = array();

        //
        if ($errorList) {
            return $this->view->render($response, 'admin/location_add_update.html.twig',
                    [ 'errorList' => $errorList, ['location' => $location]  ]);
        } else {
            if ($operation == 'add') {
                DB::insert("locations", $location);                                    
                return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => $operation, 'object' => 'location' ]);
            } else {
                DB::update('locations', $location, "id = %d", $args['id']);
                return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => $operation, 'object' => 'location' ]);
            }
        }
    }
    if (isset($_POST['cancel'])) {        
        return $response->withRedirect("/admin/location/list");
    }
});

// STATE 1: first display
$app->get('/admin/location/delete/{id:[0-9]+}', function ($request, $response, $args) {
    $selectedRecord = DB::queryFirstRow("SELECT * FROM locations WHERE id = %d", $args['id']);
    if (!$selectedRecord) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    return $this->view->render($response, 'admin/location_delete.html.twig', ['location' => $selectedRecord] );
});

$app->post('/admin/location/delete/{id:[0-9]+}', function ($request, $response, $args) {
    DB::delete('locations', "id = %d", $args['id']);
    return $this->view->render($response, 'admin/operation_success.html.twig' , ['operation' => "delete", 'object' => 'location']);
});
