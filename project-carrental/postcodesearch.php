<?php

require_once '_setup.php';
require_once '_user.php';


$app->get('/chooseRentalPlace', function ($request, $response, $args) {
    return $this->view->render($response, 'chooseRentalPlace.html.twig', $args);
});

$app->post('/chooseRentalPlace', function ($request, $response, $args) use ($log) {
    //$pstcode = $request->getParam('postcode');

    $paramArray = $request->getQueryParams();
    $postcode = isset($paramArray['postcode']) ? $paramArray['postcode'] : "H8N 1N4";
    $arrCenterPostcode = getLatLong($postcode);
    $arrCenterPostcode['postcode'] = $postcode;
    $_SESSION['arrCenterPostcode'] = $arrCenterPostcode;    
    if ($postcode || $_SESSION['arrCenterPostcode']) {
        $locationList = DB::query("SELECT * FROM locations");
        $index = 1;
        foreach ($locationList as &$location) {
            $arrLatLong = getLatLong($location['postcode']);
            $location['index'] = $index++;
            $location['Latitude'] = $arrLatLong['Latitude'];
            $location['Longitude'] = $arrLatLong['Longitude'];
        }
        return $this->view->render(
            $response,
            'website/chooseRentalPlace.html.twig',
            [
                'locationList' => $locationList,
                'arrCenterPostcode' => $arrCenterPostcode
            ]
        );
    } else {
        return $this->view->render($response, '/chooseRentalPlace.html.twig');
    }
  
});

function getLatLong($code)
{
    //https://maps.googleapis.com/maps/api/geocode/json?&address=h9r4y5&key=AIzaSyBON6db-E8cIzVii1TuqKult0KLq9zvrDE
    $mapsApiKey = 'AIzaSyBON6db-E8cIzVii1TuqKult0KLq9zvrDE';
    $query = "https://maps.googleapis.com/maps/api/geocode/json?&address=" . urlencode($code) . "&key=" . $mapsApiKey;
    $data = file_get_contents($query);
    if ($data) {
        // convert into readable format
        $data = json_decode($data);

        $lat = $data->results[0]->geometry->location->lat;
        $long = $data->results[0]->geometry->location->lng;
        return array('Latitude' => $lat, 'Longitude' => $long);
    } else {
        return false;
    }
}

