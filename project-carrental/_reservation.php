<?php
require_once '_setup.php';

$app->get('/admin/reservation/list[/{pageNo:[0-9]+}]', function ($request, $response, $args) {
    $pageNo = $args['pageNo'] ?? 1;
    $totalRecords = DB::queryFirstField("SELECT COUNT(*) AS COUNT FROM reservations");
    $totalPages = ceil($totalRecords / ROWS_PER_PAGE);
    return $this->view->render($response, '/admin/reservation_list.html.twig', [
            'maxPages' => $totalPages,
            'pageNo' => $pageNo,
        ]);
});

$app->get('/admin/reservation/list/singlepage/{pageNo:[0-9]+}', function ($request, $response, $args) {
    $pageNo = $args['pageNo'] ?? 1;
    $recordList = DB::query("SELECT * FROM reservations LIMIT %d OFFSET %d", ROWS_PER_PAGE, ($pageNo - 1) * ROWS_PER_PAGE);
    foreach($recordList as &$record) {
        $record['pickupLocationName'] = DB::queryFirstRow("SELECT locationName FROM locations WHERE id = %d", $record['pickupLocationId'])['locationName'];
        $record['returnLocationName'] = DB::queryFirstRow("SELECT locationName FROM locations WHERE id = %d", $record['returnLocationId'])['locationName'];
    //print_r($record['pickupLocationName']);
    //print_r($record['returnLocationName']);
    }
    //print_r($recordList);
    return $this->view->render($response, '/admin/reservation_singlepage.html.twig', ['reservationList' => $recordList]);
});

$app->get('/admin/reservation/{operation:add|update}[/{id:[0-9]+}]', function ($request, $response, $args) {
    // either op is add and id is not given OR op is edit and id must be given
    $operation = $args['operation'];
    if (isset($args['id'])) {
        $id = $args['id'];
    }
    if ( ($operation == 'add' && !empty($id)) || ($operation == 'update' && empty($id)) ) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    if ($operation == 'update') {
        $selectedRecord = DB::queryFirstRow("SELECT * FROM reservations WHERE id = %d", $id);
        if (!$selectedRecord) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }
    } else {
        $selectedRecord = [];
    }
    $locationList = DB::query("SELECT * FROM locations");
    return $this->view->render($response, 'admin/reservation_add_update.html.twig', ['reservation' => $selectedRecord, 'operation' => $operation, 'locationList' => $locationList]);
});

$app->post('/admin/reservation/{operation:add|update}[/{id:[0-9]+}]', function ($request, $response, $args) {
    if (isset($_POST['submit'])) {
        $operation = $args['operation'];

        // either op is add and id is not given OR op is edit and id must be given
        if ( ($operation == 'add' && !empty($args['id'])) || ($operation == 'update' && empty($args['id'])) ) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }

        $customerId = $request->getParam('customerId');
        $carId = $request->getParam('carId');
        $startDateTime = $request->getParam('startDateTime');
        $returnDateTime = $request->getParam('returnDateTime');
        $actualReturnDT = $request->getParam('actualReturnDT');
        $pickupLocationId = $request->getParam('pickupLocationName');
        $returnLocationId = $request->getParam('returnLocationName');    
        $costPerDay = $request->getParam('costPerDay');        
        $checkboxStuatus = $request->getParam('insurance');
        $insurance = ($checkboxStuatus == 'on' ? 1 : 0);

        $reservation['customerId'] = $customerId;
        $reservation['carId'] = $carId;
        $reservation['startDateTime'] = $startDateTime;
        $reservation['returnDateTime'] = $returnDateTime;
        $reservation['actualReturnDT'] = $actualReturnDT;
        $reservation['pickupLocationId'] = $pickupLocationId;
        $reservation['returnLocationId'] = $returnLocationId;
        $reservation['costPerDay'] = $costPerDay;
        $reservation['insurance'] = $insurance;

        //$pickupLocation = DB::queryFirstRow("SELECT id FROM locations WHERE locationName = %s", $pickupLocationName);
        //$returnLocation = DB::queryFirstRow("SELECT id FROM locations WHERE locationName = %s", $returnLocationName);

        $errorList = array();

        //
        if ($errorList) {
            return $this->view->render($response, 'admin/reservation_add_update.html.twig',
                    [ 'errorList' => $errorList, ['reservation' => $reservation]]);
        } else {
            if ($operation == 'add') {     
                DB::insert("reservations", ['customerId' => $customerId, 'carId' => $carId, 'startDateTime' => $startDateTime, 'returnDateTime' => $returnDateTime, 'actualReturnDT' => $actualReturnDT, 'pickupLocationId' => $pickupLocationId, 'returnLocationId' => $returnLocationId, 'costPerDay' => $costPerDay, 'insurance' => $insurance]);                                
                return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => $operation, 'object' => "reservation" ]);
            } else {
                DB::update('reservations', ['customerId' => $customerId, 'carId' => $carId, 'startDateTime' => $startDateTime, 'returnDateTime' => $returnDateTime, 'actualReturnDT' => $actualReturnDT, 'pickupLocationId' => $pickupLocationId, 'returnLocationId' => $returnLocationId, 'costPerDay' => $costPerDay, 'insurance' => $insurance], "id = %d", $args['id']);
                return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => $operation, 'object' => "reservation"  ]);
            }
        }
    }
    if (isset($_POST['cancel'])) {        
        return $response->withRedirect("/admin/reservation/list");
    }
});

// STATE 1: first display
$app->get('/admin/reservation/delete/{id:[0-9]+}', function ($request, $response, $args) {
    $selectedRecord = DB::queryFirstRow("SELECT * FROM reservations WHERE id = %d", $args['id']);
    if (!$selectedRecord) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    return $this->view->render($response, 'admin/reservation_delete.html.twig', ['reservation' => $selectedRecord] );
});

$app->post('/admin/reservation/delete/{id:[0-9]+}', function ($request, $response, $args) {
    DB::delete('reservations', "id = %d", $args['id']);
    return $this->view->render($response, 'admin/operation_success.html.twig' , ['operation' => "delete", 'object' => "reservation" ]);
});
/*

$app->post('/reservationUpdate/{id}', function ($request, $response, $args) {
    if (isset($_POST['update'])) {    
        $id = $args['id'];        
        $customerId = $request->getParam('customerId');
        $carId = $request->getParam('carId');
        $startDateTime = $request->getParam('startDateTime');
        $returnDateTime = $request->getParam('returnDateTime');
        $actualReturnDT = $request->getParam('actualReturnDT');
        $pickupLocationName = $request->getParam('pickupLocationName');
        $returnLocationName = $request->getParam('returnLocationName');    
        $costPerDay = $request->getParam('costPerDay');
        $checkboxStuatus = $request->getParam('insurance');
        $insurance = ($checkboxStuatus == 'on' ? 1 : 0);

        $pickupLocation = DB::queryFirstRow("SELECT id FROM locations WHERE locationName = %s", $pickupLocationName);
        $returnLocation = DB::queryFirstRow("SELECT id FROM locations WHERE locationName = %s", $returnLocationName);
        
        DB::update("reservations", ['customerId' => $customerId, 'carId' => $carId, 'startDateTime' => $startDateTime, 'returnDateTime' => $returnDateTime, 'actualReturnDT' => $actualReturnDT, 'pickupLocationId' => $pickupLocation['id'], 'returnLocationId' => $returnLocation['id'], 'costPerDay' => $costPerDay, 'insurance' => $insurance, 'actualReturnDT' => NULL], "id = %d", $id); 

        $reservationList = DB::query("SELECT r.*, l1.locationName AS pickupLocationName, l2.locationName AS returnLocationName FROM reservations AS r, locations AS l1, locations AS l2 WHERE r.pickupLocationId = l1.id AND r.returnLocationId = l2.id"); 
        return $this->view->render($response, 'reservation.html.twig', [ 'reservationList' => $reservationList ]);  
    }
    if (isset($_POST['cancel'])) {
        $reservationList = DB::query("SELECT r.*, l1.locationName AS pickupLocationName, l2.locationName AS returnLocationName FROM reservations AS r, locations AS l1, locations AS l2 WHERE r.pickupLocationId = l1.id AND r.returnLocationId = l2.id"); 
        return $this->view->render($response, 'reservation.html.twig', [ 'reservationList' => $reservationList ]);  
    }
});*/