<?php
require_once '_setup.php';
require_once "vendor/autoload.php";
$app->get('/chooseRentalPlace', function ($request, $response, $args) {
    $paramArray = $request->getQueryParams();
    $postalcode = $paramArray['postalcode'];
    $arrCenterPostalcode = getLatLong($postalcode);
    $arrCenterPostalcode['postalcode'] = $postalcode;
    $_SESSION['arrCenterPostalcode'] = $arrCenterPostalcode;
    if ($postalcode || $_SESSION['arrCenterPostalcode']) {
        $rentalPlaceList = DB::query("SELECT * FROM locations");
        $index = 1;
        foreach ($rentalPlaceList as &$rentalPlace) {
            $arrLatLong = getLatLong($rentalPlace['postalcode']);
            $rentalPlace['index'] = $index++;
            $rentalPlace['Latitude'] = $arrLatLong['Latitude'];
            $rentalPlace['Longitude'] = $arrLatLong['longitude'];
            $rentalPlace['address'] = $arrLatLong['address'];
        }
        return $this->view->render($response, 'chooseRentalPlace.html.twig', ['rentalPlaceList' => $rentalPlaceList, 'arrCenterPostalcode' => $arrCenterPostalcode]);
    }
    return $this->view->render($response, 'chooseRentalPlace.html.twig');
});

