<?php
require_once '_setup.php';
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Transaction;


$app->get('/booking/step1', function ($request, $response, $args) {            
    $locationList = DB::query("SELECT * from locations");      
    return $this->view->render($response, 'booking/step1.html.twig', ['user' => $_SESSION['user'] ?? "", 'locationList' => $locationList]);    
});
/*
$app->get('/booking/step2', function ($request, $response, $args) {                  
    $categoryList = DB::query("SELECT * from categories");
    return $this->view->render($response, 'booking/step2.html.twig', ['categoryList' => $categoryList]);       
});
*/

$app->get('/booking/selectViewCategory/{categoryId:[0-9]+}/{sorterId:[0-9]+}', function ($request, $response, $args) { 
    $categoryId = $args['categoryId'];
    $sorterId = $args['sorterId'];
    switch($sorterId) {
        case "0": 
            $sortBy = "costPerDay";
            break;
        case "1":
            $sortBy = "costPerDay DESC";
            break;
        case "2":
            $sortBy = "passengersNo";
            break;
        case "3":
            $sortBy = "passengersNo DESC";
            break;
        default:
            break;
    }

    if($categoryId == "0") {
        $categoryList = DB::query("SELECT * from categories ORDER BY " . $sortBy);
    }
    else {
        $categoryList = DB::query("SELECT * from categories WHERE id = %d  ORDER BY " . $sortBy, $categoryId);
    }
    return $this->view->render($response, 'booking/selectViewCategory.html.twig', ['categoryList' => $categoryList, 'booking' => $_SESSION['booking'], 'stepNo' => 2]);
});


$app->get('/booking/step2', function ($request, $response, $args) { 
    $categoryList = DB::query("SELECT * from categories");
    return $this->view->render($response, 'booking/step2.html.twig', ['categoryList' => $categoryList, 'booking' => $_SESSION['booking'], 'stepNo' => 2]);
});

$app->post('/booking/step2', function ($request, $response, $args) { 
    $errorList = array();

    $pickupDate = $request->getParam('pickupDate');
    $pickupTime = $request->getParam('pickupTime');
    $returnDate = $request->getParam('returnDate');
    $returnTime = $request->getParam('returnTime');
    //print_r($pickupTime);
    //print_r($returnTime);

    $booking['pickupDate'] = $pickupDate;
    $booking['pickupTime'] = $pickupTime;
    $booking['returnDate'] = $returnDate;
    $booking['returnTime'] = $returnTime;
    
    $pickupLocationId = $request->getParam('pickupLocation');
    $returnLocationId = $request->getParam('returnLocation');
    $pickupLocation = DB::queryFirstRow("SELECT street, city FROM locations WHERE id = %d", $pickupLocationId);
    $returnLocation = DB::queryFirstRow("SELECT street, city FROM locations WHERE id = %d", $returnLocationId);
    $booking['pickupLocation'] = $pickupLocation;
    $booking['returnLocation'] = $returnLocation;

/*    $pickupLocation = explode(",", $request->getParam('pickupLocation'));
    $booking['pickupStreet'] = $pickupLocation[0];
    $booking['pickupCity'] = $pickupLocation[1];
    $returnLocation =  explode(",", $request->getParam('returnLocation'));
    $booking['returnStreet'] = $returnLocation[0];
    $booking['returnCity'] = $returnLocation[1];
*/    /*
    $carList = DB::query("SELECT c.*, cat.categoryName FROM cars AS c JOIN categories AS cat ON c.categoryId = cat.id 
        WHERE c.id IN (SELECT carId FROM reservations WHERE startDateTime > %s OR returnDateTime < %s)", $strReturnDateTime, $strPickupDateTime);
    */
    $strPickupDateTime = $pickupDate . " " . $pickupTime;
    $strReturnDateTime = $returnDate . " " . $returnTime;
    $pickupDateTime = new DateTime($strPickupDateTime);
    date_timezone_set($pickupDateTime, new DateTimeZone('Canada/Eastern'));
    $returnDateTime = new DateTime($strReturnDateTime);
    date_timezone_set($returnDateTime, new DateTimeZone('Canada/Eastern'));
    date_default_timezone_set('Canada/Eastern'); 
    $now = date("Y-m-d H:i:s");
    $currentDateTime = new DateTime($now);
    //print_r($currentDateTime);
    //print_r($pickupDateTime);

    if($pickupDateTime < $currentDateTime) {
        $errorList[] = "Pick up Date/Time must be later than current Date/Time.";
    }
    if($pickupDateTime >= $returnDateTime) {
        $errorList[] = "Return Date/Time must be later than pick up Date/Time.";
    }

    $duration = $returnDateTime->diff($pickupDateTime);
    if ($duration->i >= 30) {
        $duration->h ++;
    }
    $duration->i = 0;
    if ($duration->h >= 10) {
        $duration->d ++;
        $duration->h = 0;
    }
    $strDuration = $duration->d == 0 ? "" : ($duration->d . ($duration->d > 1 ? " days " : " day "));
    $strDuration = $strDuration . ($duration->h == 0 ? "" : ($duration->h . ($duration->h > 1 ? " hours" : " hour")));
       
    $booking['strDuration'] = $strDuration;
    $booking['duration'] = $duration; 
    
    $_SESSION['booking'] = $booking;
   // $_SESSION['booking']['strDuration'] = $strDuration;
    //print_r($booking['duration']->d);

    //print_r($carList);
    //print_r($reservationList);
    //print_r($reservation['startDateTime']); 
    //print_r($strPickupDateTime);
    //print_r($reservation['startDateTime'] > $strPickupDateTime);
    
    //print_r($errorList);
    if($errorList) {
        $locationList = DB::query("SELECT * from locations"); 
        return $this->view->render($response, 'booking/step1.html.twig', ['errorList' => $errorList, 'booking' => $_SESSION['booking'], 'locationList' => $locationList]);
    }

    $categoryList = DB::query("SELECT * from categories");
    return $this->view->render($response, 'booking/step2.html.twig', ['categoryList' => $categoryList, 'booking' => $_SESSION['booking'], 'stepNo' => 2]);
});

$app->post('/booking/step3/{categoryId}', function ($request, $response, $args) {     
    $categoryId = $args['categoryId'];    
    $category = DB::queryFirstRow("SELECT * FROM categories WHERE id = %d", $categoryId);
    //print_r($category);
    
    $_SESSION['category'] = $category;
    return $this->view->render($response, 'booking/step3.html.twig', ['booking' => $_SESSION['booking'], 'category' => $_SESSION['category'], 'stepNo' => 3]);
});


$app->post('/booking/step4', function ($request, $response, $args) {     
    if (isset($_SESSION['user'])) {
        $customer = DB::queryFirstRow("SELECT * FROM customers WHERE userId = %d", $_SESSION['user']['id']);
    //print_r($_SESSION['user']);
    //print_r($customer);
    }
    if (isset($_POST['confirmBooking'])) {
        
    }
    else {
        return $this->view->render($response, 'booking/step4.html.twig', ['booking' => $_SESSION['booking'], 'customer' => $customer, 'category' => $_SESSION['category'], 'stepNo' => 4]);
    }

/*    $booking = $_SESSION['booking'];
    $days = $booking['duration']->format('%a');
    $hours = $booking['duration']->format('%h');
    $pricePerDay = $_SESSION['category']['costPerDay'];
    $TotalPrice = $pricePerDay * $days + $pricePerDay * $hours * 0.1;
    $deposit = $TotalPrice * 0.1;
    print_r($TotalPrice);
    print_r($deposit);
*/
});


//$app->post('/booking/recaptcha', function ($request, $response, $args) {
function recaptcha() {
    if(isset($_POST['g-recaptcha-response'])){
        $captcha=$_POST['g-recaptcha-response'];
    }
    if(!$captcha){
        echo '<h2>Please check the the captcha form.</h2>';
        return false;
        exit;
    }
       
    $secretKey = "6LfxzbcZAAAAAOeWVvkN0v7bNkMEQfDXNSEmUICl";
    $ip = $_SERVER['REMOTE_ADDR'];

    $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($captcha);
    $response = file_get_contents($url);
    $responseKeys = json_decode($response,true);

    if(!$responseKeys["success"]) {
        echo "Error";
        return false;
        exit;
    }
        echo "You have successfully reserve your car rental";
        return true;
}
//});

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

$app->add(function (ServerRequestInterface $request, ResponseInterface $response, callable $next) {
    $url = $request->getUri()->getPath();
    if (startsWith($url, "/booking/payment/redirect")) {
        if (!recaptcha()) {
            $response = $response->withStatus(403);
            return $this->view->render($response, 'booking/error_access_denied.html.twig');
        }
        return $next($request, $response);
    }
    return $next($request, $response);
});


$app->post('/booking/payment/redirect', function ($request, $response, $args) {
	echo ("not implemented this function.");
});


