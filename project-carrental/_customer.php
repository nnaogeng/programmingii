<?php
require_once '_setup.php';

$app->get('/admin/customer/list[/{pageNo:[0-9]+}]', function ($request, $response, $args) {
    $pageNo = $args['pageNo'] ?? 1;
    $totalRecords = DB::queryFirstField("SELECT COUNT(*) AS COUNT FROM customers");
    $totalPages = ceil($totalRecords / ROWS_PER_PAGE);
    return $this->view->render($response, '/admin/customer_list.html.twig', [
            'maxPages' => $totalPages,
            'pageNo' => $pageNo,
        ]);
});

$app->get('/admin/customer/list/singlepage/{pageNo:[0-9]+}', function ($request, $response, $args) {
    $pageNo = $args['pageNo'] ?? 1;
    $recordList = DB::query("SELECT c.*, u.email FROM customers AS c, users AS u WHERE u.id = c.userId LIMIT %d OFFSET %d", ROWS_PER_PAGE, ($pageNo - 1) * ROWS_PER_PAGE);
    return $this->view->render($response, '/admin/customer_singlepage.html.twig', ['customerList' => $recordList]);
});

$app->get('/admin/customer/{operation:add|update}[/{id:[0-9]+}]', function ($request, $response, $args) {    
    $operation = $args['operation'];
    if (isset($args['id'])) {
        $id = $args['id'];
    }
    if ( ($operation == 'add' && !empty($id)) || ($operation == 'update' && empty($id)) ) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    if ($operation == 'update') {
        $selectedRecord = DB::queryFirstRow("SELECT * FROM customers WHERE id = %d LIMIT 1", $id);
        if (!$selectedRecord) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }
    } else {
        $selectedRecord = [];
    }
    return $this->view->render($response, 'admin/customer_add_update.html.twig', ['customer' => $selectedRecord, 'operation' => $operation]);
});

$app->post('/admin/customer/{operation:add|update}[/{id:[0-9]+}]', function ($request, $response, $args) {
    if (isset($_POST['submit'])) {            
        $operation = $args['operation'];

        // either op is add and id is not given OR op is edit and id must be given
        if ( ($operation == 'add' && !empty($args['id'])) || ($operation == 'update' && empty($args['id'])) ) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }

        $firstName = $request->getParam('firstName');
        $lastName = $request->getParam('lastName');
        //$email = $request->getParam('email');
        $phoneNo = $request->getParam('phoneNo');
        $street = $request->getParam('street');
        $city = $request->getParam('city');
        $province = $request->getParam('province');    
        $postalCode = $request->getParam('postalCode');

        $customer['firstName'] = $firstName;
        $customer['lastName'] = $lastName;
        //$customer['email'] = $email;
        $customer['phoneNo'] = $phoneNo;
        $customer['street'] = $street;
        $customer['city'] = $city;
        $customer['province'] = $province;
        $customer['postalCode'] = $postalCode;

        $errorList = array();

        //
        if ($errorList) {
            return $this->view->render($response, 'admin/customer_add_update.html.twig',
                    [ 'errorList' => $errorList, ['customer' => $customer]  ]);
        } else {
            if ($operation == 'add') {
                DB::insert("customers", $customer);                                    
                return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => $operation, 'object' => 'customer' ]);
            } else {
                DB::update('customers', $customer, "id = %d", $args['id']);
                return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => $operation, 'object' => 'customer' ]);
            }
        }
    }
    if (isset($_POST['cancel'])) {        
        return $response->withRedirect("/admin/customer/list");
    }
});

// STATE 1: first display
$app->get('/admin/customer/delete/{id:[0-9]+}', function ($request, $response, $args) {
    $selectedRecord = DB::queryFirstRow("SELECT * FROM customers WHERE id = %d", $args['id']);
    if (!$selectedRecord) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    return $this->view->render($response, 'admin/customer_delete.html.twig', ['customer' => $selectedRecord] );
});


$app->post('/admin/customer/delete/{id:[0-9]+}', function ($request, $response, $args) {
    DB::delete('customers', "id = %d", $args['id']);
    return $this->view->render($response, 'admin/operation_success.html.twig' , ['operation' => "delete", 'object' => "customer"]);
});



$app->post('/customerAdd', function ($request, $response, $args) {
    if (isset($_POST['add'])) {            
        $firstName = $request->getParam('firstName');
        $lastName = $request->getParam('lastName');
        $email = $request->getParam('email');
        $phoneNo = $request->getParam('phoneNo');
        $street = $request->getParam('street');
        $city = $request->getParam('city');
        $province = $request->getParam('province');    
        $postalCode = $request->getParam('postalCode');

        DB::insert("customers", ['firstName' => $firstName, 'lastName' => $lastName, 'email' => $email, 'phoneNo' => $phoneNo, 'street' => $street, 'city' => $city, 'province' => $province, 'postalCode' => $postalCode, 'userId' => 2]);                        
        $customerList = DB::query("SELECT * FROM customers"); 
        return $this->view->render($response, 'customer.html.twig', [ 'customerList' => $customerList ]);  
    }
    if (isset($_POST['cancel'])) {
        $customerList = DB::query("SELECT * FROM customers"); 
        return $this->view->render($response, 'customer.html.twig', [ 'customerList' => $customerList ]); 
    }
});

$app->get('/customerUpdate/{id}', function ($request, $response, $args) {
    $id = $args['id'];
    $customer = DB::queryFirstRow("SELECT * FROM customers WHERE id = %d LIMIT 1", $id);                          
    return $this->view->render($response, 'customerUpdate.html.twig', ['customer' => $customer]);  
});

$app->post('/customerUpdate/{id}', function ($request, $response, $args) {
    if (isset($_POST['update'])) {    
        $id = $args['id'];        
        $firstName = $request->getParam('firstName');
        $lastName = $request->getParam('lastName');
        $email = $request->getParam('email');
        $phoneNo = $request->getParam('phoneNo');
        $street = $request->getParam('street');
        $city = $request->getParam('city');
        $province = $request->getParam('province');    
        $postalCode = $request->getParam('postalCode');

        DB::update("customers", ['firstName' => $firstName, 'lastName' => $lastName, 'street' => $street, 'city' => $city, 'province' => $province, 'postalCode' => $postalCode], "id = %d", $id);                        
        $customerList = DB::query("SELECT * FROM customers"); 
        return $this->view->render($response, 'customer.html.twig', [ 'customerList' => $customerList ]);  
    }
    if (isset($_POST['cancel'])) {
        $customerList = DB::query("SELECT * FROM customers"); 
        return $this->view->render($response, 'customer.html.twig', [ 'customerList' => $customerList ]); 
    }
});