<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* booking/masterBooking.html.twig */
class __TwigTemplate_1809e084545da2dc8c105d72d712463739f0b390e197fad2599add0b2aa758d0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
            'footer' => [$this, 'block_footer'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">    
    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css\" integrity=\"sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk\" crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"../css/masterTemp.css\">
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>

    <title>";
        // line 10
        $this->displayBlock('title', $context, $blocks);
        echo " - My Webpage</title>
    ";
        // line 11
        $this->displayBlock('head', $context, $blocks);
        // line 12
        echo "</head>

<body>
    <div id=\"centeredContent\">
        <div class=\"row\">
            <div class=\"col\"></div>
            <div class=\"col-11 col-md-10 col-lg-9 border border-light rounded px-2 bg-light\">
                <nav class=\"navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow\">
                    <a class=\"navbar-brand col-md-3 col-lg-2 mr-0 px-3\" href=\"#\">IPD Car Rental</a>
                    <button class=\"navbar-toggler position-absolute d-md-none collapsed\" type=\"button\" data-toggle=\"collapse\" data-target=\"#sidebarMenu\" aria-controls=\"sidebarMenu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                        <span class=\"navbar-toggler-icon\"></span>
                    </button>
                    ";
        // line 24
        if (($context["user"] ?? null)) {
            // line 25
            echo "                    <input class=\"form-control bg-dark w-100 text-white text-right\" type=\"text\" value=\"";
            echo twig_escape_filter($this->env, (("Welcome " . twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "userName", [], "any", false, false, false, 25)) . " !"), "html", null, true);
            echo "\" aria-label=\"\">
                    
                    ";
            // line 29
            echo "                    <ul class=\"navbar-nav px-3\">
                       ";
            // line 31
            echo " 
                        <li class=\"nav-item text-nowrap\">
                            <a class=\"nav-link\" href=\"/logout\">Sign out</a>
                    ";
        } else {
            // line 35
            echo "                    <input class=\"form-control form-control-dark w-100\" type=\"text\" aria-label=\"\">
                    <ul class=\"navbar-nav px-3\">
                        <li class=\"nav-item text-nowrap\">
                            <a class=\"nav-link\" href=\"/login\">Sign in</a>
                    ";
        }
        // line 40
        echo "                        </li>
                    </ul>
                </nav>
            </div>
            <div class=\"col\"></div>
        </div>

        ";
        // line 47
        $this->displayBlock('content', $context, $blocks);
        // line 48
        echo "
    </div>

    <div id=\"footer\">
        <div class=\"row\">
            <div class=\"col\"></div>
            <div class=\"col-11 col-md-10 col-lg-9 border border-light rounded px-2 text-center bg-secondary\">
                ";
        // line 55
        $this->displayBlock('footer', $context, $blocks);
        // line 58
        echo "            </div>
            <div class=\"col\"></div>
        </div>        
    </div>

    ";
        // line 63
        $this->displayBlock('js', $context, $blocks);
        // line 64
        echo "</body>
</html>";
    }

    // line 10
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 11
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 47
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 55
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 56
        echo "                    &copy; Copyright 2011 by <a href=\"http://domain.invalid/\">IPD 21</a>.
                ";
    }

    // line 63
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    public function getTemplateName()
    {
        return "booking/masterBooking.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 63,  155 => 56,  151 => 55,  145 => 47,  139 => 11,  133 => 10,  128 => 64,  126 => 63,  119 => 58,  117 => 55,  108 => 48,  106 => 47,  97 => 40,  90 => 35,  84 => 31,  81 => 29,  75 => 25,  73 => 24,  59 => 12,  57 => 11,  53 => 10,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">    
    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css\" integrity=\"sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk\" crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"../css/masterTemp.css\">
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>

    <title>{% block title %}{% endblock %} - My Webpage</title>
    {% block head %}{% endblock %}
</head>

<body>
    <div id=\"centeredContent\">
        <div class=\"row\">
            <div class=\"col\"></div>
            <div class=\"col-11 col-md-10 col-lg-9 border border-light rounded px-2 bg-light\">
                <nav class=\"navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow\">
                    <a class=\"navbar-brand col-md-3 col-lg-2 mr-0 px-3\" href=\"#\">IPD Car Rental</a>
                    <button class=\"navbar-toggler position-absolute d-md-none collapsed\" type=\"button\" data-toggle=\"collapse\" data-target=\"#sidebarMenu\" aria-controls=\"sidebarMenu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                        <span class=\"navbar-toggler-icon\"></span>
                    </button>
                    {% if user %}
                    <input class=\"form-control bg-dark w-100 text-white text-right\" type=\"text\" value=\"{{'Welcome ' ~ user.userName ~ ' !'}}\" aria-label=\"\">
                    
                    {#<label class=\"text-white text-right\">{{'Welcome ' ~ user.userName ~ ' !'}}</label>
                    #}
                    <ul class=\"navbar-nav px-3\">
                       {# <li class=\"nav-item text-nowrap text-white\">{{'Welcome ' ~ user.userName ~ ' !'}}</li>
                       #} 
                        <li class=\"nav-item text-nowrap\">
                            <a class=\"nav-link\" href=\"/logout\">Sign out</a>
                    {% else %}
                    <input class=\"form-control form-control-dark w-100\" type=\"text\" aria-label=\"\">
                    <ul class=\"navbar-nav px-3\">
                        <li class=\"nav-item text-nowrap\">
                            <a class=\"nav-link\" href=\"/login\">Sign in</a>
                    {% endif %}
                        </li>
                    </ul>
                </nav>
            </div>
            <div class=\"col\"></div>
        </div>

        {% block content %}{% endblock %}

    </div>

    <div id=\"footer\">
        <div class=\"row\">
            <div class=\"col\"></div>
            <div class=\"col-11 col-md-10 col-lg-9 border border-light rounded px-2 text-center bg-secondary\">
                {% block footer %}
                    &copy; Copyright 2011 by <a href=\"http://domain.invalid/\">IPD 21</a>.
                {% endblock %}
            </div>
            <div class=\"col\"></div>
        </div>        
    </div>

    {% block js %} {% endblock %}
</body>
</html>", "booking/masterBooking.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\booking\\masterBooking.html.twig");
    }
}
