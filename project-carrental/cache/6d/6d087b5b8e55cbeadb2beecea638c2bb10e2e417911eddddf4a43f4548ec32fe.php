<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_121cc118a298000ff059d9a992b859c7aae90ac0ab27a6a8792910e65398614a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Home Page";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css\" integrity=\"sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z\" crossorigin=\"anonymous\">
<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js\" integrity=\"sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV\" crossorigin=\"anonymous\"></script>
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script> 
    <script>
        \$(document).ready(function(){
            \$(\"#postalcodeSearch\").click(function(){
                var postalcode = \$(\"#postalcode\").val();
                window.location.href=\"/chooseRentalPlace/\"+ postalcode;
            });
        });

        \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
            console.log(\"Ajax error occured on \" + settings.url);
            alert(\"Ajax error occured\");
        });
    </script>
\t<style>
\t\t.booking {
\t\t\tbackground: #f2f2f2;
\t\t\twidth: 100%;
\t\t\tmargin-top: 80px;
\t\t\theight: 10%;
\t\t\ttext-align: center;
\t\t\tpadding-top: 10px;
\t\t}

\t\t.booking input {
\t\t\twidth: 300px;
\t\t\theight: 70px;
\t\t\tborder: none;
\t\t\tbackground: #fe4c50;
\t\t\tcolor: #FFFFFF;
\t\t\tfont-size: 24px;
\t\t\tfont-weight: 500;
\t\t\ttext-transform: uppercase;
\t\t\tcursor: pointer;
\t\t\tborder-radius: 25px;
\t\t}
\t\t.booking input:hover {
\t\t\tbackground: #FE7C7F;
\t\t}
\t</style>
";
    }

    // line 50
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 51
        echo "    <!-- Slider --> 
<!--    <div class=\"main_slider\" style=\"background-image:url(../image/slider_1.jpg)\">
    </div>
<div class=\"newsletter\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-6\">
\t\t\t\t\t<div class=\"newsletter_text d-flex flex-column justify-content-center align-items-lg-start align-items-md-center text-center\">
\t\t\t\t\t\t<h4>Rental Place</h4>
\t\t\t\t\t\t<p>You can search the nearest location by PostCode</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-lg-6\">
\t\t\t\t\t<form action=\"post\" action=\"/chooseRentalPlace\">
\t\t\t\t\t\t<div class=\"newsletter_form d-flex flex-md-row flex-column flex-xs-column align-items-center justify-content-lg-end justify-content-center\">
\t\t\t\t\t\t\t<input id=\"newsletter_email\" name=\"postalcode\"  id=\"postalcode\" type=\"text\" placeholder=\"H8N 1N4\" required=\"required\" data-error=\"Valid email is required.\">
\t\t\t\t\t\t\t<button type=\"submit\" class=\"newsletter_submit_btn trans_300\" value=\"Submit\"  id=\"postalcodeSearch\">search</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</form>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>-->
\t<div class=\"booking\">
\t
\t ";
        // line 76
        if (($context["userSession"] ?? null)) {
            // line 77
            echo "\t\t<a href=\"/booking/step1\"><input type=\"submit\" name=\"booking\" value=\"Booking a car\"></input></a>                                                                  
\t\t\t";
        } else {
            // line 79
            echo "\t\t\tPlease Login first
\t\t\t<a href=\"/login\"><input type=\"submit\" name=\"booking\" value=\"Booking a car\"></input></a>
\t\t\t";
        }
        // line 82
        echo "\t</div>
\t<!-- Banner -->
\t<div class=\"banner\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_1.webp)\">\t
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 36/day  &nbsp&nbsp&nbsp&nbsp&nbsp<b>Economy</b></a>
\t\t\t\t\t</div>
                    <p>2 Adults, 2 Bags</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_2.jpg)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 32/day &nbsp&nbsp&nbsp&nbsp&nbsp<b>Compact</b></a>
\t\t\t\t\t</div>
                    <p>4 Adults, 2 Bags</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_3.webp)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 41/day &nbsp&nbsp&nbsp<b>Intermediate</b></a>
\t\t\t\t\t</div>
                    <p>5 Adults, 3 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_4.webp)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 36/day &nbsp&nbsp&nbsp&nbsp&nbsp<b>Standard</b></a>
\t\t\t\t\t</div>
                    <p>5 Adults, 3 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_5.jpg)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 35/day &nbsp&nbsp&nbsp<b>Full-size</b></a>
\t\t\t\t\t</div>
                    <p>4 Adults, 2 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_6.jpg)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 66/day &nbsp&nbsp&nbsp&nbsp&nbsp<b>Minivan</b></a>
\t\t\t\t\t</div>
                    <p>5 Adults, 5 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_7.webp)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 83/day &nbsp<b>Full-size SUV</b></a>
\t\t\t\t\t</div>
                    <p>5 Adults, 2 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_8.webp)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 40/day &nbsp&nbsp&nbsp&nbsp&nbsp<b>Premium</b></a>
\t\t\t\t\t</div>
                    <p>5 Adults, 5 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_9.png)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 150/day <b>Pass Van</b></a>
\t\t\t\t\t</div>
                    <p>5 Adults, 3 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_10.jpg)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 93/day &nbsp&nbsp&nbsp&nbsp&nbsp<b>Luxury</b></a>
\t\t\t\t\t</div>
                    <p> 5 Adults, 5 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_11.jpg)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 55/day &nbsp&nbsp&nbsp<b>Pickup Truck</b></a>
\t\t\t\t\t</div>
                    <p>5 Adults, 3 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_12.jpg)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 160/day &nbsp&nbsp&nbsp<b>Premium SUV</b></a>
\t\t\t\t\t</div>
                    <p>5 Adults, 5 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_13.webp)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 35/day &nbsp&nbsp&nbsp<b>Compact SUV</b></a>
\t\t\t\t\t</div>
                    <p> 4 Adults, 2 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_14.png)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 87/day &nbsp&nbsp<b>Luxury SUV</b></a>
\t\t\t\t\t</div>
                    <p> 5 Adults, 5 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_15.webp)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 44/day &nbsp&nbsp&nbsp&nbsp&nbsp<b>Commercial</b></a>
\t\t\t\t\t</div>
                    <p> 5 Adults, 3 Bags</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>\t
";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 82,  142 => 79,  138 => 77,  136 => 76,  109 => 51,  105 => 50,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Home Page{% endblock %}

{% block head %}
<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css\" integrity=\"sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z\" crossorigin=\"anonymous\">
<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js\" integrity=\"sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV\" crossorigin=\"anonymous\"></script>
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script> 
    <script>
        \$(document).ready(function(){
            \$(\"#postalcodeSearch\").click(function(){
                var postalcode = \$(\"#postalcode\").val();
                window.location.href=\"/chooseRentalPlace/\"+ postalcode;
            });
        });

        \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
            console.log(\"Ajax error occured on \" + settings.url);
            alert(\"Ajax error occured\");
        });
    </script>
\t<style>
\t\t.booking {
\t\t\tbackground: #f2f2f2;
\t\t\twidth: 100%;
\t\t\tmargin-top: 80px;
\t\t\theight: 10%;
\t\t\ttext-align: center;
\t\t\tpadding-top: 10px;
\t\t}

\t\t.booking input {
\t\t\twidth: 300px;
\t\t\theight: 70px;
\t\t\tborder: none;
\t\t\tbackground: #fe4c50;
\t\t\tcolor: #FFFFFF;
\t\t\tfont-size: 24px;
\t\t\tfont-weight: 500;
\t\t\ttext-transform: uppercase;
\t\t\tcursor: pointer;
\t\t\tborder-radius: 25px;
\t\t}
\t\t.booking input:hover {
\t\t\tbackground: #FE7C7F;
\t\t}
\t</style>
{% endblock %}

{% block content %}
    <!-- Slider --> 
<!--    <div class=\"main_slider\" style=\"background-image:url(../image/slider_1.jpg)\">
    </div>
<div class=\"newsletter\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-6\">
\t\t\t\t\t<div class=\"newsletter_text d-flex flex-column justify-content-center align-items-lg-start align-items-md-center text-center\">
\t\t\t\t\t\t<h4>Rental Place</h4>
\t\t\t\t\t\t<p>You can search the nearest location by PostCode</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-lg-6\">
\t\t\t\t\t<form action=\"post\" action=\"/chooseRentalPlace\">
\t\t\t\t\t\t<div class=\"newsletter_form d-flex flex-md-row flex-column flex-xs-column align-items-center justify-content-lg-end justify-content-center\">
\t\t\t\t\t\t\t<input id=\"newsletter_email\" name=\"postalcode\"  id=\"postalcode\" type=\"text\" placeholder=\"H8N 1N4\" required=\"required\" data-error=\"Valid email is required.\">
\t\t\t\t\t\t\t<button type=\"submit\" class=\"newsletter_submit_btn trans_300\" value=\"Submit\"  id=\"postalcodeSearch\">search</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</form>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>-->
\t<div class=\"booking\">
\t
\t {% if userSession %}
\t\t<a href=\"/booking/step1\"><input type=\"submit\" name=\"booking\" value=\"Booking a car\"></input></a>                                                                  
\t\t\t{% else %}
\t\t\tPlease Login first
\t\t\t<a href=\"/login\"><input type=\"submit\" name=\"booking\" value=\"Booking a car\"></input></a>
\t\t\t{% endif %}
\t</div>
\t<!-- Banner -->
\t<div class=\"banner\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_1.webp)\">\t
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 36/day  &nbsp&nbsp&nbsp&nbsp&nbsp<b>Economy</b></a>
\t\t\t\t\t</div>
                    <p>2 Adults, 2 Bags</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_2.jpg)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 32/day &nbsp&nbsp&nbsp&nbsp&nbsp<b>Compact</b></a>
\t\t\t\t\t</div>
                    <p>4 Adults, 2 Bags</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_3.webp)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 41/day &nbsp&nbsp&nbsp<b>Intermediate</b></a>
\t\t\t\t\t</div>
                    <p>5 Adults, 3 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_4.webp)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 36/day &nbsp&nbsp&nbsp&nbsp&nbsp<b>Standard</b></a>
\t\t\t\t\t</div>
                    <p>5 Adults, 3 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_5.jpg)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 35/day &nbsp&nbsp&nbsp<b>Full-size</b></a>
\t\t\t\t\t</div>
                    <p>4 Adults, 2 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_6.jpg)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 66/day &nbsp&nbsp&nbsp&nbsp&nbsp<b>Minivan</b></a>
\t\t\t\t\t</div>
                    <p>5 Adults, 5 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_7.webp)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 83/day &nbsp<b>Full-size SUV</b></a>
\t\t\t\t\t</div>
                    <p>5 Adults, 2 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_8.webp)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 40/day &nbsp&nbsp&nbsp&nbsp&nbsp<b>Premium</b></a>
\t\t\t\t\t</div>
                    <p>5 Adults, 5 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_9.png)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 150/day <b>Pass Van</b></a>
\t\t\t\t\t</div>
                    <p>5 Adults, 3 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_10.jpg)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 93/day &nbsp&nbsp&nbsp&nbsp&nbsp<b>Luxury</b></a>
\t\t\t\t\t</div>
                    <p> 5 Adults, 5 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_11.jpg)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 55/day &nbsp&nbsp&nbsp<b>Pickup Truck</b></a>
\t\t\t\t\t</div>
                    <p>5 Adults, 3 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_12.jpg)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 160/day &nbsp&nbsp&nbsp<b>Premium SUV</b></a>
\t\t\t\t\t</div>
                    <p>5 Adults, 5 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_13.webp)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 35/day &nbsp&nbsp&nbsp<b>Compact SUV</b></a>
\t\t\t\t\t</div>
                    <p> 4 Adults, 2 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_14.png)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 87/day &nbsp&nbsp<b>Luxury SUV</b></a>
\t\t\t\t\t</div>
                    <p> 5 Adults, 5 Bags</p>
\t\t\t\t</div>
                <div class=\"col-md-4\">
\t\t\t\t\t<div class=\"banner_item align-items-center\" style=\"background-image:url(../image/banner_15.webp)\"></div>
\t\t\t\t\t<div class=\"banner_category\">
\t\t\t\t\t\t<a>C\$ 44/day &nbsp&nbsp&nbsp&nbsp&nbsp<b>Commercial</b></a>
\t\t\t\t\t</div>
                    <p> 5 Adults, 3 Bags</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>\t
{% endblock %}
", "index.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\index.html.twig");
    }
}
