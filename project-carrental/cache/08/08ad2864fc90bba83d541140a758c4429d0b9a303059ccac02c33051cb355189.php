<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* login.html.twig */
class __TwigTemplate_91c9fc86f29f4638206ab772e9bd3ac8433fa684effec10502ef698b7b7e13cd extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Login";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\" integrity=\"sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN\" crossorigin=\"anonymous\">
<link rel=\"stylesheet\" href=\"./css/loginregister.css\" />
<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>
<script src=\"js/jquery-2.2.0.min.js\"></script>
<script>
\$(document).ready(function() {
            \$('input[name=email]').on('paste, blur change', function() {
                var email = \$('input[name=email]').val();
                \$(\"#nothisemail\").load(\"/nothisemail/\" + email);
            });
          
            \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
                console.log(\"Ajax error occured on \" + settings.url);
                alert(\"Ajax error occured\");
            });
        });
</script>
";
    }

    // line 25
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        if (($context["errorList"] ?? null)) {
            // line 27
            echo "        <ul class=\"errorMessage\">
        ";
            // line 28
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 29
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "<li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 31
            echo "        </ul>
    ";
        }
        // line 33
        echo "<div class=\"wrap\">
    <form method=\"post\">
        <label>Email: </label>
        <input type=\"text\" name=\"email\" id=\"email\" value=\"\"></input>
        <span class=\"errorMessage\" id=\"nothisemail\"></span><br />
        <label>Password: </label>
        <input type=\"password\" name=\"password\" id=\"password\" value=\"\"></input><br />
        <input type=\"submit\" name=\"login\" id=\"login\" value=\"Login\" class=\"button\" /><br />\t\t
        <button class=\"loginBtn loginBtn--google\" onclick=\"window.location='/logingoogle.php'\" type=\"button\" name=\"logingmail\"  class=\"btn btn-danger\">Login with Google</button><br>
        <button class=\"loginBtn loginBtn--facebook\" onclick=\"window.location='/loginfacebook.php'\" type=\"button\" name=\"loginfacebook\" class=\"btn btn-danger\">Login with Facebook</button><br>      
        <p><a href=\"/forgotpassword\">Forgot Password</a></p>
        <p>Not a Member?<a href=\"/register\">Sign Up Now!</a></p>      
    </form>   
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 33,  102 => 31,  93 => 29,  89 => 28,  86 => 27,  84 => 26,  80 => 25,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Login{% endblock %}

{% block head %}
<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\" integrity=\"sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN\" crossorigin=\"anonymous\">
<link rel=\"stylesheet\" href=\"./css/loginregister.css\" />
<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>
<script src=\"js/jquery-2.2.0.min.js\"></script>
<script>
\$(document).ready(function() {
            \$('input[name=email]').on('paste, blur change', function() {
                var email = \$('input[name=email]').val();
                \$(\"#nothisemail\").load(\"/nothisemail/\" + email);
            });
          
            \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
                console.log(\"Ajax error occured on \" + settings.url);
                alert(\"Ajax error occured\");
            });
        });
</script>
{% endblock %}

{% block content %}
{% if errorList %}
        <ul class=\"errorMessage\">
        {% for error in errorList %}
            <li>{{error}}<li>
        {% endfor %}
        </ul>
    {% endif %}
<div class=\"wrap\">
    <form method=\"post\">
        <label>Email: </label>
        <input type=\"text\" name=\"email\" id=\"email\" value=\"\"></input>
        <span class=\"errorMessage\" id=\"nothisemail\"></span><br />
        <label>Password: </label>
        <input type=\"password\" name=\"password\" id=\"password\" value=\"\"></input><br />
        <input type=\"submit\" name=\"login\" id=\"login\" value=\"Login\" class=\"button\" /><br />\t\t
        <button class=\"loginBtn loginBtn--google\" onclick=\"window.location='/logingoogle.php'\" type=\"button\" name=\"logingmail\"  class=\"btn btn-danger\">Login with Google</button><br>
        <button class=\"loginBtn loginBtn--facebook\" onclick=\"window.location='/loginfacebook.php'\" type=\"button\" name=\"loginfacebook\" class=\"btn btn-danger\">Login with Facebook</button><br>      
        <p><a href=\"/forgotpassword\">Forgot Password</a></p>
        <p>Not a Member?<a href=\"/register\">Sign Up Now!</a></p>      
    </form>   
    </div>
</div>
{% endblock %}
", "login.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\login.html.twig");
    }
}
