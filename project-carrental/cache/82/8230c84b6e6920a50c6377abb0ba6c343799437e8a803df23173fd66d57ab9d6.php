<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /admin/dashboard.html.twig */
class __TwigTemplate_86cfc826926866ad9457ae120f3154b4d3e6643b25c7634aa1382b99723f8c93 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'addItem' => [$this, 'block_addItem'],
            'table' => [$this, 'block_table'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/masterAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("admin/masterAdmin.html.twig", "/admin/dashboard.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_addItem($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "                        <h1 class=\"h2\">Dashboard</h1>
                        <div class=\"btn-toolbar mb-2 mb-md-0\">
                        <div class=\"btn-group mr-2\">
                            <button type=\"button\" class=\"btn btn-sm btn-outline-secondary\">Share</button>
                            <button type=\"button\" class=\"btn btn-sm btn-outline-secondary\">Export</button>
                        </div>
                        <button type=\"button\" class=\"btn btn-sm btn-outline-secondary dropdown-toggle\">
                            <span data-feather=\"calendar\"></span>
                            This week
                        </button>
                        </div>
                    ";
    }

    // line 17
    public function block_table($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "                    <div>              
                        <canvas class=\"my-4 w-100\" id=\"lineChart\" width=\"900\" height=\"380\"></canvas>  
                    </div>
                    <div>   
                        ";
        // line 22
        if (($context["categoryList"] ?? null)) {
            // line 23
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categoryList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                echo "     
                                <input class=\"category\" type=\"hidden\" value=";
                // line 24
                echo twig_escape_filter($this->env, $context["category"], "html", null, true);
                echo " />
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "            
                        ";
        }
        // line 26
        echo "  
                        ";
        // line 27
        if (($context["quantityList"] ?? null)) {
            // line 28
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["quantityList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["quantity"]) {
                echo "     
                                <input class=\"quantity\" type=\"hidden\" value=";
                // line 29
                echo twig_escape_filter($this->env, $context["quantity"], "html", null, true);
                echo " />
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quantity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "            
                        ";
        }
        // line 31
        echo " 

                        <canvas class=\"my-4 w-100\" id=\"pieChart\" width=\"900\" height=\"380\"></canvas>  
                    </div>
                ";
    }

    // line 37
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 38
        echo "                    <script src=\"/js/myCharts.js\"></script>
                    
                ";
    }

    public function getTemplateName()
    {
        return "/admin/dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 38,  130 => 37,  122 => 31,  118 => 30,  110 => 29,  103 => 28,  101 => 27,  98 => 26,  94 => 25,  86 => 24,  79 => 23,  77 => 22,  71 => 18,  67 => 17,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"admin/masterAdmin.html.twig\" %}

{% block addItem %}
                        <h1 class=\"h2\">Dashboard</h1>
                        <div class=\"btn-toolbar mb-2 mb-md-0\">
                        <div class=\"btn-group mr-2\">
                            <button type=\"button\" class=\"btn btn-sm btn-outline-secondary\">Share</button>
                            <button type=\"button\" class=\"btn btn-sm btn-outline-secondary\">Export</button>
                        </div>
                        <button type=\"button\" class=\"btn btn-sm btn-outline-secondary dropdown-toggle\">
                            <span data-feather=\"calendar\"></span>
                            This week
                        </button>
                        </div>
                    {% endblock %}

                {% block table %}
                    <div>              
                        <canvas class=\"my-4 w-100\" id=\"lineChart\" width=\"900\" height=\"380\"></canvas>  
                    </div>
                    <div>   
                        {% if categoryList %}
                            {% for category in categoryList %}     
                                <input class=\"category\" type=\"hidden\" value={{category}} />
                            {% endfor %}            
                        {% endif %}  
                        {% if quantityList %}
                            {% for quantity in quantityList %}     
                                <input class=\"quantity\" type=\"hidden\" value={{quantity}} />
                            {% endfor %}            
                        {% endif %} 

                        <canvas class=\"my-4 w-100\" id=\"pieChart\" width=\"900\" height=\"380\"></canvas>  
                    </div>
                {% endblock %}

                {% block js %}
                    <script src=\"/js/myCharts.js\"></script>
                    
                {% endblock %}", "/admin/dashboard.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\admin\\dashboard.html.twig");
    }
}
