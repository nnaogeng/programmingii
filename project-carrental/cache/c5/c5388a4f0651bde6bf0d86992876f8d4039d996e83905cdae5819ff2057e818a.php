<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* booking/step1.html.twig */
class __TwigTemplate_07f4bf02fa4431ebfd8e312d9cc79498be499c59087646d2e684ac934afb73fa extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "booking/masterBooking.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("booking/masterBooking.html.twig", "booking/step1.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Booking Step1";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 
    <link rel=\"stylesheet\" href=\"//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css\">
    <script>
        \$(document).ready(function() {                    
            \$(\"#cbSameLocation\").change(function() 
            {
                if(this.checked == true)
                {                   
                    \$(\"#returnLocation\").val(\$(\"#pickupLocation\").val());
                    \$( \"#returnLocation\" ).prop( \"disabled\", true );
                }
                else
                {
                    \$( \"#returnLocation\" ).prop( \"disabled\", false);
                }
            });                          
        });
    </script>
";
    }

    // line 25
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        echo "    <div class=\"row form-responsive\">
        <div class=\"col\"></div>
        <div class=\"col-11 col-md-10 col-lg-9 border border-light rounded p-2 bg-light\">
            <form method=\"post\" class=\"form form-sm\" action=\"/booking/step2\">
                <h4>Step 1: When And Where</h4> 
                ";
        // line 31
        if (($context["errorList"] ?? null)) {
            // line 32
            echo "                    <h5 class=\"errorMessage mt-4 ml-4\">Error:</h5>
                    <ul class=\"errorMessage\">
                        ";
            // line 34
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 35
                echo "                            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "<li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 37
            echo "                    </ul>
                ";
        }
        // line 39
        echo "                <div class=\"row\">
                    <div class=\"col-12 col-md-6 col-lg-6\">
                        <label class=\"col-form-label\">Pick up Date/Time:</label><br/>
                        <div class=\"row\">
                            <div class=\"col-7\">                                                            
                                <input type=\"date\" name=\"pickupDate\" class=\"datepicker form-control\" value=\"";
        // line 44
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "pickupDate", [], "any", false, false, false, 44), "html", null, true);
        echo "\" required></input>                            </div>
                            <div class=\"col-5\">                                                                                               
                                <input name=\"pickupTime\" class=\"timepicker form-control\" value=\"";
        // line 46
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "pickupTime", [], "any", false, false, false, 46), "html", null, true);
        echo "\" required></input>
                            </div>
                        </div> 
                        <label class=\"col-form-label\">Pickup Location:</label><br/>
                        <select id=\"pickupLocation\" name=\"pickupLocation\" class=\"form-control\">
                            ";
        // line 51
        if (($context["locationList"] ?? null)) {
            // line 52
            echo "                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["locationList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["location"]) {
                // line 53
                echo "                                    <option value=";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["location"], "id", [], "any", false, false, false, 53), "html", null, true);
                echo ">";
                echo twig_escape_filter($this->env, twig_join_filter([0 => twig_get_attribute($this->env, $this->source, $context["location"], "street", [], "any", false, false, false, 53), 1 => twig_get_attribute($this->env, $this->source, $context["location"], "city", [], "any", false, false, false, 53)], " , "), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['location'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 54
            echo "            
                            ";
        }
        // line 55
        echo "                            
                        </select><br/>
                        <p>Can't find the location? <a href=\"#\">View Map</a></p>  
                    </div>

                    <div class=\"col-12 col-md-6 col-lg-6\">
                        <label class=\"col-form-label\">Return Date/Time:</label><br/>
                        <div class=\"row\">
                            <div class=\"col-7\">                                
                                <input type=\"date\" name=\"returnDate\" class=\"datepicker form-control\" value=\"";
        // line 64
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "returnDate", [], "any", false, false, false, 64), "html", null, true);
        echo "\" required></input>
                            </div>
                            <div class=\"col-5\">                                
                                <input name=\"returnTime\" class=\"timepicker form-control\" value=\"";
        // line 67
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "returnTime", [], "any", false, false, false, 67), "html", null, true);
        echo "\" required></input>
                            </div>
                        </div>
                        <label class=\"col-form-label\">Return Location:</label><br/>
                        <select name=\"returnLocation\" class=\"form-control\" id=\"returnLocation\">
                            ";
        // line 72
        if (($context["locationList"] ?? null)) {
            // line 73
            echo "                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["locationList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["location"]) {
                // line 74
                echo "                                    <option value=";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["location"], "id", [], "any", false, false, false, 74), "html", null, true);
                echo ">";
                echo twig_escape_filter($this->env, twig_join_filter([0 => twig_get_attribute($this->env, $this->source, $context["location"], "street", [], "any", false, false, false, 74), 1 => twig_get_attribute($this->env, $this->source, $context["location"], "city", [], "any", false, false, false, 74)], " , "), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['location'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "            
                            ";
        }
        // line 76
        echo "                            
                        </select><br/>
                        <div class=\"form-check\">
                            <input class=\"form-check-input\" type=\"checkbox\"  name=\"sameLocation\" id=\"cbSameLocation\">
                            <label class=\"form-check-label\">Same as the pickup location</label>
                        </div>                             
                    </div>                            
                </div>      
                <div class=\"mx-auto my-3\" style=\"width: 200px;\">                
                    <input type=\"submit\" name=\"getQuote\" value=\"GET A QUOTE NOW\" class=\"btn btn-secondary\" style=\"width: 200px;\"></input>                                        
                </div>                            
            </form>
        </div>
        <div class=\"col\"></div>    
    </div>
";
    }

    // line 93
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 
    <script src=\"//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js\"></script>
    <script>
        \$('.timepicker').timepicker({
            timeFormat: 'HH:mm',
            interval: 30,
            minTime: '6',
            maxTime: '23:00',
            //defaultTime: '6',
            startTime: '6:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });
        \$(\".datepicker\").keypress(function(event) {
            event.preventDefault();
        });
        \$(\".timepicker\").keypress(function(event) {
            event.preventDefault();
        });
    </script>

";
    }

    public function getTemplateName()
    {
        return "booking/step1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  223 => 93,  204 => 76,  200 => 75,  189 => 74,  184 => 73,  182 => 72,  174 => 67,  168 => 64,  157 => 55,  153 => 54,  142 => 53,  137 => 52,  135 => 51,  127 => 46,  122 => 44,  115 => 39,  111 => 37,  102 => 35,  98 => 34,  94 => 32,  92 => 31,  85 => 26,  81 => 25,  56 => 5,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"booking/masterBooking.html.twig\" %}

{% block title %}Booking Step1{% endblock %}

{% block head %} 
    <link rel=\"stylesheet\" href=\"//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css\">
    <script>
        \$(document).ready(function() {                    
            \$(\"#cbSameLocation\").change(function() 
            {
                if(this.checked == true)
                {                   
                    \$(\"#returnLocation\").val(\$(\"#pickupLocation\").val());
                    \$( \"#returnLocation\" ).prop( \"disabled\", true );
                }
                else
                {
                    \$( \"#returnLocation\" ).prop( \"disabled\", false);
                }
            });                          
        });
    </script>
{% endblock %}

{% block content %}
    <div class=\"row form-responsive\">
        <div class=\"col\"></div>
        <div class=\"col-11 col-md-10 col-lg-9 border border-light rounded p-2 bg-light\">
            <form method=\"post\" class=\"form form-sm\" action=\"/booking/step2\">
                <h4>Step 1: When And Where</h4> 
                {% if errorList %}
                    <h5 class=\"errorMessage mt-4 ml-4\">Error:</h5>
                    <ul class=\"errorMessage\">
                        {% for error in errorList %}
                            <li>{{error}}<li>
                        {% endfor %}
                    </ul>
                {% endif %}
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 col-lg-6\">
                        <label class=\"col-form-label\">Pick up Date/Time:</label><br/>
                        <div class=\"row\">
                            <div class=\"col-7\">                                                            
                                <input type=\"date\" name=\"pickupDate\" class=\"datepicker form-control\" value=\"{{booking.pickupDate}}\" required></input>                            </div>
                            <div class=\"col-5\">                                                                                               
                                <input name=\"pickupTime\" class=\"timepicker form-control\" value=\"{{booking.pickupTime}}\" required></input>
                            </div>
                        </div> 
                        <label class=\"col-form-label\">Pickup Location:</label><br/>
                        <select id=\"pickupLocation\" name=\"pickupLocation\" class=\"form-control\">
                            {% if locationList %}
                                {% for location in locationList %}
                                    <option value={{location.id}}>{{[location.street , location.city] | join(' , ')}}</option>
                                {% endfor %}            
                            {% endif %}                            
                        </select><br/>
                        <p>Can't find the location? <a href=\"#\">View Map</a></p>  
                    </div>

                    <div class=\"col-12 col-md-6 col-lg-6\">
                        <label class=\"col-form-label\">Return Date/Time:</label><br/>
                        <div class=\"row\">
                            <div class=\"col-7\">                                
                                <input type=\"date\" name=\"returnDate\" class=\"datepicker form-control\" value=\"{{booking.returnDate}}\" required></input>
                            </div>
                            <div class=\"col-5\">                                
                                <input name=\"returnTime\" class=\"timepicker form-control\" value=\"{{booking.returnTime}}\" required></input>
                            </div>
                        </div>
                        <label class=\"col-form-label\">Return Location:</label><br/>
                        <select name=\"returnLocation\" class=\"form-control\" id=\"returnLocation\">
                            {% if locationList %}
                                {% for location in locationList %}
                                    <option value={{location.id}}>{{[location.street , location.city] | join(' , ')}}</option>
                                {% endfor %}            
                            {% endif %}                            
                        </select><br/>
                        <div class=\"form-check\">
                            <input class=\"form-check-input\" type=\"checkbox\"  name=\"sameLocation\" id=\"cbSameLocation\">
                            <label class=\"form-check-label\">Same as the pickup location</label>
                        </div>                             
                    </div>                            
                </div>      
                <div class=\"mx-auto my-3\" style=\"width: 200px;\">                
                    <input type=\"submit\" name=\"getQuote\" value=\"GET A QUOTE NOW\" class=\"btn btn-secondary\" style=\"width: 200px;\"></input>                                        
                </div>                            
            </form>
        </div>
        <div class=\"col\"></div>    
    </div>
{% endblock %}

{% block js %} 
    <script src=\"//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js\"></script>
    <script>
        \$('.timepicker').timepicker({
            timeFormat: 'HH:mm',
            interval: 30,
            minTime: '6',
            maxTime: '23:00',
            //defaultTime: '6',
            startTime: '6:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });
        \$(\".datepicker\").keypress(function(event) {
            event.preventDefault();
        });
        \$(\".timepicker\").keypress(function(event) {
            event.preventDefault();
        });
    </script>

{% endblock %}


", "booking/step1.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\booking\\step1.html.twig");
    }
}
