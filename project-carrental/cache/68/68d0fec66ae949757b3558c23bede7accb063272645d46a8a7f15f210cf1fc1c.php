<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* master.html.twig */
class __TwigTemplate_14492ed2f27329f6c0f2468af312a00d88bb5ed4916901f04e47efe18f63091b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<html>
    <head>        
        <!---<link rel=\"stylesheet\" href=\"../css/admin.css\" />-->
\t\t<script src=\"../js/jquery-2.2.0.min.js\"></script>
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\">
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo " - My Webpage</title>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t    <link rel=\"stylesheet\" type=\"text/css\" href=\"../css/bootstrap4/bootstrap.min.css\">
\t    <link rel=\"stylesheet\" type=\"text/css\" href=\"../css/main_styles.css\">
\t    <link rel=\"stylesheet\" type=\"text/css\" href=\"../css/responsive.css\">
\t\t<style>
\t\t\t.footer
\t\t\t{
\t\t\t\twidth: 100%;
\t\t\t\tbackground-color: #f2f2f2;;
\t\t\t\ttext-align: center;
\t\t\t\tposition: relative;
\t\t\t\tleft: 0;
\t\t\t\tbottom: 0;
\t\t\t}
\t\t</style>
        ";
        // line 22
        $this->displayBlock('head', $context, $blocks);
        // line 25
        echo "    </head>
    <body>
    <div class=\"super_container\">
    <!-- Top Navigation -->

\t\t<div class=\"top_nav\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"top_nav_left\">Right Equipment, Lowest Cost. Guaranteed. We're Here! IPD Car Rental help you</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6 text-right\">
\t\t\t\t\t\t<div class=\"top_nav_right\">
\t\t\t\t\t\t\t<ul class=\"top_nav_menu\">

\t\t\t\t\t\t\t\t<!-- Language / My Account -->
\t\t\t\t\t\t\t\t<li class=\"language\">
\t\t\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t\t\tEnglish
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<ul class=\"language_selection\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">French</a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"account\">
                                 \t";
        // line 51
        if (($context["userSession"] ?? null)) {
            // line 52
            echo "                                   \t\t<b style=\"color:red\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["userSession"] ?? null), "userName", [], "any", false, false, false, 52), "html", null, true);
            echo "</b>
\t\t\t\t\t\t\t\t   \t\t";
            // line 53
            if (twig_get_attribute($this->env, $this->source, ($context["userSession"] ?? null), "isAdmin", [], "any", false, false, false, 53)) {
                // line 54
                echo "\t\t\t\t\t\t\t\t   \t\t<span><a href=\"/admin\"><b>Admin Management</b></a></span>                                                                  
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 56
            echo "\t\t\t\t\t\t\t\t    ";
        } else {
            // line 57
            echo "\t\t\t\t\t\t\t\t\t\t<a href=\"/login\">
\t\t\t\t\t\t\t\t\t\t\tMy Account
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t\t\t\t\t\t\t\t\t</a>
                                    ";
        }
        // line 61
        echo "\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</li>
                                ";
        // line 63
        if (($context["userSession"] ?? null)) {
            // line 64
            echo "                                \t<li class=\"language\">
\t\t\t\t\t\t\t\t\t\t<a href=\"/logout\">Logout<i class=\"fa fa-angle-down\"></i></a>
\t\t\t\t\t\t\t\t\t</li>
                                ";
        }
        // line 68
        echo "\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<!-- Main Navigation -->

\t\t<div class=\"main_nav_container\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-lg-12 text-left\">
\t\t\t\t\t\t";
        // line 84
        echo "\t\t\t\t\t\t<nav class=\"navbar\">
\t\t\t\t\t\t\t<ul class=\"navbar_menu\">
\t\t\t\t\t\t\t\t<li><a href=\"/\">home</a></li>
                                <li><a href=\"#\">about us</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#\">contact</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</nav>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t</header>

\t<div class=\"fs_menu_overlay\"></div>
\t<div class=\"hamburger_menu\">
\t\t<div class=\"hamburger_close\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>
\t\t<div class=\"hamburger_menu_content text-right\">
\t\t\t<ul class=\"menu_top_nav\">
\t\t\t\t<li class=\"menu_item has-children\">
\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\tEnglish
\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t\t\t\t</a>
\t\t\t\t\t<ul class=\"menu_selection\">
\t\t\t\t\t\t<li><a href=\"#\">French</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t\t<li class=\"menu_item has-children\">
\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\tMy Account
\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t\t\t\t</a>
\t\t\t\t\t<ul class=\"menu_selection\">
\t\t\t\t\t\t<li><a href=\"/login\"><i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>Sign In</a></li>
\t\t\t\t\t\t<li><a href=\"/register\"><i class=\"fa fa-user-plus\" aria-hidden=\"true\"></i>Register</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t\t<li class=\"menu_item\"><a href=\"#\">home</a></li>
\t\t\t\t<li class=\"menu_item\"><a href=\"#\">promotion</a></li>
\t\t\t\t<li class=\"menu_item\"><a href=\"#\">contact</a></li>
\t\t\t</ul>
\t\t</div>
\t</div>
        <div id=\"centeredContent\">
\t\t\t";
        // line 129
        $this->displayBlock('content', $context, $blocks);
        // line 132
        echo "        </div>
        <footer class=\"footer\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-6\">
\t\t\t\t\t<div class=\"footer_nav_container d-flex flex-sm-row flex-column align-items-center justify-content-lg-start justify-content-center text-center\">
\t\t\t\t\t\t<ul class=\"footer_nav\">
\t\t\t\t\t\t\t<li><a href=\"/questions\">FAQs</a></li>
\t\t\t\t\t\t\t<li><a href=\"/contactus\">Contact us</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t<div class=\"footer_nav_container\">
\t\t\t\t\t\t<div class=\"cr\">© All Rights Reserverd Hong And ChunJie</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</footer>
    </div>
    </body>
</html>";
    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 22
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 23
        echo "        
        ";
    }

    // line 129
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 130
        echo "\t\t\t\t
\t\t\t";
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  242 => 130,  238 => 129,  233 => 23,  229 => 22,  223 => 6,  195 => 132,  193 => 129,  146 => 84,  131 => 68,  125 => 64,  123 => 63,  119 => 61,  112 => 57,  109 => 56,  105 => 54,  103 => 53,  98 => 52,  96 => 51,  68 => 25,  66 => 22,  47 => 6,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<html>
    <head>        
        <!---<link rel=\"stylesheet\" href=\"../css/admin.css\" />-->
\t\t<script src=\"../js/jquery-2.2.0.min.js\"></script>
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\">
        <title>{% block title %}{% endblock %} - My Webpage</title>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t    <link rel=\"stylesheet\" type=\"text/css\" href=\"../css/bootstrap4/bootstrap.min.css\">
\t    <link rel=\"stylesheet\" type=\"text/css\" href=\"../css/main_styles.css\">
\t    <link rel=\"stylesheet\" type=\"text/css\" href=\"../css/responsive.css\">
\t\t<style>
\t\t\t.footer
\t\t\t{
\t\t\t\twidth: 100%;
\t\t\t\tbackground-color: #f2f2f2;;
\t\t\t\ttext-align: center;
\t\t\t\tposition: relative;
\t\t\t\tleft: 0;
\t\t\t\tbottom: 0;
\t\t\t}
\t\t</style>
        {% block head %}
        
        {% endblock %}
    </head>
    <body>
    <div class=\"super_container\">
    <!-- Top Navigation -->

\t\t<div class=\"top_nav\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"top_nav_left\">Right Equipment, Lowest Cost. Guaranteed. We're Here! IPD Car Rental help you</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6 text-right\">
\t\t\t\t\t\t<div class=\"top_nav_right\">
\t\t\t\t\t\t\t<ul class=\"top_nav_menu\">

\t\t\t\t\t\t\t\t<!-- Language / My Account -->
\t\t\t\t\t\t\t\t<li class=\"language\">
\t\t\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t\t\tEnglish
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<ul class=\"language_selection\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">French</a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"account\">
                                 \t{% if userSession %}
                                   \t\t<b style=\"color:red\">{{userSession.userName}}</b>
\t\t\t\t\t\t\t\t   \t\t{% if userSession.isAdmin %}
\t\t\t\t\t\t\t\t   \t\t<span><a href=\"/admin\"><b>Admin Management</b></a></span>                                                                  
\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t    {% else %}
\t\t\t\t\t\t\t\t\t\t<a href=\"/login\">
\t\t\t\t\t\t\t\t\t\t\tMy Account
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t\t\t\t\t\t\t\t\t</a>
                                    {% endif %}\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</li>
                                {% if userSession %}
                                \t<li class=\"language\">
\t\t\t\t\t\t\t\t\t\t<a href=\"/logout\">Logout<i class=\"fa fa-angle-down\"></i></a>
\t\t\t\t\t\t\t\t\t</li>
                                {% endif %}
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<!-- Main Navigation -->

\t\t<div class=\"main_nav_container\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-lg-12 text-left\">
\t\t\t\t\t\t{#<div class=\"logo_container\">
\t\t\t\t\t\t\t<a href=\"#\">Car<span>Rental</span></a>
\t\t\t\t\t\t</div>#}
\t\t\t\t\t\t<nav class=\"navbar\">
\t\t\t\t\t\t\t<ul class=\"navbar_menu\">
\t\t\t\t\t\t\t\t<li><a href=\"/\">home</a></li>
                                <li><a href=\"#\">about us</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#\">contact</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</nav>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t</header>

\t<div class=\"fs_menu_overlay\"></div>
\t<div class=\"hamburger_menu\">
\t\t<div class=\"hamburger_close\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>
\t\t<div class=\"hamburger_menu_content text-right\">
\t\t\t<ul class=\"menu_top_nav\">
\t\t\t\t<li class=\"menu_item has-children\">
\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\tEnglish
\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t\t\t\t</a>
\t\t\t\t\t<ul class=\"menu_selection\">
\t\t\t\t\t\t<li><a href=\"#\">French</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t\t<li class=\"menu_item has-children\">
\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\tMy Account
\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t\t\t\t</a>
\t\t\t\t\t<ul class=\"menu_selection\">
\t\t\t\t\t\t<li><a href=\"/login\"><i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>Sign In</a></li>
\t\t\t\t\t\t<li><a href=\"/register\"><i class=\"fa fa-user-plus\" aria-hidden=\"true\"></i>Register</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t\t<li class=\"menu_item\"><a href=\"#\">home</a></li>
\t\t\t\t<li class=\"menu_item\"><a href=\"#\">promotion</a></li>
\t\t\t\t<li class=\"menu_item\"><a href=\"#\">contact</a></li>
\t\t\t</ul>
\t\t</div>
\t</div>
        <div id=\"centeredContent\">
\t\t\t{% block content %}
\t\t\t\t
\t\t\t{% endblock %}
        </div>
        <footer class=\"footer\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-6\">
\t\t\t\t\t<div class=\"footer_nav_container d-flex flex-sm-row flex-column align-items-center justify-content-lg-start justify-content-center text-center\">
\t\t\t\t\t\t<ul class=\"footer_nav\">
\t\t\t\t\t\t\t<li><a href=\"/questions\">FAQs</a></li>
\t\t\t\t\t\t\t<li><a href=\"/contactus\">Contact us</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t<div class=\"footer_nav_container\">
\t\t\t\t\t\t<div class=\"cr\">© All Rights Reserverd Hong And ChunJie</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</footer>
    </div>
    </body>
</html>", "master.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\master.html.twig");
    }
}
