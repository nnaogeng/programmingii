<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /admin/user_singlepage.html.twig */
class __TwigTemplate_df9e282a72d9d890b5e3b600499578673a75031af6b43e80c0bfc30afe3e5ae6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["userList"] ?? null)) {
            // line 2
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["userList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 3
                echo "        <tr>
            <td>";
                // line 4
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "userName", [], "any", false, false, false, 4), "html", null, true);
                echo "</td>
            <td>";
                // line 5
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "email", [], "any", false, false, false, 5), "html", null, true);
                echo "</td>
            <td>";
                // line 6
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "password", [], "any", false, false, false, 6), "html", null, true);
                echo "</td>                                                                                                   
            <td><input type=\"checkbox\" ";
                // line 7
                echo (((twig_get_attribute($this->env, $this->source, $context["user"], "isAdmin", [], "any", false, false, false, 7) == 0)) ? ("") : ("checked"));
                echo " onclick=\"return false\"></td>            
            <td><a href=\"/admin/user/update/";
                // line 8
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 8), "html", null, true);
                echo "\"><img src=\"/image/edit.png\" width=\"20\", height=\"20\"></a>
                <a href=\"/admin/user/delete/";
                // line 9
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 9), "html", null, true);
                echo "\"><img src=\"/image/delete.jpg\" width=\"20\", height=\"20\"></a>
                
            </td>
        </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "            
";
        }
        // line 14
        echo "  


";
    }

    public function getTemplateName()
    {
        return "/admin/user_singlepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 14,  78 => 13,  67 => 9,  63 => 8,  59 => 7,  55 => 6,  51 => 5,  47 => 4,  44 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if userList %}
    {% for user in userList %}
        <tr>
            <td>{{user.userName}}</td>
            <td>{{user.email}}</td>
            <td>{{user.password}}</td>                                                                                                   
            <td><input type=\"checkbox\" {{ user.isAdmin == 0 ? '' : 'checked'}} onclick=\"return false\"></td>            
            <td><a href=\"/admin/user/update/{{user.id}}\"><img src=\"/image/edit.png\" width=\"20\", height=\"20\"></a>
                <a href=\"/admin/user/delete/{{user.id}}\"><img src=\"/image/delete.jpg\" width=\"20\", height=\"20\"></a>
                
            </td>
        </tr>
    {% endfor %}            
{% endif %}  


", "/admin/user_singlepage.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\admin\\user_singlepage.html.twig");
    }
}
