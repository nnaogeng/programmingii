<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* booking/step3.html.twig */
class __TwigTemplate_0ac8b59771b4774b3f983901611da5df6b9d8e0eb4095324d2480d64201361d3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'formAction' => [$this, 'block_formAction'],
            'step' => [$this, 'block_step'],
            'selectedCarDetail' => [$this, 'block_selectedCarDetail'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "booking/masterBooking2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("booking/masterBooking2.html.twig", "booking/step3.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Booking Step3";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "    
";
    }

    // line 8
    public function block_formAction($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 
    action=\"/booking/step4\"
";
    }

    // line 12
    public function block_step($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    <div class=\"bg-light p-2 pl-3\">
        <h4>Step 3: Price And Insurance</h4>
    </div>
";
    }

    // line 18
    public function block_selectedCarDetail($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "    <div class=\"row mt-3 pl-1\">
        <div class=\"col-8\"><b>Car Type</b></div>
        <div class=\"col-4\"><a href=\"/booking/step2\">Change</a></div>
    </div>
    <div class=\"row mt-1 pl-1\">
        <div class=\"col-4\">
            <img src=";
        // line 25
        echo twig_escape_filter($this->env, ("../" . twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "photoFilePath", [], "any", false, false, false, 25)), "html", null, true);
        echo " width=\"72\" height=\"49\" class=\"img-responsive\"/>
        </div>
        <div class=\"col-8\">
            <p>";
        // line 28
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "categoryName", [], "any", false, false, false, 28), "html", null, true);
        echo "</p>
            <p>";
        // line 29
        echo "Example of this range: ";
        echo "</p>
            <p>";
        // line 30
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "carExample", [], "any", false, false, false, 30), "html", null, true);
        echo "</p>                                                            
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "booking/step3.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 30,  107 => 29,  103 => 28,  97 => 25,  89 => 19,  85 => 18,  78 => 13,  74 => 12,  65 => 8,  57 => 5,  50 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"booking/masterBooking2.html.twig\" %}

{% block title %}Booking Step3{% endblock %}

{% block head %}    
{% endblock %}

{% block formAction %} 
    action=\"/booking/step4\"
{% endblock %}

{% block step %}
    <div class=\"bg-light p-2 pl-3\">
        <h4>Step 3: Price And Insurance</h4>
    </div>
{% endblock %}

{% block selectedCarDetail %}
    <div class=\"row mt-3 pl-1\">
        <div class=\"col-8\"><b>Car Type</b></div>
        <div class=\"col-4\"><a href=\"/booking/step2\">Change</a></div>
    </div>
    <div class=\"row mt-1 pl-1\">
        <div class=\"col-4\">
            <img src={{'../' ~ category.photoFilePath}} width=\"72\" height=\"49\" class=\"img-responsive\"/>
        </div>
        <div class=\"col-8\">
            <p>{{category.categoryName}}</p>
            <p>{{\"Example of this range: \"}}</p>
            <p>{{category.carExample}}</p>                                                            
        </div>
    </div>
{% endblock %}

{#
{% block table %}     
    <div class=\"col-8 mt-2 table-responsive\">
        <table class=\"table table-sm\">
            <tbody>
                <tr>
                    <td>
                        <b>Rental Duration</b>
                    </td>
                    <td class=\"text-right\">
                        <p>{{booking.strDuration}}</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span><b>Price Per Day</b><span></br>
                        {% if booking.duration.d != 0 %}
                            <p>{{booking.duration.d ~ \" Day X \$CAD \" ~ category.costPerDay ~ \".00\"}}</p>
                        {% endif %}
                    </td>
                    <td class=\"text-right\">
                        {{\"\$CAD \" ~ category.costPerDay * booking.duration.d ~ \".00\"}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <span><b>Price Per Hour</b><span></br>
                        {% if booking.duration.h != 0 %}
                            <p>{{booking.duration.h ~ \" Hour X \$CAD \" ~ category.costPerDay * 0.1 ~ \".00\"}}</p>
                        {% endif %}
                    </td>
                    <td class=\"text-right\">
                        {{\"\$CAD \" ~ category.costPerDay * 0.1 * booking.duration.h ~ \".00\"}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <span><b>Car Rental Fee</b><span></br>
                        <p>{{\"\$CAD \" ~ category.costPerDay * booking.duration.d ~ \".00\"}}
                        {% if booking.duration.h != 0 %}
                            {{\" + \" ~ \"\$CAD \" ~ (category.costPerDay * 0.1 * booking.duration.h) ~ \".00\"}}
                        {% endif %}        
                        </p>    
                    </td>
                    <td class=\"text-right\">
                        {{\"\$CAD \" ~ (category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h) ~ \".00\"}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <span><b>Insurance Fee</b><span></br>
                    </td>
                    <td class=\"text-right\">                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <span><b>Sub-total</b><span></br>
                    </td>
                    <td class=\"text-right\">
                        {{\"\$CAD \" ~ (category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h) ~ \".00\"}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class=\"text-danger\"><b>Total Price<b><pan></br>
                    </td>
                    <td class=\"text-right\">
                        {{\"\$CAD \" ~ (category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h) ~ \".00\"}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <span><b>Required Deposit<b><pan></br>
                        <p>{{\"10% of \" ~ \"\$CAD \" ~ (category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h) ~ \".00\"}}
                    </td>
                    <td class=\"text-right\">
                        {{\"\$CAD \" ~ ((category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h) * 0.1) ~ \".00\"}}
                    </td>
                </tr>
            </tbody>
        </table>
        <input type=\"submit\" name=\"continue\" value=\"CONTINUE\" class=\"btn-sm btn-primary\">
    </div>
{% endblock %}
#}
", "booking/step3.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\booking\\step3.html.twig");
    }
}
