<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /admin/car_singlepage.html.twig */
class __TwigTemplate_9c6a01ace6e21ae4c0aa62ff69e5685621390d67137080d5108134b848669132 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["carList"] ?? null)) {
            // line 2
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["carList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["car"]) {
                // line 3
                echo "        <tr>
            <td>";
                // line 4
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["car"], "registrationNo", [], "any", false, false, false, 4), "html", null, true);
                echo "</td>
            <td>";
                // line 5
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["car"], "make", [], "any", false, false, false, 5), "html", null, true);
                echo "</td>
            <td>";
                // line 6
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["car"], "model", [], "any", false, false, false, 6), "html", null, true);
                echo "</td>
            <td>";
                // line 7
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["car"], "mileage", [], "any", false, false, false, 7), "html", null, true);
                echo "</td>
            <td>";
                // line 8
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["car"], "engineSize", [], "any", false, false, false, 8), "html", null, true);
                echo "</td>
            <td>";
                // line 9
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["car"], "productionYear", [], "any", false, false, false, 9), "html", null, true);
                echo "</td>
            <td>";
                // line 10
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["car"], "color", [], "any", false, false, false, 10), "html", null, true);
                echo "</td>
            <td>";
                // line 11
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["car"], "locationName", [], "any", false, false, false, 11), "html", null, true);
                echo "</td>
            <td>";
                // line 12
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["car"], "categoryName", [], "any", false, false, false, 12), "html", null, true);
                echo "</td>
            <td><a href=\"/admin/car/update/";
                // line 13
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["car"], "id", [], "any", false, false, false, 13), "html", null, true);
                echo "\"><img src=\"../../image/edit.png\" width=\"20\", height=\"20\"></a>
                <a href=\"/admin/car/delete/";
                // line 14
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["car"], "id", [], "any", false, false, false, 14), "html", null, true);
                echo "\"><img src=\"../../image/delete.jpg\" width=\"20\", height=\"20\"></a>
            </td>
        </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['car'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "            
";
        }
        // line 18
        echo "     


";
    }

    public function getTemplateName()
    {
        return "/admin/car_singlepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 18,  97 => 17,  87 => 14,  83 => 13,  79 => 12,  75 => 11,  71 => 10,  67 => 9,  63 => 8,  59 => 7,  55 => 6,  51 => 5,  47 => 4,  44 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if carList %}
    {% for car in carList %}
        <tr>
            <td>{{car.registrationNo}}</td>
            <td>{{car.make}}</td>
            <td>{{car.model}}</td>
            <td>{{car.mileage}}</td>
            <td>{{car.engineSize}}</td>
            <td>{{car.productionYear}}</td>
            <td>{{car.color}}</td>
            <td>{{car.locationName}}</td>
            <td>{{car.categoryName}}</td>
            <td><a href=\"/admin/car/update/{{car.id}}\"><img src=\"../../image/edit.png\" width=\"20\", height=\"20\"></a>
                <a href=\"/admin/car/delete/{{car.id}}\"><img src=\"../../image/delete.jpg\" width=\"20\", height=\"20\"></a>
            </td>
        </tr>
    {% endfor %}            
{% endif %}     


", "/admin/car_singlepage.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\admin\\car_singlepage.html.twig");
    }
}
