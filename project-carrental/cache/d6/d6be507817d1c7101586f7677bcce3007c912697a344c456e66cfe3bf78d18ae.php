<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* booking/step4.html.twig */
class __TwigTemplate_f1b4a8a6ae0d766cd6ba3fea695f55639ba8d940bc63b2ce6b2b1267b9a3723c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'formAction' => [$this, 'block_formAction'],
            'step' => [$this, 'block_step'],
            'selectedCarDetail' => [$this, 'block_selectedCarDetail'],
            'personInfo' => [$this, 'block_personInfo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "booking/masterBooking2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("booking/masterBooking2.html.twig", "booking/step4.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Booking Step4";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <script src=\"https://www.google.com/recaptcha/api.js?hl=en\"></script>    
";
    }

    // line 9
    public function block_formAction($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 
    action=\"/booking/payment/redirect\"
";
    }

    // line 13
    public function block_step($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "    <div class=\"bg-light p-2 pl-3\">
        <h4>Step 4: Checkout</h4>
    </div>
";
    }

    // line 19
    public function block_selectedCarDetail($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        echo "    <div class=\"row mt-3 pl-1\">
        <div class=\"col-8\"><b>Car Type</b></div>
        <div class=\"col-4\"><a href=\"/booking/step2\">Change</a></div>
    </div>
    <div class=\"row mt-1 pl-1\">
        <div class=\"col-4\">
            <img src=";
        // line 26
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "photoFilePath", [], "any", false, false, false, 26), "html", null, true);
        echo " width=\"72\" height=\"49\" class=\"img-responsive\"/>
        </div>
        <div class=\"col-8\">
            <p>";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "categoryName", [], "any", false, false, false, 29), "html", null, true);
        echo "</p>
            <p>";
        // line 30
        echo "Example of this range: ";
        echo "</p>
            <p>";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "carExample", [], "any", false, false, false, 31), "html", null, true);
        echo "</p>                                                            
        </div>
    </div>
";
    }

    // line 127
    public function block_personInfo($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 128
        echo "    <div class=\"border border-light rounded mt-2 p-3\">
        <div class=\"bg-light p-1\">
            <b>Personal Details</b>
        </div>
        <div class=\"row pb-1\">
            <div class=\"col-4\">
                <label class=\"col-form-label\">First Name:</label></br>
                <input type=\"text\" class=\"form-control\" name=\"firstName\" value=\"";
        // line 135
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["customer"] ?? null), "firstName", [], "any", false, false, false, 135), "html", null, true);
        echo "\" required maxlength=\"50\"></input>  
            </div>
            <div class=\"col-4\">
                <label class=\"col-form-label\">Last Name:</label></br>
                <input type=\"text\" class=\"form-control\" name=\"lastName\" value=\"";
        // line 139
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["customer"] ?? null), "lastName", [], "any", false, false, false, 139), "html", null, true);
        echo "\" required maxlength=\"50\"></input>  
            </div>
            <div class=\"col-4\">
                <label class=\"col-form-label\">Phone Number:</label></br>
                <input type=\"text\" class=\"form-control\" name=\"phoneNo\" value=\"";
        // line 143
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["customer"] ?? null), "phoneNo", [], "any", false, false, false, 143), "html", null, true);
        echo "\" required minlength=\"10\" maxlength=\"15\"></input>  
            </div>
        </div>
    </div>   

    <div class=\"border border-light rounded mt-2 p-3\">
        <div class=\"bg-light p-1\">
            <b>Billing Address</b>
        </div>
        <div>
                <label class=\"col-form-label\">Street:</label></br>
                <input type=\"text\" class=\"form-control\" name=\"street\" value=\"";
        // line 154
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["customer"] ?? null), "street", [], "any", false, false, false, 154), "html", null, true);
        echo "\"  maxlength=\"100\"></input>  
        </div>
        <div class=\"row pb-1\">                        
            <div class=\"col-4\">
                <label class=\"col-form-label\">City:</label></br>
                <input type=\"text\" class=\"form-control\" name=\"city\" value=\"";
        // line 159
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["customer"] ?? null), "city", [], "any", false, false, false, 159), "html", null, true);
        echo "\"  maxlength=\"50\"></input>  
            </div>
            <div class=\"col-4\">
                <label class=\"col-form-label\">Province:</label></br>
                <input type=\"text\" class=\"form-control\" name=\"province\" value=\"";
        // line 163
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["customer"] ?? null), "province", [], "any", false, false, false, 163), "html", null, true);
        echo "\"  maxlength=\"30\"></input>  
            </div>
            <div class=\"col-4\">
                <label class=\"col-form-label\">Postal Code:</label></br>
                <input type=\"text\" class=\"form-control\" name=\"postalCode\" value=\"";
        // line 167
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["customer"] ?? null), "postalCode", [], "any", false, false, false, 167), "html", null, true);
        echo "\"  maxlength=\"10\"></input> 
            </div>
        </div>
    </div>      

    <div class=\"border border-light rounded mt-2 p-3\">
        <div class=\"bg-light p-1\">
            <b>Payment Method</b>
        </div>                    
        <div class=\"row pb-1\">                        
            <div class=\"col-4\">
                <label class=\"col-form-label\">Select Payment Method:</label></br>
                <select type=\"text\" class=\"form-control\" name=\"paymentMethod\">
                    <option>Select Payment Method:</option>
                    <option>Cahs</option>
                    <option>Paypal</option>
                </select> 
            </div>                        
        </div>
    </div>    

    <div class=\"border border-light rounded mt-2 p-3\">
        <div class=\"bg-light p-1\">
            <b>Captcha</b>
        </div>                    
        <div class=\"row pb-1\">                        
            <div class=\"col-4\">                            
                <div class=\"g-recaptcha\" data-sitekey=\"6LfxzbcZAAAAAJGRJq8QtCvJdwdYRff920f6vkVA\"></div>
            </div>                        
        </div>
    </div> 
    <div class=\"text-center mb-4\">
        <input type=\"submit\" name=\"comfirmBooking\" value=\"Confirm Booking\"></input>
    </div>
";
    }

    public function getTemplateName()
    {
        return "booking/step4.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 167,  177 => 163,  170 => 159,  162 => 154,  148 => 143,  141 => 139,  134 => 135,  125 => 128,  121 => 127,  113 => 31,  109 => 30,  105 => 29,  99 => 26,  91 => 20,  87 => 19,  80 => 14,  76 => 13,  67 => 9,  62 => 6,  58 => 5,  51 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"booking/masterBooking2.html.twig\" %}

{% block title %}Booking Step4{% endblock %}

{% block head %}
    <script src=\"https://www.google.com/recaptcha/api.js?hl=en\"></script>    
{% endblock %}

{% block formAction %} 
    action=\"/booking/payment/redirect\"
{% endblock %}

{% block step %}
    <div class=\"bg-light p-2 pl-3\">
        <h4>Step 4: Checkout</h4>
    </div>
{% endblock %}

{% block selectedCarDetail %}
    <div class=\"row mt-3 pl-1\">
        <div class=\"col-8\"><b>Car Type</b></div>
        <div class=\"col-4\"><a href=\"/booking/step2\">Change</a></div>
    </div>
    <div class=\"row mt-1 pl-1\">
        <div class=\"col-4\">
            <img src={{category.photoFilePath}} width=\"72\" height=\"49\" class=\"img-responsive\"/>
        </div>
        <div class=\"col-8\">
            <p>{{category.categoryName}}</p>
            <p>{{\"Example of this range: \"}}</p>
            <p>{{category.carExample}}</p>                                                            
        </div>
    </div>
{% endblock %}

{#
{% block table %}
    <div class=\"col-8 mt-2 border border-light rounded table-responsive\">
        <div class=\"border border-light rounded mt-2 p-1\">
            <h5>Payment</h5>
        </div>
        <table class=\"table\">
            <tbody>
                <tr>
                    <td>
                        <b>Rental Duration</b>
                    </td>
                    <td class=\"text-right\">
                        <p>{{booking.strDuration}}</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span><b>Price Per Day</b><span></br>
                        {% if booking.duration.d != 0 %}
                            <p>{{booking.duration.d ~ \" Day X \$CAD \" ~ category.costPerDay}}</p>
                        {% endif %}
                    </td>
                    <td class=\"text-right\">
                        {{\"\$CAD \" ~ category.costPerDay * booking.duration.d}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <span><b>Price Per Hour</b><span></br>
                        {% if booking.duration.h != 0 %}
                            <p>{{booking.duration.h ~ \" Hour X \$CAD \" ~ category.costPerDay * 0.1}}</p>
                        {% endif %}
                    </td>
                    <td class=\"text-right\">
                        {{\"\$CAD \" ~ category.costPerDay * 0.1 * booking.duration.h}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <span><b>Car Rental Fee</b><span></br>
                        <p>{{category.costPerDay * booking.duration.d}}
                        {% if booking.duration.h != 0 %}
                            {{\" + \" ~ category.costPerDay * 0.1 * booking.duration.h}}
                        {% endif %}        
                        </p>    
                    </td>
                    <td class=\"text-right\">
                        {{category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <span><b>Insurance Fee</b><span></br>
                    </td>
                    <td class=\"text-right\">
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <span><b>Sub-total</b><span></br>
                    </td>
                    <td class=\"text-right\">
                        {{category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class=\"text-danger\"><b>Total Price<b><pan></br>
                    </td>
                    <td class=\"text-right\">
                        {{category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <span><b>Required Deposit<b><pan></br>
                        <p>{{\"10% of \" ~ (category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h)}}
                    </td>
                    <td class=\"text-right\">
                        {{(category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h) * 0.1}}
                    </td>
                </tr>
            </tbody>
        </table>          
        <input type=\"hidden\" name=\"deposit\" value=\"{{(category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h) * 0.1}}\">              
    </div>
{% endblock %}
#}
                
{% block personInfo %}
    <div class=\"border border-light rounded mt-2 p-3\">
        <div class=\"bg-light p-1\">
            <b>Personal Details</b>
        </div>
        <div class=\"row pb-1\">
            <div class=\"col-4\">
                <label class=\"col-form-label\">First Name:</label></br>
                <input type=\"text\" class=\"form-control\" name=\"firstName\" value=\"{{customer.firstName}}\" required maxlength=\"50\"></input>  
            </div>
            <div class=\"col-4\">
                <label class=\"col-form-label\">Last Name:</label></br>
                <input type=\"text\" class=\"form-control\" name=\"lastName\" value=\"{{customer.lastName}}\" required maxlength=\"50\"></input>  
            </div>
            <div class=\"col-4\">
                <label class=\"col-form-label\">Phone Number:</label></br>
                <input type=\"text\" class=\"form-control\" name=\"phoneNo\" value=\"{{customer.phoneNo}}\" required minlength=\"10\" maxlength=\"15\"></input>  
            </div>
        </div>
    </div>   

    <div class=\"border border-light rounded mt-2 p-3\">
        <div class=\"bg-light p-1\">
            <b>Billing Address</b>
        </div>
        <div>
                <label class=\"col-form-label\">Street:</label></br>
                <input type=\"text\" class=\"form-control\" name=\"street\" value=\"{{customer.street}}\"  maxlength=\"100\"></input>  
        </div>
        <div class=\"row pb-1\">                        
            <div class=\"col-4\">
                <label class=\"col-form-label\">City:</label></br>
                <input type=\"text\" class=\"form-control\" name=\"city\" value=\"{{customer.city}}\"  maxlength=\"50\"></input>  
            </div>
            <div class=\"col-4\">
                <label class=\"col-form-label\">Province:</label></br>
                <input type=\"text\" class=\"form-control\" name=\"province\" value=\"{{customer.province}}\"  maxlength=\"30\"></input>  
            </div>
            <div class=\"col-4\">
                <label class=\"col-form-label\">Postal Code:</label></br>
                <input type=\"text\" class=\"form-control\" name=\"postalCode\" value=\"{{customer.postalCode}}\"  maxlength=\"10\"></input> 
            </div>
        </div>
    </div>      

    <div class=\"border border-light rounded mt-2 p-3\">
        <div class=\"bg-light p-1\">
            <b>Payment Method</b>
        </div>                    
        <div class=\"row pb-1\">                        
            <div class=\"col-4\">
                <label class=\"col-form-label\">Select Payment Method:</label></br>
                <select type=\"text\" class=\"form-control\" name=\"paymentMethod\">
                    <option>Select Payment Method:</option>
                    <option>Cahs</option>
                    <option>Paypal</option>
                </select> 
            </div>                        
        </div>
    </div>    

    <div class=\"border border-light rounded mt-2 p-3\">
        <div class=\"bg-light p-1\">
            <b>Captcha</b>
        </div>                    
        <div class=\"row pb-1\">                        
            <div class=\"col-4\">                            
                <div class=\"g-recaptcha\" data-sitekey=\"6LfxzbcZAAAAAJGRJq8QtCvJdwdYRff920f6vkVA\"></div>
            </div>                        
        </div>
    </div> 
    <div class=\"text-center mb-4\">
        <input type=\"submit\" name=\"comfirmBooking\" value=\"Confirm Booking\"></input>
    </div>
{% endblock %}        
            

", "booking/step4.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\booking\\step4.html.twig");
    }
}
