<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/car_add_update.html.twig */
class __TwigTemplate_24f5e5bcbce368e46f59b3a619e286049d46499c33d99d2c4a9397613172c704 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'table' => [$this, 'block_table'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/masterAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("admin/masterAdmin.html.twig", "admin/car_add_update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_table($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $context["action"] = (((($context["operation"] ?? null) == "add")) ? ("Add") : ("Update"));
        // line 5
        echo "    <h2>";
        echo twig_escape_filter($this->env, twig_join_filter([0 => ($context["action"] ?? null), 1 => "Car"], " "), "html", null, true);
        echo "</h2>
    <div class=\"form-responsive\">
        <form method=\"post\" class=\"form form-sm\">                        
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Registration Number:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"registrationNo\" class=\"form-control\" value=\"";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["car"] ?? null), "registrationNo", [], "any", false, false, false, 11), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Make</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"make\" class=\"form-control\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["car"] ?? null), "make", [], "any", false, false, false, 17), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Model:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"model\" class=\"form-control\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["car"] ?? null), "model", [], "any", false, false, false, 23), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Mileage:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"mileage\" class=\"form-control\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["car"] ?? null), "mileage", [], "any", false, false, false, 29), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Engine Size:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"engineSize\" class=\"form-control\" value=\"";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["car"] ?? null), "engineSize", [], "any", false, false, false, 35), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Production Year:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"productionYear\" class=\"form-control\" value=\"";
        // line 41
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["car"] ?? null), "productionYear", [], "any", false, false, false, 41), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Color:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"color\" class=\"form-control\" value=\"";
        // line 47
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["car"] ?? null), "color", [], "any", false, false, false, 47), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Default Location:</label>
                <div class=\"col-sm-5\">
                    <select name=\"locationName\" class=\"form-control\" value=\"";
        // line 53
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["car"] ?? null), "defaultLocationId", [], "any", false, false, false, 53), "html", null, true);
        echo "\">                    
                        ";
        // line 54
        if (($context["locationList"] ?? null)) {
            // line 55
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["locationList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["location"]) {
                // line 56
                echo "                                <option value=";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["location"], "id", [], "any", false, false, false, 56), "html", null, true);
                echo ">";
                echo twig_escape_filter($this->env, twig_join_filter([0 => twig_get_attribute($this->env, $this->source, $context["location"], "street", [], "any", false, false, false, 56), 1 => twig_get_attribute($this->env, $this->source, $context["location"], "city", [], "any", false, false, false, 56)], " , "), "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['location'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "            
                        ";
        }
        // line 58
        echo "                            
                    </select>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Category Name:</label>
                <div class=\"col-sm-5\">
                    <select name=\"categoryName\" class=\"form-control\" value=\"";
        // line 65
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["car"] ?? null), "categorId", [], "any", false, false, false, 65), "html", null, true);
        echo "\"></input>
                        ";
        // line 66
        if (($context["categoryList"] ?? null)) {
            // line 67
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categoryList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 68
                echo "                                <option value=";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 68), "html", null, true);
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "categoryName", [], "any", false, false, false, 68), "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 69
            echo "            
                        ";
        }
        // line 70
        echo "                            
                    </select>
                </div>
            </div>
            <div class=\"row\">
            <div class=\"col-sm-8 d-flex justify-content-around\">
                <input type=\"submit\" id=\"submitCar\" name=\"submit\" value=\"";
        // line 76
        echo twig_escape_filter($this->env, ($context["action"] ?? null), "html", null, true);
        echo "\" class=\"btn btn-secondary\" style=\"width: 100px;\"></input>
                <input type=\"submit\" name=\"cancel\" value=\"Cancel\" class=\"btn btn-secondary\" style=\"width: 100px;\"></input>
            </div>
            </div>
        </form>
    </div>

";
    }

    // line 85
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 86
        echo "    <script src=\"../../js/carFormValidation.js\"></script>
";
    }

    public function getTemplateName()
    {
        return "admin/car_add_update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  212 => 86,  208 => 85,  196 => 76,  188 => 70,  184 => 69,  173 => 68,  168 => 67,  166 => 66,  162 => 65,  153 => 58,  149 => 57,  138 => 56,  133 => 55,  131 => 54,  127 => 53,  118 => 47,  109 => 41,  100 => 35,  91 => 29,  82 => 23,  73 => 17,  64 => 11,  54 => 5,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"admin/masterAdmin.html.twig\" %}

{% block table %}
    {% set action = (operation == 'add' ? \"Add\" : \"Update\") %}
    <h2>{{[action, 'Car'] | join(' ') }}</h2>
    <div class=\"form-responsive\">
        <form method=\"post\" class=\"form form-sm\">                        
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Registration Number:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"registrationNo\" class=\"form-control\" value=\"{{car.registrationNo}}\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Make</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"make\" class=\"form-control\" value=\"{{car.make}}\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Model:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"model\" class=\"form-control\" value=\"{{car.model}}\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Mileage:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"mileage\" class=\"form-control\" value=\"{{car.mileage}}\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Engine Size:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"engineSize\" class=\"form-control\" value=\"{{car.engineSize}}\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Production Year:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"productionYear\" class=\"form-control\" value=\"{{car.productionYear}}\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Color:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"color\" class=\"form-control\" value=\"{{car.color}}\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Default Location:</label>
                <div class=\"col-sm-5\">
                    <select name=\"locationName\" class=\"form-control\" value=\"{{car.defaultLocationId}}\">                    
                        {% if locationList %}
                            {% for location in locationList %}
                                <option value={{location.id}}>{{[location.street , location.city] | join(' , ')}}</option>
                            {% endfor %}            
                        {% endif %}                            
                    </select>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Category Name:</label>
                <div class=\"col-sm-5\">
                    <select name=\"categoryName\" class=\"form-control\" value=\"{{car.categorId}}\"></input>
                        {% if categoryList %}
                            {% for category in categoryList %}
                                <option value={{category.id}}>{{category.categoryName}}</option>
                            {% endfor %}            
                        {% endif %}                            
                    </select>
                </div>
            </div>
            <div class=\"row\">
            <div class=\"col-sm-8 d-flex justify-content-around\">
                <input type=\"submit\" id=\"submitCar\" name=\"submit\" value=\"{{action}}\" class=\"btn btn-secondary\" style=\"width: 100px;\"></input>
                <input type=\"submit\" name=\"cancel\" value=\"Cancel\" class=\"btn btn-secondary\" style=\"width: 100px;\"></input>
            </div>
            </div>
        </form>
    </div>

{% endblock %}

{% block js %}
    <script src=\"../../js/carFormValidation.js\"></script>
{% endblock %}", "admin/car_add_update.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\admin\\car_add_update.html.twig");
    }
}
