<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/category_add_update.html.twig */
class __TwigTemplate_b22be5d4391e3f7e60bd6e1980873aef49937c7a2502863c071b122fa250a77d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'table' => [$this, 'block_table'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/masterAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("admin/masterAdmin.html.twig", "admin/category_add_update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_table($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $context["action"] = (((($context["operation"] ?? null) == "add")) ? ("Add") : ("Update"));
        // line 5
        echo "    <h2>";
        echo twig_escape_filter($this->env, twig_join_filter([0 => ($context["action"] ?? null), 1 => "Category"], " "), "html", null, true);
        echo "</h2>
    <div class=\"form-responsive\">
        <form method=\"post\" class=\"form form-sm\">                        
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Category Name:</label>
                <div class=\"col-sm-5\">                    
                    <input type=\"text\" name=\"categoryName\" class=\"form-control\" value=\"";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "categoryName", [], "any", false, false, false, 11), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Number Of Passengers:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"passengersNo\" class=\"form-control\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "passengersNo", [], "any", false, false, false, 17), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Number Of Doors:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"doorsNo\" class=\"form-control\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "doorsNo", [], "any", false, false, false, 23), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Price / Day:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"costPerDay\" class=\"form-control\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 29), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"row\">
            <div class=\"col-sm-8 d-flex justify-content-around\">
                <input type=\"submit\" name=\"submit\" id=\"submitCategory\" value=\"";
        // line 34
        echo twig_escape_filter($this->env, ($context["action"] ?? null), "html", null, true);
        echo "\" class=\"btn btn-secondary\" style=\"width: 100px;\"></input>
                <input type=\"submit\" name=\"cancel\" value=\"Cancel\" class=\"btn btn-secondary\" style=\"width: 100px;\"></input>
            </div>
            </div>
        </form>
    </div>

";
    }

    public function getTemplateName()
    {
        return "admin/category_add_update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 34,  90 => 29,  81 => 23,  72 => 17,  63 => 11,  53 => 5,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"admin/masterAdmin.html.twig\" %}

{% block table %}
    {% set action = (operation == 'add' ? \"Add\" : \"Update\") %}
    <h2>{{[action, 'Category'] | join(' ') }}</h2>
    <div class=\"form-responsive\">
        <form method=\"post\" class=\"form form-sm\">                        
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Category Name:</label>
                <div class=\"col-sm-5\">                    
                    <input type=\"text\" name=\"categoryName\" class=\"form-control\" value=\"{{category.categoryName}}\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Number Of Passengers:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"passengersNo\" class=\"form-control\" value=\"{{category.passengersNo}}\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Number Of Doors:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"doorsNo\" class=\"form-control\" value=\"{{category.doorsNo}}\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Price / Day:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"costPerDay\" class=\"form-control\" value=\"{{category.costPerDay}}\"></input>
                </div>
            </div>
            <div class=\"row\">
            <div class=\"col-sm-8 d-flex justify-content-around\">
                <input type=\"submit\" name=\"submit\" id=\"submitCategory\" value=\"{{action}}\" class=\"btn btn-secondary\" style=\"width: 100px;\"></input>
                <input type=\"submit\" name=\"cancel\" value=\"Cancel\" class=\"btn btn-secondary\" style=\"width: 100px;\"></input>
            </div>
            </div>
        </form>
    </div>

{% endblock %}", "admin/category_add_update.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\admin\\category_add_update.html.twig");
    }
}
