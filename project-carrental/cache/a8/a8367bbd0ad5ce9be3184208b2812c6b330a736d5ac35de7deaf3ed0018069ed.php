<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/masterAdmin.html.twig */
class __TwigTemplate_df9ae06bafad57e3d2d7876fd9c1abb536febf61ec64a68f2edfeec6f637ecce extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'buttonAdd' => [$this, 'block_buttonAdd'],
            'table' => [$this, 'block_table'],
            'dialog' => [$this, 'block_dialog'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">    
    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css\" integrity=\"sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk\" crossorigin=\"anonymous\">
    
    <script src=\"https://code.jquery.com/jquery-3.4.1.min.js\" integrity=\"sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=\" crossorigin=\"anonymous\"></script>
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.js\"></script>
    
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
            font-size: 3.5rem;
            }
        }
    </style>
    <link rel=\"stylesheet\" href=\"/css/admin.css\">
    <title>Admin</title>
    
    ";
        // line 31
        $this->displayBlock('head', $context, $blocks);
        // line 32
        echo "
</head>

<body>
    <nav class=\"navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow\">
        <a class=\"navbar-brand col-md-3 col-lg-2 mr-0 px-3\" href=\"#\">IPD Car Rental</a>
        <button class=\"navbar-toggler position-absolute d-md-none collapsed\" type=\"button\" data-toggle=\"collapse\" data-target=\"#sidebarMenu\" aria-controls=\"sidebarMenu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"></span>
        </button>
        ";
        // line 42
        echo "        <ul class=\"navbar-nav px-3\">
            <li class=\"nav-item text-nowrap\">
                <a class=\"nav-link\" href=\"#\">Sign out</a>
            </li>
        </ul>
    </nav>

    <div class=\"container-fluid\">
        <div class=\"row\">
            <nav id=\"sidebarMenu\" class=\"col-md-3 col-lg-2 d-md-block bg-light sidebar collapse\">
                <div class=\"sidebar-sticky pt-3\">
                    <ul class=\"nav flex-column\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/admin\">
                                <span><img src=\"/image/home.png\" width=\"15\", height=\"15\"/>&nbsp&nbsp</span>Dashboard <span class=\"sr-only\">(current)</span>
                            </a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/admin/reservation/list\">
                                <span><img src=\"/image/reservation.png\" width=\"15\", height=\"15\"/>&nbsp&nbsp</span>Reservations
                            </a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/admin/category/list\">
                                <span><img src=\"/image/category.png\" width=\"15\", height=\"15\"/>&nbsp&nbsp</span>Categories
                            </a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/admin/car/list\">
                                <span><img src=\"/image/car.png\" width=\"15\", height=\"15\" />&nbsp&nbsp</span>Cars
                            </a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/admin/location/list\">
                                <span><img src=\"/image/location.png\" width=\"15\", height=\"15\" />&nbsp&nbsp</span>Locations
                            </a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/admin/customer/list\">
                                <span><img src=\"/image/customer.png\" width=\"15\", height=\"15\" />&nbsp&nbsp</span>Customers
                            </a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/admin/user/list\">
                                <span><img src=\"/image/user.png\" width=\"15\", height=\"15\" />&nbsp&nbsp</span>Users
                            </a>
                        </li>
                    </ul>        
                </div>
            </nav>

            <main role=\"main\" class=\"col-md-9 ml-sm-auto col-lg-10 px-md-4\">
                <div class=\"d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom\">             
                    ";
        // line 95
        $this->displayBlock('buttonAdd', $context, $blocks);
        // line 97
        echo "                </div>
                
                <div class=\"tableDiv\" style=\"height:100%\">
                    ";
        // line 100
        $this->displayBlock('table', $context, $blocks);
        // line 102
        echo "                </div>

";
        // line 121
        echo "            </main>

            ";
        // line 123
        $this->displayBlock('dialog', $context, $blocks);
        // line 124
        echo "  

        </div>
    </div>    
    
    
    ";
        // line 130
        $this->displayBlock('js', $context, $blocks);
        // line 131
        echo "</body>
</html>";
    }

    // line 31
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 95
    public function block_buttonAdd($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 96
        echo "                    ";
    }

    // line 100
    public function block_table($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 101
        echo "                    ";
    }

    // line 123
    public function block_dialog($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 124
        echo "            ";
    }

    // line 130
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "admin/masterAdmin.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  206 => 130,  202 => 124,  198 => 123,  194 => 101,  190 => 100,  186 => 96,  182 => 95,  176 => 31,  171 => 131,  169 => 130,  161 => 124,  159 => 123,  155 => 121,  151 => 102,  149 => 100,  144 => 97,  142 => 95,  87 => 42,  76 => 32,  74 => 31,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">    
    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css\" integrity=\"sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk\" crossorigin=\"anonymous\">
    
    <script src=\"https://code.jquery.com/jquery-3.4.1.min.js\" integrity=\"sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=\" crossorigin=\"anonymous\"></script>
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.js\"></script>
    
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
            font-size: 3.5rem;
            }
        }
    </style>
    <link rel=\"stylesheet\" href=\"/css/admin.css\">
    <title>Admin</title>
    
    {% block head %}{% endblock %}

</head>

<body>
    <nav class=\"navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow\">
        <a class=\"navbar-brand col-md-3 col-lg-2 mr-0 px-3\" href=\"#\">IPD Car Rental</a>
        <button class=\"navbar-toggler position-absolute d-md-none collapsed\" type=\"button\" data-toggle=\"collapse\" data-target=\"#sidebarMenu\" aria-controls=\"sidebarMenu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"></span>
        </button>
        {#<input class=\"form-control form-control-dark w-100\" type=\"text\" placeholder=\"Search\" aria-label=\"Search\">#}
        <ul class=\"navbar-nav px-3\">
            <li class=\"nav-item text-nowrap\">
                <a class=\"nav-link\" href=\"#\">Sign out</a>
            </li>
        </ul>
    </nav>

    <div class=\"container-fluid\">
        <div class=\"row\">
            <nav id=\"sidebarMenu\" class=\"col-md-3 col-lg-2 d-md-block bg-light sidebar collapse\">
                <div class=\"sidebar-sticky pt-3\">
                    <ul class=\"nav flex-column\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/admin\">
                                <span><img src=\"/image/home.png\" width=\"15\", height=\"15\"/>&nbsp&nbsp</span>Dashboard <span class=\"sr-only\">(current)</span>
                            </a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/admin/reservation/list\">
                                <span><img src=\"/image/reservation.png\" width=\"15\", height=\"15\"/>&nbsp&nbsp</span>Reservations
                            </a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/admin/category/list\">
                                <span><img src=\"/image/category.png\" width=\"15\", height=\"15\"/>&nbsp&nbsp</span>Categories
                            </a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/admin/car/list\">
                                <span><img src=\"/image/car.png\" width=\"15\", height=\"15\" />&nbsp&nbsp</span>Cars
                            </a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/admin/location/list\">
                                <span><img src=\"/image/location.png\" width=\"15\", height=\"15\" />&nbsp&nbsp</span>Locations
                            </a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/admin/customer/list\">
                                <span><img src=\"/image/customer.png\" width=\"15\", height=\"15\" />&nbsp&nbsp</span>Customers
                            </a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/admin/user/list\">
                                <span><img src=\"/image/user.png\" width=\"15\", height=\"15\" />&nbsp&nbsp</span>Users
                            </a>
                        </li>
                    </ul>        
                </div>
            </nav>

            <main role=\"main\" class=\"col-md-9 ml-sm-auto col-lg-10 px-md-4\">
                <div class=\"d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom\">             
                    {% block buttonAdd %}
                    {% endblock %}
                </div>
                
                <div class=\"tableDiv\" style=\"height:100%\">
                    {% block table %}
                    {% endblock %}
                </div>

{#                <div class=\"pageDiv\">
                    {% if currentPage != 1 %}
                        <a class=\"pageNav\" href=\"?page={{ currentPage - 1 }}\"> Previous </a>
                    {% endif %}

                    {% for page in 1 .. totalPages %}
                        {% if page != currentPage %}
                            <a href=\"?page={{page}}\">{{page}}</a>
                        {% else %}
                            <span>{{page}}</span>
                        {% endif %}
                    {% endfor %}

                    {% if currentPage != totalPages %}
                        <a class=\"pageNav\" href=\"?page={{ currentPage + 1 }}\"> Next </a>
                    {% endif %}
                </div>
#}            </main>

            {% block dialog %}
            {% endblock %}  

        </div>
    </div>    
    
    
    {% block js %}{% endblock %}
</body>
</html>", "admin/masterAdmin.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\admin\\masterAdmin.html.twig");
    }
}
