<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* booking/selectViewCategory.html.twig */
class __TwigTemplate_d88808c95651f81ac7506c85bd0a845f3e04a55a86400f4dcba6095f08237eff extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["categoryList"] ?? null)) {
            // line 2
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categoryList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 3
                echo "        <tr class=\"border border-light rounded\">
            <td class=\"form-responsive\">
                <form method=\"post\" class=\"form form-sm\" action=\"/booking/step3/";
                // line 5
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 5), "html", null, true);
                echo "\">
                    <div class=\"row\">
                        <div class=\"col-4\">
                            <img src=";
                // line 8
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "photoFilePath", [], "any", false, false, false, 8), "html", null, true);
                echo " width=\"154\" height=\"98\" class=\"img-responsive\"/>
                        </div>
                        <div class=\"col-5\">
                            <h5>";
                // line 11
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "categoryName", [], "any", false, false, false, 11), "html", null, true);
                echo "</h5>
                            <p>Example of this range:</p>
                            <p>";
                // line 13
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "carExample", [], "any", false, false, false, 13), "html", null, true);
                echo "</p>
                        </div>
                        <div class=\"col-3 text-center\">
                            <div class=\"bg-light text-center\">
                                <p>Total Price:</p>
                                <p><b>";
                // line 18
                echo twig_escape_filter($this->env, (("\$CAD " . (twig_get_attribute($this->env, $this->source, $context["category"], "costPerDay", [], "any", false, false, false, 18) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 18), "d", [], "any", false, false, false, 18))) . ".00"), "html", null, true);
                echo "</b></p>
                            </div>
                            <input type=\"submit\" name=\"continue\" value=\"CONTINUE\" class=\"btn-sm btn-primary\">
                        </div>
                    </div>
                </form>
            </td>                                    
        </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 26
            echo "            
";
        }
        // line 27
        echo "   ";
    }

    public function getTemplateName()
    {
        return "booking/selectViewCategory.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 27,  88 => 26,  73 => 18,  65 => 13,  60 => 11,  54 => 8,  48 => 5,  44 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if categoryList %}
    {% for category in categoryList %}
        <tr class=\"border border-light rounded\">
            <td class=\"form-responsive\">
                <form method=\"post\" class=\"form form-sm\" action=\"/booking/step3/{{category.id}}\">
                    <div class=\"row\">
                        <div class=\"col-4\">
                            <img src={{category.photoFilePath}} width=\"154\" height=\"98\" class=\"img-responsive\"/>
                        </div>
                        <div class=\"col-5\">
                            <h5>{{category.categoryName}}</h5>
                            <p>Example of this range:</p>
                            <p>{{category.carExample}}</p>
                        </div>
                        <div class=\"col-3 text-center\">
                            <div class=\"bg-light text-center\">
                                <p>Total Price:</p>
                                <p><b>{{'\$CAD ' ~ category.costPerDay * booking.duration.d ~ '.00'}}</b></p>
                            </div>
                            <input type=\"submit\" name=\"continue\" value=\"CONTINUE\" class=\"btn-sm btn-primary\">
                        </div>
                    </div>
                </form>
            </td>                                    
        </tr>
    {% endfor %}            
{% endif %}   ", "booking/selectViewCategory.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\booking\\selectViewCategory.html.twig");
    }
}
