<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /admin/location_singlepage.html.twig */
class __TwigTemplate_6145c03c216432310618cbf00a6d134c0be659eb1b881eef5e4e2ead2bb1553a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["locationList"] ?? null)) {
            // line 2
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["locationList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["location"]) {
                // line 3
                echo "        <tr>
            <td>";
                // line 4
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["location"], "locationName", [], "any", false, false, false, 4), "html", null, true);
                echo "</td>
            <td>";
                // line 5
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["location"], "street", [], "any", false, false, false, 5), "html", null, true);
                echo "</td>
            <td>";
                // line 6
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["location"], "city", [], "any", false, false, false, 6), "html", null, true);
                echo "</td>
            <td>";
                // line 7
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["location"], "province", [], "any", false, false, false, 7), "html", null, true);
                echo "</td> 
            <td>";
                // line 8
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["location"], "postalCode", [], "any", false, false, false, 8), "html", null, true);
                echo "</td>  
            <td><a href=\"/admin/location/update/";
                // line 9
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["location"], "id", [], "any", false, false, false, 9), "html", null, true);
                echo "\"><img src=\"../../image/edit.png\" width=\"20\", height=\"20\"></a>
                <a href=\"/admin/location/delete/";
                // line 10
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["location"], "id", [], "any", false, false, false, 10), "html", null, true);
                echo "\" class=\"confirmation\"><img src=\"../../image/delete.jpg\" width=\"20\", height=\"20\"></a>
            </td>                         
        </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['location'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "            
";
        }
        // line 14
        echo "  ";
    }

    public function getTemplateName()
    {
        return "/admin/location_singlepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 14,  81 => 13,  71 => 10,  67 => 9,  63 => 8,  59 => 7,  55 => 6,  51 => 5,  47 => 4,  44 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if locationList %}
    {% for location in locationList %}
        <tr>
            <td>{{location.locationName}}</td>
            <td>{{location.street}}</td>
            <td>{{location.city}}</td>
            <td>{{location.province}}</td> 
            <td>{{location.postalCode}}</td>  
            <td><a href=\"/admin/location/update/{{location.id}}\"><img src=\"../../image/edit.png\" width=\"20\", height=\"20\"></a>
                <a href=\"/admin/location/delete/{{location.id}}\" class=\"confirmation\"><img src=\"../../image/delete.jpg\" width=\"20\", height=\"20\"></a>
            </td>                         
        </tr>
    {% endfor %}            
{% endif %}  ", "/admin/location_singlepage.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\admin\\location_singlepage.html.twig");
    }
}
