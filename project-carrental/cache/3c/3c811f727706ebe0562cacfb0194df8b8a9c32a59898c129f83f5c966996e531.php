<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* login_success.html.twig */
class __TwigTemplate_6b76ebb449b12cd3250d4fc66d6bcc76f482c28edab90f768619ac0c88e39251 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "login_success.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Login Success";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <meta http-equiv=\"refresh\" content=\"2;URL='/'\" />
";
    }

    // line 9
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 
    <p>Login successfully. Click to continue.<a href='/'></a>You will be automatically redirected in 2 seconds</p>
    ";
        // line 11
        if (twig_get_attribute($this->env, $this->source, ($context["userSession"] ?? null), "isAdmin", [], "any", false, false, false, 11)) {
            // line 12
            echo "        <p>You can click to access <a href=\"/admin\">Administration Page.</a><p>
    ";
        }
    }

    public function getTemplateName()
    {
        return "login_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 12,  71 => 11,  64 => 9,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Login Success{% endblock %}

{% block head %}
    <meta http-equiv=\"refresh\" content=\"2;URL='/'\" />
{% endblock %}

{% block content %} 
    <p>Login successfully. Click to continue.<a href='/'></a>You will be automatically redirected in 2 seconds</p>
    {% if userSession.isAdmin %}
        <p>You can click to access <a href=\"/admin\">Administration Page.</a><p>
    {% endif %}
{% endblock %}
", "login_success.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\login_success.html.twig");
    }
}
