<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* aboutus.html.twig */
class __TwigTemplate_24f5ee7f919eab9f4691f794938bf57fc33cf91b887557c6952dc3ff3fb3032e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "aboutus.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Logout";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "<script src=\"../js/contact_custom.js\"></script>
<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/contact_styles.css\">
<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/contact_responsive.css\">
<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/responsive.css\">
<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/main_styles.css\">
";
    }

    // line 13
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "    <div class=\"container contact_container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col\">
\t\t\t\t<div class=\"breadcrumbs d<h5>-flex flex<h5>-row align<h5>-items<h5>-center\">
\t\t\t\t\t<ul>
\t\t\t\t\t\t<a href=\"/\">Home</a>
\t\t\t\t\t\t<li class=\"active\"><a href=\"/aboutus\"><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>aboutus</a>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"row\">
\t\t\t<div class=\"col\">
\t\t\t\t<div id=\"google_map\">
\t\t\t\t\t<div class=\"aboutus\">
\t\t\t\t\t\t<h4>IPD carrental has a new face. We are new in business, we decided to give a fresher look to our brand and our services. With our fully renewed fleet of vehicles, we are ready to meet all expectations and requirements.</h4>
\t\t\t\t\t\t<h3>Why choose us?</h3>
\t\t\t\t\t\t<h5>-  If you want to book directly through a supplier, and not through a broker – choose IPD carrental for Rentiing A Car;</h5>
\t\t\t\t\t\t<h5>-  this will give you better flexibility in terms of vehicle choices;</h5>
\t\t\t\t\t\t<h5>-  vehicle make and model will be confirmed, and not “similar” to those you selected;</h5>
\t\t\t\t\t\t<h5>-  you can directly negotiate some of the terms and conditions, payment options, especially if you require unique or long term rental service;</h5>
\t\t\t\t\t\t<h5>-  you can book “commission free”;</h5>
\t\t\t\t\t\t<h5>-   you can reach us 24/7 on our mobile numbers;</h5>
\t\t\t\t\t\t<h5>-  you can call us free from the “Free call” service on our website;</h5>
\t\t\t\t\t\t<h2>We pride ourselves on personalized service, great cars and excellent rates.</h2>\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

";
    }

    public function getTemplateName()
    {
        return "aboutus.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 14,  68 => 13,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Logout{% endblock %}

{% block head %}
<script src=\"../js/contact_custom.js\"></script>
<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/contact_styles.css\">
<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/contact_responsive.css\">
<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/responsive.css\">
<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/main_styles.css\">
{% endblock %}

{% block content %}
    <div class=\"container contact_container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col\">
\t\t\t\t<div class=\"breadcrumbs d<h5>-flex flex<h5>-row align<h5>-items<h5>-center\">
\t\t\t\t\t<ul>
\t\t\t\t\t\t<a href=\"/\">Home</a>
\t\t\t\t\t\t<li class=\"active\"><a href=\"/aboutus\"><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>aboutus</a>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"row\">
\t\t\t<div class=\"col\">
\t\t\t\t<div id=\"google_map\">
\t\t\t\t\t<div class=\"aboutus\">
\t\t\t\t\t\t<h4>IPD carrental has a new face. We are new in business, we decided to give a fresher look to our brand and our services. With our fully renewed fleet of vehicles, we are ready to meet all expectations and requirements.</h4>
\t\t\t\t\t\t<h3>Why choose us?</h3>
\t\t\t\t\t\t<h5>-  If you want to book directly through a supplier, and not through a broker – choose IPD carrental for Rentiing A Car;</h5>
\t\t\t\t\t\t<h5>-  this will give you better flexibility in terms of vehicle choices;</h5>
\t\t\t\t\t\t<h5>-  vehicle make and model will be confirmed, and not “similar” to those you selected;</h5>
\t\t\t\t\t\t<h5>-  you can directly negotiate some of the terms and conditions, payment options, especially if you require unique or long term rental service;</h5>
\t\t\t\t\t\t<h5>-  you can book “commission free”;</h5>
\t\t\t\t\t\t<h5>-   you can reach us 24/7 on our mobile numbers;</h5>
\t\t\t\t\t\t<h5>-  you can call us free from the “Free call” service on our website;</h5>
\t\t\t\t\t\t<h2>We pride ourselves on personalized service, great cars and excellent rates.</h2>\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

{% endblock %}
", "aboutus.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\aboutus.html.twig");
    }
}
