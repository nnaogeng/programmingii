<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /admin/category_singlepage.html.twig */
class __TwigTemplate_3a066651e25620504e57c93c03d7d1a3b1d3d9a9b49574a835a608f327f4ec89 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["categoryList"] ?? null)) {
            // line 2
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categoryList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 3
                echo "        <tr>
            <td>";
                // line 4
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "categoryName", [], "any", false, false, false, 4), "html", null, true);
                echo "</td>
            <td>";
                // line 5
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "passengersNo", [], "any", false, false, false, 5), "html", null, true);
                echo "</td>
            <td>";
                // line 6
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "doorsNo", [], "any", false, false, false, 6), "html", null, true);
                echo "</td>
            <td>";
                // line 7
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "costPerDay", [], "any", false, false, false, 7), "html", null, true);
                echo "</td>   
            <td><a href=\"/admin/category/update/";
                // line 8
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 8), "html", null, true);
                echo "\"><img src=\"/image/edit.png\" width=\"20\", height=\"20\"></a>
                <a href=\"/admin/category/delete/";
                // line 9
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 9), "html", null, true);
                echo "\" class=\"confirmation\"><img src=\"/image/delete.jpg\" width=\"20\", height=\"20\"></a>
            </td>                         
        </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "            
";
        }
        // line 13
        echo "  ";
    }

    public function getTemplateName()
    {
        return "/admin/category_singlepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 13,  77 => 12,  67 => 9,  63 => 8,  59 => 7,  55 => 6,  51 => 5,  47 => 4,  44 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if categoryList %}
    {% for category in categoryList %}
        <tr>
            <td>{{category.categoryName}}</td>
            <td>{{category.passengersNo}}</td>
            <td>{{category.doorsNo}}</td>
            <td>{{category.costPerDay}}</td>   
            <td><a href=\"/admin/category/update/{{category.id}}\"><img src=\"/image/edit.png\" width=\"20\", height=\"20\"></a>
                <a href=\"/admin/category/delete/{{category.id}}\" class=\"confirmation\"><img src=\"/image/delete.jpg\" width=\"20\", height=\"20\"></a>
            </td>                         
        </tr>
    {% endfor %}            
{% endif %}  ", "/admin/category_singlepage.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\admin\\category_singlepage.html.twig");
    }
}
