<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /admin/category_list.html.twig */
class __TwigTemplate_e27adbd72cb8f6ad4a7cf9a264f773f8a19eb57c909f1e31bb3ee0af92fbb8af extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'buttonAdd' => [$this, 'block_buttonAdd'],
            'table' => [$this, 'block_table'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/masterAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("admin/masterAdmin.html.twig", "/admin/category_list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    
    <script>
        var currPageNo = ";
        // line 6
        echo twig_escape_filter($this->env, ($context["pageNo"] ?? null), "html", null, true);
        echo ";
        var maxPages = ";
        // line 7
        echo twig_escape_filter($this->env, ($context["maxPages"] ?? null), "html", null, true);
        echo ";
    </script>
    <script src=\"/js/admin_pagination.js\"></script>
    <script>
        \$(document).ready(function() {
            \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
                console.log(\"Ajax error occured on \" + settings.url);
                alert(\"Ajax error occured\");
            });

            loadPage(";
        // line 17
        echo twig_escape_filter($this->env, ($context["pageNo"] ?? null), "html", null, true);
        echo ", true, '/admin/category/list/','/admin/category/list/singlepage/');
        });
    </script>
    
";
    }

    // line 23
    public function block_buttonAdd($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "    <a href=\"/admin/category/add\"><input type=\"submit\" name=\"addCategory\" value=\"+ Add Category\"></input></a>
";
    }

    // line 27
    public function block_table($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "    <h2>Categories</h2>
    <div class=\"table-responsive\">
        <table class=\"table table-striped table-sm\">
            <thead>
                <tr>
                    <th>Type</th>
                    <th>Number Of Passengers</th>
                    <th>Number Of Doors</th>
                    <th>Rate / Day</th>                    
                </tr>
            </thead> 
            <tbody id=\"tableBody\">
                                                                  
            </tbody>
        </table>

        <div class=\"pageNavigation\">
            <br>
            <span id=\"pageNavPrev\" onclick=\"loadPage(currPageNo-1, true, '/admin/category/list/','/admin/category/list/singlepage/')\">Previous</span>
            ";
        // line 47
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, ($context["maxPages"] ?? null)));
        foreach ($context['_seq'] as $context["_key"] => $context["no"]) {
            // line 48
            echo "                <span id=\"pageNav";
            echo twig_escape_filter($this->env, $context["no"], "html", null, true);
            echo "\" onclick=\"loadPage(";
            echo twig_escape_filter($this->env, $context["no"], "html", null, true);
            echo ", true, '/admin/category/list/','/admin/category/list/singlepage/')\">";
            echo twig_escape_filter($this->env, $context["no"], "html", null, true);
            echo "</span>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['no'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "            <span  id=\"pageNavNext\" onclick=\"loadPage(currPageNo+1, true, '/admin/category/list/','/admin/category/list/singlepage/')\">Next</span>
        </div>

    </div>
";
    }

    // line 56
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 57
        echo "    <script type=\"text/javascript\">
        var elems = document.getElementsByClassName('confirmation');
        var confirmIt = function (e) {
            if (!confirm('Are you sure to delete selected entry?')) e.preventDefault();
        };
        for (var i = 0, l = elems.length; i < l; i++) {
            elems[i].addEventListener('click', confirmIt, false);
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "/admin/category_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 57,  142 => 56,  134 => 50,  121 => 48,  117 => 47,  96 => 28,  92 => 27,  87 => 24,  83 => 23,  74 => 17,  61 => 7,  57 => 6,  53 => 4,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"admin/masterAdmin.html.twig\" %}

{% block head %}
    
    <script>
        var currPageNo = {{pageNo}};
        var maxPages = {{maxPages}};
    </script>
    <script src=\"/js/admin_pagination.js\"></script>
    <script>
        \$(document).ready(function() {
            \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
                console.log(\"Ajax error occured on \" + settings.url);
                alert(\"Ajax error occured\");
            });

            loadPage({{pageNo}}, true, '/admin/category/list/','/admin/category/list/singlepage/');
        });
    </script>
    
{% endblock %}

{% block buttonAdd %}
    <a href=\"/admin/category/add\"><input type=\"submit\" name=\"addCategory\" value=\"+ Add Category\"></input></a>
{% endblock %}

{% block table %}
    <h2>Categories</h2>
    <div class=\"table-responsive\">
        <table class=\"table table-striped table-sm\">
            <thead>
                <tr>
                    <th>Type</th>
                    <th>Number Of Passengers</th>
                    <th>Number Of Doors</th>
                    <th>Rate / Day</th>                    
                </tr>
            </thead> 
            <tbody id=\"tableBody\">
                                                                  
            </tbody>
        </table>

        <div class=\"pageNavigation\">
            <br>
            <span id=\"pageNavPrev\" onclick=\"loadPage(currPageNo-1, true, '/admin/category/list/','/admin/category/list/singlepage/')\">Previous</span>
            {% for no in 1 .. maxPages %}
                <span id=\"pageNav{{no}}\" onclick=\"loadPage({{no}}, true, '/admin/category/list/','/admin/category/list/singlepage/')\">{{no}}</span>
            {% endfor %}
            <span  id=\"pageNavNext\" onclick=\"loadPage(currPageNo+1, true, '/admin/category/list/','/admin/category/list/singlepage/')\">Next</span>
        </div>

    </div>
{% endblock %}

{% block js %}
    <script type=\"text/javascript\">
        var elems = document.getElementsByClassName('confirmation');
        var confirmIt = function (e) {
            if (!confirm('Are you sure to delete selected entry?')) e.preventDefault();
        };
        for (var i = 0, l = elems.length; i < l; i++) {
            elems[i].addEventListener('click', confirmIt, false);
        }
    </script>
{% endblock %}", "/admin/category_list.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\admin\\category_list.html.twig");
    }
}
