<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /admin/user_list.html.twig */
class __TwigTemplate_2f32e21ace2980b3e7a157fb8a3368fe5b1874f7b831717f8077545180d1cf79 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'buttonAdd' => [$this, 'block_buttonAdd'],
            'table' => [$this, 'block_table'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/masterAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("admin/masterAdmin.html.twig", "/admin/user_list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    
    <script>
        var currPageNo = ";
        // line 6
        echo twig_escape_filter($this->env, ($context["pageNo"] ?? null), "html", null, true);
        echo ";
        var maxPages = ";
        // line 7
        echo twig_escape_filter($this->env, ($context["maxPages"] ?? null), "html", null, true);
        echo ";
    </script>
    <script src=\"/js/admin_pagination.js\"></script>
    <script>
        \$(document).ready(function() {
            \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
                console.log(\"Ajax error occured on \" + settings.url);
                alert(\"Ajax error occured\");
            });

            loadPage(";
        // line 17
        echo twig_escape_filter($this->env, ($context["pageNo"] ?? null), "html", null, true);
        echo ", true, '/admin/user/list/','/admin/user/list/singlepage/');
        });
    </script>
    
";
    }

    // line 23
    public function block_buttonAdd($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "    <a href=\"/admin/user/add\"><input type=\"submit\" name=\"addUser\" value=\"+ Add User\"></input></a>
    <button id=\"btnDelete\" data-value=1>Show Popup</button>
    
";
    }

    // line 29
    public function block_table($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo "    <h2>Users</h2>
    <div class=\"table-responsive\">
        <table class=\"table table-striped table-sm\">
            <thead>
                <tr class=\"table-secondary\">
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Admin</th> 
                    <th></th>                   
                </tr>
            </thead>
            <tbody id=\"tableBody\">
                                                                                       
            </tbody>
        </table>  

        <div class=\"pageNavigation\">
            <br>
            <span id=\"pageNavPrev\" onclick=\"loadPage(currPageNo-1, true, '/admin/user/list/','/admin/user/list/singlepage/' )\">Previous</span>
            ";
        // line 50
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, ($context["maxPages"] ?? null)));
        foreach ($context['_seq'] as $context["_key"] => $context["no"]) {
            // line 51
            echo "                <span id=\"pageNav";
            echo twig_escape_filter($this->env, $context["no"], "html", null, true);
            echo "\" onclick=\"loadPage(";
            echo twig_escape_filter($this->env, $context["no"], "html", null, true);
            echo ", true, '/admin/user/list/','/admin/user/list/singlepage/')\">";
            echo twig_escape_filter($this->env, $context["no"], "html", null, true);
            echo "</span>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['no'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "            <span  id=\"pageNavNext\" onclick=\"loadPage(currPageNo+1, true, '/admin/user/list/','/admin/user/list/singlepage/')\">Next</span>
        </div> 
    </div>
";
    }

    // line 58
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 59
        echo "<script>
/*    \$(document).ready(function(){
        var passedValue;
        \$(\"#dialogConfirm\").dialog({
            resizable: false,
            height:140,
            modal: true,
            buttons: {
                \"Delete\": function() {
                    alert(\"passed values is :\"+ passedValue); 

                    \$( this ).dialog( \"close\" );
                },
                Cancel: function() {
                \$( this ).dialog( \"close\" );
                }
            }
        });

        \$(\"#btnDelete\").click(function(){
            passedValue = \$(this).data(\"value\");
        
            \$(\"#dialogConfirm\" ).dialog(\"open\");
        });
    });
    <div id=\"dialogConfirm\" style=\"display: none\" align = \"center\">
        Are you sure to delete selected item?
    </div>
    <button class=\"btn-delete\" data-value=";
        // line 87
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 87), "html", null, true);
        echo ">Show Popup</button>*/
</script>

";
    }

    public function getTemplateName()
    {
        return "/admin/user_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 87,  148 => 59,  144 => 58,  137 => 53,  124 => 51,  120 => 50,  98 => 30,  94 => 29,  87 => 24,  83 => 23,  74 => 17,  61 => 7,  57 => 6,  53 => 4,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"admin/masterAdmin.html.twig\" %}

{% block head %}
    
    <script>
        var currPageNo = {{pageNo}};
        var maxPages = {{maxPages}};
    </script>
    <script src=\"/js/admin_pagination.js\"></script>
    <script>
        \$(document).ready(function() {
            \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
                console.log(\"Ajax error occured on \" + settings.url);
                alert(\"Ajax error occured\");
            });

            loadPage({{pageNo}}, true, '/admin/user/list/','/admin/user/list/singlepage/');
        });
    </script>
    
{% endblock %}

{% block buttonAdd %}
    <a href=\"/admin/user/add\"><input type=\"submit\" name=\"addUser\" value=\"+ Add User\"></input></a>
    <button id=\"btnDelete\" data-value=1>Show Popup</button>
    
{% endblock %}

{% block table %}
    <h2>Users</h2>
    <div class=\"table-responsive\">
        <table class=\"table table-striped table-sm\">
            <thead>
                <tr class=\"table-secondary\">
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Admin</th> 
                    <th></th>                   
                </tr>
            </thead>
            <tbody id=\"tableBody\">
                                                                                       
            </tbody>
        </table>  

        <div class=\"pageNavigation\">
            <br>
            <span id=\"pageNavPrev\" onclick=\"loadPage(currPageNo-1, true, '/admin/user/list/','/admin/user/list/singlepage/' )\">Previous</span>
            {% for no in 1 .. maxPages %}
                <span id=\"pageNav{{no}}\" onclick=\"loadPage({{no}}, true, '/admin/user/list/','/admin/user/list/singlepage/')\">{{no}}</span>
            {% endfor %}
            <span  id=\"pageNavNext\" onclick=\"loadPage(currPageNo+1, true, '/admin/user/list/','/admin/user/list/singlepage/')\">Next</span>
        </div> 
    </div>
{% endblock %}

{% block js %}
<script>
/*    \$(document).ready(function(){
        var passedValue;
        \$(\"#dialogConfirm\").dialog({
            resizable: false,
            height:140,
            modal: true,
            buttons: {
                \"Delete\": function() {
                    alert(\"passed values is :\"+ passedValue); 

                    \$( this ).dialog( \"close\" );
                },
                Cancel: function() {
                \$( this ).dialog( \"close\" );
                }
            }
        });

        \$(\"#btnDelete\").click(function(){
            passedValue = \$(this).data(\"value\");
        
            \$(\"#dialogConfirm\" ).dialog(\"open\");
        });
    });
    <div id=\"dialogConfirm\" style=\"display: none\" align = \"center\">
        Are you sure to delete selected item?
    </div>
    <button class=\"btn-delete\" data-value={{user.id}}>Show Popup</button>*/
</script>

{% endblock %}", "/admin/user_list.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\admin\\user_list.html.twig");
    }
}
