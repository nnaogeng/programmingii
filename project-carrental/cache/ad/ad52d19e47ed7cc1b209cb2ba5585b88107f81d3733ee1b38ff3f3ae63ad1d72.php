<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/master.html.twig */
class __TwigTemplate_010267a19b8e2f35b10c584f278018173a88b519c4eabe83f472958e6d376e05 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"> 
            
        <link rel=\"stylesheet\" href=\"/css/masterTemp.css\" />

        <title>";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        echo " - Admin interface</title>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <div id=\"centeredContent\">
            <h2>Admin interface</h2>
            ";
        // line 15
        $this->displayBlock('content', $context, $blocks);
        // line 16
        echo "        </div>
    </body>
</html>";
    }

    // line 9
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 10
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 15
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "admin/master.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  82 => 15,  76 => 10,  70 => 9,  64 => 16,  62 => 15,  56 => 11,  54 => 10,  50 => 9,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"> 
            
        <link rel=\"stylesheet\" href=\"/css/masterTemp.css\" />

        <title>{% block title %}{% endblock %} - Admin interface</title>
        {% block head %}{% endblock %}
    </head>
    <body>
        <div id=\"centeredContent\">
            <h2>Admin interface</h2>
            {% block content %}{% endblock %}
        </div>
    </body>
</html>", "admin/master.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\admin\\master.html.twig");
    }
}
