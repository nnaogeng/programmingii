<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* booking/step2.html.twig */
class __TwigTemplate_6ed98b1d1ad48deed4a78ba96e5543a32b4886499a4bcfb3bf07596e3ddfcc65 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'formAction' => [$this, 'block_formAction'],
            'filterAndSorter' => [$this, 'block_filterAndSorter'],
            'step' => [$this, 'block_step'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "booking/masterBooking2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("booking/masterBooking2.html.twig", "booking/step2.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Booking Step2";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "    
";
    }

    // line 8
    public function block_formAction($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 
    action=\"/booking/step3\"
";
    }

    // line 12
    public function block_filterAndSorter($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    <div class=\"row mt-2 ml-2\">
        <div class=\"col-4\">  
            <div class=\"row\">
                <div clas=\"col-4\">                                         
                    <label class=\"col-form-label\">Filter By:</label>
                </div>
                <div class=\"col-8\">
                    <select type=\"text\" id=\"selectViewCategory\" name=\"filter\" class=\"form-control\">
                        <option value=\"0\">View All</option>
                        ";
        // line 22
        if (($context["categoryList"] ?? null)) {
            // line 23
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categoryList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 24
                echo "                                <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 24), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "categoryName", [], "any", false, false, false, 24), "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "            
                        ";
        }
        // line 27
        echo "                    </select>  
                </div>
            </div>                                                                                          
        </div>
        <div class=\"col-4\"></div>
        <div class=\"col-4\">
            <div class=\"row\">
                <div clas=\"col-4\">                                         
                    <label class=\"col-form-label\">Sorted By:</label>
                </div>
                <div class=\"col-8\">
                    <select type=\"text\" id=\"sortCategory\" name=\"sorter\" class=\"form-control\">
                        <option value=\"0\">Price (Low to High)</option>
                        <option value=\"1\">Price (High to Low)</option>
                        <option value=\"2\">Number of Seats (Low to High)</option>
                        <option value=\"3\">Number of Seats (High to Low)</option>
                    </select>  
                </div>
            </div>                                                                                                                                                           
        </div>                
    </div>
";
    }

    // line 50
    public function block_step($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 51
        echo "    <div class=\"bg-light p-2 pl-3\">
        <h4>Step 2: Choose A Car</h4>
    </div>
";
    }

    // line 91
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 92
        echo "    <script>
        \$(document).ready(function() {
            \$(\"#carCategoryListing\").load(\"/booking/selectViewCategory/0/0\");
            \$(\"#selectViewCategory\").change(function(){
                \$categoryId = \$(\"#selectViewCategory\").val();
                \$sorterId = \$(\"#sortCategory\").val();
                \$(\"#carCategoryListing\").load(\"/booking/selectViewCategory/\" +  \$categoryId + \"/\" + \$sorterId);
            });
            \$(\"#sortCategory\").change(function(){
                \$categoryId = \$(\"#selectViewCategory\").val();
                \$sorterId = \$(\"#sortCategory\").val();
                \$(\"#carCategoryListing\").load(\"/booking/selectViewCategory/\" +  \$categoryId + \"/\" + \$sorterId);
            });

        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "booking/step2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 92,  148 => 91,  141 => 51,  137 => 50,  112 => 27,  108 => 25,  97 => 24,  92 => 23,  90 => 22,  79 => 13,  75 => 12,  66 => 8,  58 => 5,  51 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"booking/masterBooking2.html.twig\" %}

{% block title %}Booking Step2{% endblock %}

{% block head %}    
{% endblock %}

{% block formAction %} 
    action=\"/booking/step3\"
{% endblock %}

{% block filterAndSorter %}
    <div class=\"row mt-2 ml-2\">
        <div class=\"col-4\">  
            <div class=\"row\">
                <div clas=\"col-4\">                                         
                    <label class=\"col-form-label\">Filter By:</label>
                </div>
                <div class=\"col-8\">
                    <select type=\"text\" id=\"selectViewCategory\" name=\"filter\" class=\"form-control\">
                        <option value=\"0\">View All</option>
                        {% if categoryList %}
                            {% for category in categoryList %}
                                <option value=\"{{category.id}}\">{{category.categoryName}}</option>
                            {% endfor %}            
                        {% endif %}
                    </select>  
                </div>
            </div>                                                                                          
        </div>
        <div class=\"col-4\"></div>
        <div class=\"col-4\">
            <div class=\"row\">
                <div clas=\"col-4\">                                         
                    <label class=\"col-form-label\">Sorted By:</label>
                </div>
                <div class=\"col-8\">
                    <select type=\"text\" id=\"sortCategory\" name=\"sorter\" class=\"form-control\">
                        <option value=\"0\">Price (Low to High)</option>
                        <option value=\"1\">Price (High to Low)</option>
                        <option value=\"2\">Number of Seats (Low to High)</option>
                        <option value=\"3\">Number of Seats (High to Low)</option>
                    </select>  
                </div>
            </div>                                                                                                                                                           
        </div>                
    </div>
{% endblock %}

{% block step %}
    <div class=\"bg-light p-2 pl-3\">
        <h4>Step 2: Choose A Car</h4>
    </div>
{% endblock %}

{#
{% block table %}     
    <div class=\"col-8 table-responsive\">
        <table class=\"table\">
            {% if categoryList %}
                {% for category in categoryList %}
                    <tr class=\"border border-light rounded\">
                        <td class=\"form-responsive\">
                            <form method=\"post\" class=\"form form-sm\" action=\"/booking/step3/{{category.id}}\">
                                <div class=\"row\">
                                    <div class=\"col-4\">
                                        <img src={{category.photoFilePath}} width=\"154\" height=\"98\" class=\"img-responsive\"/>
                                    </div>
                                    <div class=\"col-5\">
                                        <h5>{{category.categoryName}}</h5>
                                        <p>Example of this range:</p>
                                        <p>{{category.carExample}}</p>
                                    </div>
                                    <div class=\"col-3 text-center\">
                                        <div class=\"bg-light text-center\">
                                            <p>Total Price:</p>
                                            <p><b>{{'\$CAD ' ~ category.costPerDay * booking.duration.d ~ '.00'}}</b></p>
                                        </div>
                                        <input type=\"submit\" name=\"continue\" value=\"CONTINUE\" class=\"btn-sm btn-primary\">
                                    </div>
                            </form>
                        </td>                                    
                    </tr>
                {% endfor %}            
            {% endif %}   
        </table>
    </div>
{% endblock %}
#}

{% block js %}
    <script>
        \$(document).ready(function() {
            \$(\"#carCategoryListing\").load(\"/booking/selectViewCategory/0/0\");
            \$(\"#selectViewCategory\").change(function(){
                \$categoryId = \$(\"#selectViewCategory\").val();
                \$sorterId = \$(\"#sortCategory\").val();
                \$(\"#carCategoryListing\").load(\"/booking/selectViewCategory/\" +  \$categoryId + \"/\" + \$sorterId);
            });
            \$(\"#sortCategory\").change(function(){
                \$categoryId = \$(\"#selectViewCategory\").val();
                \$sorterId = \$(\"#sortCategory\").val();
                \$(\"#carCategoryListing\").load(\"/booking/selectViewCategory/\" +  \$categoryId + \"/\" + \$sorterId);
            });

        });
    </script>
{% endblock %}", "booking/step2.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\booking\\step2.html.twig");
    }
}
