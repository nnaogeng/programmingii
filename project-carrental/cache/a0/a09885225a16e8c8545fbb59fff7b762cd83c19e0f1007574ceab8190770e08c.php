<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /admin/reservation_list.html.twig */
class __TwigTemplate_548e95b16b70a585fd9e8c6e82e7bb53820f9c526431bd69c2d779e0a4ae74e6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'addItem' => [$this, 'block_addItem'],
            'table' => [$this, 'block_table'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/masterAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("admin/masterAdmin.html.twig", "/admin/reservation_list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    
    <script>
        var currPageNo = ";
        // line 6
        echo twig_escape_filter($this->env, ($context["pageNo"] ?? null), "html", null, true);
        echo ";
        var maxPages = ";
        // line 7
        echo twig_escape_filter($this->env, ($context["maxPages"] ?? null), "html", null, true);
        echo ";
    </script>
    <script src=\"/js/admin_pagination.js\"></script>
    <script>
        \$(document).ready(function() {
            \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
                console.log(\"Ajax error occured on \" + settings.url);
                alert(\"Ajax error occured\");
            });

            loadPage(";
        // line 17
        echo twig_escape_filter($this->env, ($context["pageNo"] ?? null), "html", null, true);
        echo ", true, '/admin/reservation/list/','/admin/reservation/list/singlepage/');
        });
    </script>
    
";
    }

    // line 23
    public function block_addItem($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "    <a href=\"/reservationAdd\"><input type=\"submit\" name=\"addReservation\" value=\"+ Add Reservatiom\"></input></a>
";
    }

    // line 27
    public function block_table($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "    <h2>Reservations</h2>
    <div class=\"table-responsive\">
        <table class=\"table table-striped table-sm\">
            <thead>
                <tr>
                    <th>Customer ID</th>
                    <th>Car ID</th>
                    <th>Start Date/Time</th>
                    <th>Return Date/Time</th>
                    <th>Actual Return Date/Time</th>
                    <th>Pick Up Location</th>
                    <th>Return Location</th>
                    <th>Cost / Day</th>
                    <th>Insurance</th>                                                                          
                </tr>
            </thead>
            <tbody id=\"tableBody\">
                                                               
            </tbody>
        </table>
        
        <div class=\"pageNavigation\">
            <br>
            <span id=\"pageNavPrev\" onclick=\"loadPage(currPageNo-1, true, '/admin/reservation/list/','/admin/reservation/list/singlepage/')\">Previous</span>
            ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, ($context["maxPages"] ?? null)));
        foreach ($context['_seq'] as $context["_key"] => $context["no"]) {
            // line 53
            echo "                <span id=\"pageNav";
            echo twig_escape_filter($this->env, $context["no"], "html", null, true);
            echo "\" onclick=\"loadPage(";
            echo twig_escape_filter($this->env, $context["no"], "html", null, true);
            echo ", true, '/admin/reservation/list/','/admin/reservation/list/singlepage/')\">";
            echo twig_escape_filter($this->env, $context["no"], "html", null, true);
            echo "</span>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['no'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "            <span  id=\"pageNavNext\" onclick=\"loadPage(currPageNo+1, true, '/admin/reservation/list/','/admin/reservation/list/singlepage/')\">Next</span>
        </div>

    </div>

";
    }

    // line 62
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 63
        echo "    <script type=\"text/javascript\">
        var elems = document.getElementsByClassName('confirmation');
        var confirmIt = function (e) {
            if (!confirm('Are you sure to delete selected entry?')) e.preventDefault();
        };
        for (var i = 0, l = elems.length; i < l; i++) {
            elems[i].addEventListener('click', confirmIt, false);
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "/admin/reservation_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 63,  148 => 62,  139 => 55,  126 => 53,  122 => 52,  96 => 28,  92 => 27,  87 => 24,  83 => 23,  74 => 17,  61 => 7,  57 => 6,  53 => 4,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"admin/masterAdmin.html.twig\" %}

{% block head %}
    
    <script>
        var currPageNo = {{pageNo}};
        var maxPages = {{maxPages}};
    </script>
    <script src=\"/js/admin_pagination.js\"></script>
    <script>
        \$(document).ready(function() {
            \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
                console.log(\"Ajax error occured on \" + settings.url);
                alert(\"Ajax error occured\");
            });

            loadPage({{pageNo}}, true, '/admin/reservation/list/','/admin/reservation/list/singlepage/');
        });
    </script>
    
{% endblock %}

{% block addItem %}
    <a href=\"/reservationAdd\"><input type=\"submit\" name=\"addReservation\" value=\"+ Add Reservatiom\"></input></a>
{% endblock %}

{% block table %}
    <h2>Reservations</h2>
    <div class=\"table-responsive\">
        <table class=\"table table-striped table-sm\">
            <thead>
                <tr>
                    <th>Customer ID</th>
                    <th>Car ID</th>
                    <th>Start Date/Time</th>
                    <th>Return Date/Time</th>
                    <th>Actual Return Date/Time</th>
                    <th>Pick Up Location</th>
                    <th>Return Location</th>
                    <th>Cost / Day</th>
                    <th>Insurance</th>                                                                          
                </tr>
            </thead>
            <tbody id=\"tableBody\">
                                                               
            </tbody>
        </table>
        
        <div class=\"pageNavigation\">
            <br>
            <span id=\"pageNavPrev\" onclick=\"loadPage(currPageNo-1, true, '/admin/reservation/list/','/admin/reservation/list/singlepage/')\">Previous</span>
            {% for no in 1 .. maxPages %}
                <span id=\"pageNav{{no}}\" onclick=\"loadPage({{no}}, true, '/admin/reservation/list/','/admin/reservation/list/singlepage/')\">{{no}}</span>
            {% endfor %}
            <span  id=\"pageNavNext\" onclick=\"loadPage(currPageNo+1, true, '/admin/reservation/list/','/admin/reservation/list/singlepage/')\">Next</span>
        </div>

    </div>

{% endblock %}

{% block js %}
    <script type=\"text/javascript\">
        var elems = document.getElementsByClassName('confirmation');
        var confirmIt = function (e) {
            if (!confirm('Are you sure to delete selected entry?')) e.preventDefault();
        };
        for (var i = 0, l = elems.length; i < l; i++) {
            elems[i].addEventListener('click', confirmIt, false);
        }
    </script>
{% endblock %}", "/admin/reservation_list.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\admin\\reservation_list.html.twig");
    }
}
