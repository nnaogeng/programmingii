<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* booking/masterBooking2.html.twig */
class __TwigTemplate_f194871958b6c0107fc1d98877bca941d48aedf9cd1bab0936a0c737bcac563b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
            'formAction' => [$this, 'block_formAction'],
            'step' => [$this, 'block_step'],
            'filterAndSorter' => [$this, 'block_filterAndSorter'],
            'selectedCarDetail' => [$this, 'block_selectedCarDetail'],
            'table2' => [$this, 'block_table2'],
            'table3' => [$this, 'block_table3'],
            'personInfo' => [$this, 'block_personInfo'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "booking/masterBooking.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("booking/masterBooking.html.twig", "booking/masterBooking2.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Booking Step2";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "    
";
    }

    // line 8
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "     
    <div class=\"row form-responsive\">
        <div class=\"col\"></div>
        <div class=\"col-11 col-md-10 col-lg-9 border border-light rounded px-2\">
            <form method=\"post\" class=\"form form-sm mt-2\" ";
        // line 12
        $this->displayBlock('formAction', $context, $blocks);
        echo ">
                
                ";
        // line 14
        $this->displayBlock('step', $context, $blocks);
        // line 15
        echo " 

                ";
        // line 17
        $this->displayBlock('filterAndSorter', $context, $blocks);
        // line 19
        echo "                                    
                <div class=\"row mt-2\">
                    <div class=\"col-4 mt-2\">
                        <div class=\"border border-light rounded mt-2 p-1 bg-light\">
                            <h5>Booking Details</h5>
                        </div>
                        <div class=\"border border-light rounded mt-2 p-1\">
                            <div class=\"row mt-1 pl-1\">
                                <div class=\"col-8\"><b>Time And Place</b></div>
                                <div class=\"col-4\"><a href=\"/booking/step1\">Change</a></div>
                            </div>
                            <div class=\"row mt-1 pl-1\">
                                <div class=\"col-4\">Pick up:</div>
                                <div class=\"col-8\">
                                    <p>";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "pickupLocation", [], "any", false, false, false, 33), "street", [], "any", false, false, false, 33), "html", null, true);
        echo "</p>
                                    <p>";
        // line 34
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "pickupLocation", [], "any", false, false, false, 34), "city", [], "any", false, false, false, 34), "html", null, true);
        echo "</p>
                                    <p>";
        // line 35
        echo twig_escape_filter($this->env, twig_join_filter([0 => twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "pickupDate", [], "any", false, false, false, 35), 1 => twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "pickupTime", [], "any", false, false, false, 35)], " , "), "html", null, true);
        echo "</p>                            
                                </div>
                            </div>
                            <div class=\"row mt-1 pl-1\">
                                <div class=\"col-4\">Return:</div>
                                <div class=\"col-8\">
                                    <p>";
        // line 41
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "returnLocation", [], "any", false, false, false, 41), "street", [], "any", false, false, false, 41), "html", null, true);
        echo "</p>
                                    <p>";
        // line 42
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "returnLocation", [], "any", false, false, false, 42), "city", [], "any", false, false, false, 42), "html", null, true);
        echo "</p>
                                    <p>";
        // line 43
        echo twig_escape_filter($this->env, twig_join_filter([0 => twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "returnDate", [], "any", false, false, false, 43), 1 => twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "returnTime", [], "any", false, false, false, 43)], " , "), "html", null, true);
        echo "</p>                            
                                </div>
                            </div>
                            <div class=\"row mt-1 pl-1\">
                                <div class=\"col-4\">Rental Duration:</div>
                                <div class=\"col-8\">
                                    <p>";
        // line 49
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "strDuration", [], "any", false, false, false, 49), "html", null, true);
        echo "</p>                                                        
                                </div>
                            </div>

                            ";
        // line 53
        $this->displayBlock('selectedCarDetail', $context, $blocks);
        // line 55
        echo "
                        </div>
                    </div>

                    ";
        // line 59
        if ((($context["stepNo"] ?? null) == 2)) {
            echo " 
                        ";
            // line 60
            $this->displayBlock('table2', $context, $blocks);
            // line 91
            echo "                    
                    ";
        } elseif (((        // line 92
($context["stepNo"] ?? null) == 3) || (($context["stepNo"] ?? null) == 4))) {
            // line 93
            echo "                        ";
            $this->displayBlock('table3', $context, $blocks);
            // line 181
            echo "                    ";
        }
        // line 182
        echo "                </div>  

                ";
        // line 184
        $this->displayBlock('personInfo', $context, $blocks);
        // line 186
        echo "                          
            </form>
        </div>
        <div class=\"col\"></div>    
    </div>
";
    }

    // line 12
    public function block_formAction($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    // line 14
    public function block_step($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "                ";
    }

    // line 17
    public function block_filterAndSorter($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "                ";
    }

    // line 53
    public function block_selectedCarDetail($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 54
        echo "                            ";
    }

    // line 60
    public function block_table2($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "     
                            <div class=\"col-8 table-responsive\">
                                <table id=\"carCategoryListing\" class=\"table\">
                                ";
        // line 89
        echo "                                </table>
                            </div>
                        ";
    }

    // line 93
    public function block_table3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "     
                            <div class=\"col-8 mt-2 table-responsive\">
                                <table class=\"table table-sm\">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <b>Rental Duration</b>
                                            </td>
                                            <td class=\"text-right\">
                                                <p>";
        // line 102
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "strDuration", [], "any", false, false, false, 102), "html", null, true);
        echo "</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span><b>Price Per Day</b><span></br>
                                                ";
        // line 108
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 108), "d", [], "any", false, false, false, 108) != 0)) {
            // line 109
            echo "                                                    <p>";
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 109), "d", [], "any", false, false, false, 109) . " Day X \$CAD ") . twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 109)), "html", null, true);
            echo "</p>
                                                ";
        }
        // line 111
        echo "                                            </td>
                                            <td class=\"text-right\">
                                                ";
        // line 113
        echo twig_escape_filter($this->env, (("\$CAD " . (twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 113) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 113), "d", [], "any", false, false, false, 113))) . ".00"), "html", null, true);
        echo "
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span><b>Price Per Hour</b><span></br>
                                                ";
        // line 119
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 119), "h", [], "any", false, false, false, 119) != 0)) {
            // line 120
            echo "                                                    <p>";
            echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 120), "h", [], "any", false, false, false, 120) . " Hour X \$CAD ") . (twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 120) * 0.1)) . ".00"), "html", null, true);
            echo "</p>
                                                ";
        }
        // line 122
        echo "                                            </td>
                                            <td class=\"text-right\">
                                                ";
        // line 124
        echo twig_escape_filter($this->env, (("\$CAD " . ((twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 124) * 0.1) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 124), "h", [], "any", false, false, false, 124))) . ".00"), "html", null, true);
        echo "
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span><b>Car Rental Fee</b><span></br>
                                                <p>";
        // line 130
        echo twig_escape_filter($this->env, (("\$CAD " . (twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 130) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 130), "d", [], "any", false, false, false, 130))) . ".00"), "html", null, true);
        echo "
                                                ";
        // line 131
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 131), "h", [], "any", false, false, false, 131) != 0)) {
            // line 132
            echo "                                                    ";
            echo twig_escape_filter($this->env, (((" + " . "\$CAD ") . ((twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 132) * 0.1) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 132), "h", [], "any", false, false, false, 132))) . ".00"), "html", null, true);
            echo "
                                                ";
        }
        // line 133
        echo "        
                                                </p>    
                                            </td>
                                            <td class=\"text-right\">
                                                ";
        // line 137
        echo twig_escape_filter($this->env, (("\$CAD " . ((twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 137) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 137), "d", [], "any", false, false, false, 137)) + ((twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 137) * 0.1) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 137), "h", [], "any", false, false, false, 137)))) . ".00"), "html", null, true);
        echo "
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span><b>Insurance Fee</b><span></br>
                                            </td>
                                            <td class=\"text-right\">                        
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span><b>Sub-total</b><span></br>
                                            </td>
                                            <td class=\"text-right\">
                                                ";
        // line 152
        echo twig_escape_filter($this->env, (("\$CAD " . ((twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 152) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 152), "d", [], "any", false, false, false, 152)) + ((twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 152) * 0.1) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 152), "h", [], "any", false, false, false, 152)))) . ".00"), "html", null, true);
        echo "
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class=\"text-danger\"><b>Total Price<b><pan></br>
                                            </td>
                                            <td class=\"text-right\">
                                                ";
        // line 160
        echo twig_escape_filter($this->env, (("\$CAD " . ((twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 160) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 160), "d", [], "any", false, false, false, 160)) + ((twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 160) * 0.1) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 160), "h", [], "any", false, false, false, 160)))) . ".00"), "html", null, true);
        echo "
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span><b>Required Deposit<b><pan></br>
                                                <p>";
        // line 166
        echo twig_escape_filter($this->env, ((("10% of " . "\$CAD ") . ((twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 166) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 166), "d", [], "any", false, false, false, 166)) + ((twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 166) * 0.1) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 166), "h", [], "any", false, false, false, 166)))) . ".00"), "html", null, true);
        echo "
                                            </td>
                                            <td class=\"text-right\">
                                                ";
        // line 169
        echo twig_escape_filter($this->env, (("\$CAD " . (((twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 169) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 169), "d", [], "any", false, false, false, 169)) + ((twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 169) * 0.1) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 169), "h", [], "any", false, false, false, 169))) * 0.1)) . ".00"), "html", null, true);
        echo "
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                ";
        // line 174
        if ((($context["stepNo"] ?? null) == 3)) {
            // line 175
            echo "                                    <input type=\"submit\" name=\"continue\" value=\"CONTINUE\" class=\"btn-sm btn-primary\">
                                ";
        } elseif ((        // line 176
($context["stepNo"] ?? null) == 4)) {
            // line 177
            echo "                                    <input type=\"hidden\" name=\"deposit\" value=\"";
            echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 177) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 177), "d", [], "any", false, false, false, 177)) + ((twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "costPerDay", [], "any", false, false, false, 177) * 0.1) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "duration", [], "any", false, false, false, 177), "h", [], "any", false, false, false, 177))) * 0.1), "html", null, true);
            echo "\">              
                                ";
        }
        // line 179
        echo "                            </div>
                        ";
    }

    // line 184
    public function block_personInfo($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 
                ";
    }

    public function getTemplateName()
    {
        return "booking/masterBooking2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  387 => 184,  382 => 179,  376 => 177,  374 => 176,  371 => 175,  369 => 174,  361 => 169,  355 => 166,  346 => 160,  335 => 152,  317 => 137,  311 => 133,  305 => 132,  303 => 131,  299 => 130,  290 => 124,  286 => 122,  280 => 120,  278 => 119,  269 => 113,  265 => 111,  259 => 109,  257 => 108,  248 => 102,  234 => 93,  228 => 89,  220 => 60,  216 => 54,  212 => 53,  208 => 18,  204 => 17,  200 => 15,  196 => 14,  189 => 12,  180 => 186,  178 => 184,  174 => 182,  171 => 181,  168 => 93,  166 => 92,  163 => 91,  161 => 60,  157 => 59,  151 => 55,  149 => 53,  142 => 49,  133 => 43,  129 => 42,  125 => 41,  116 => 35,  112 => 34,  108 => 33,  92 => 19,  90 => 17,  86 => 15,  84 => 14,  79 => 12,  70 => 8,  62 => 5,  55 => 3,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"booking/masterBooking.html.twig\" %}

{% block title %}Booking Step2{% endblock %}

{% block head %}    
{% endblock %}

{% block content %}     
    <div class=\"row form-responsive\">
        <div class=\"col\"></div>
        <div class=\"col-11 col-md-10 col-lg-9 border border-light rounded px-2\">
            <form method=\"post\" class=\"form form-sm mt-2\" {% block formAction %} {% endblock %}>
                
                {% block step %}
                {% endblock %} 

                {% block filterAndSorter %}
                {% endblock %}
                                    
                <div class=\"row mt-2\">
                    <div class=\"col-4 mt-2\">
                        <div class=\"border border-light rounded mt-2 p-1 bg-light\">
                            <h5>Booking Details</h5>
                        </div>
                        <div class=\"border border-light rounded mt-2 p-1\">
                            <div class=\"row mt-1 pl-1\">
                                <div class=\"col-8\"><b>Time And Place</b></div>
                                <div class=\"col-4\"><a href=\"/booking/step1\">Change</a></div>
                            </div>
                            <div class=\"row mt-1 pl-1\">
                                <div class=\"col-4\">Pick up:</div>
                                <div class=\"col-8\">
                                    <p>{{booking.pickupLocation.street}}</p>
                                    <p>{{booking.pickupLocation.city}}</p>
                                    <p>{{[booking.pickupDate, booking.pickupTime] | join(' , ')}}</p>                            
                                </div>
                            </div>
                            <div class=\"row mt-1 pl-1\">
                                <div class=\"col-4\">Return:</div>
                                <div class=\"col-8\">
                                    <p>{{booking.returnLocation.street}}</p>
                                    <p>{{booking.returnLocation.city}}</p>
                                    <p>{{[booking.returnDate, booking.returnTime] | join(' , ')}}</p>                            
                                </div>
                            </div>
                            <div class=\"row mt-1 pl-1\">
                                <div class=\"col-4\">Rental Duration:</div>
                                <div class=\"col-8\">
                                    <p>{{booking.strDuration}}</p>                                                        
                                </div>
                            </div>

                            {% block selectedCarDetail %}
                            {% endblock %}

                        </div>
                    </div>

                    {% if stepNo == 2 %} 
                        {% block table2 %}     
                            <div class=\"col-8 table-responsive\">
                                <table id=\"carCategoryListing\" class=\"table\">
                                {#    {% if categoryList %}
                                        {% for category in categoryList %}
                                            <tr class=\"border border-light rounded\">
                                                <td class=\"form-responsive\">
                                                    <form method=\"post\" class=\"form form-sm\" action=\"/booking/step3/{{category.id}}\">
                                                        <div class=\"row\">
                                                            <div class=\"col-4\">
                                                                <img src={{category.photoFilePath}} width=\"154\" height=\"98\" class=\"img-responsive\"/>
                                                            </div>
                                                            <div class=\"col-5\">
                                                                <h5>{{category.categoryName}}</h5>
                                                                <p>Example of this range:</p>
                                                                <p>{{category.carExample}}</p>
                                                            </div>
                                                            <div class=\"col-3 text-center\">
                                                                <div class=\"bg-light text-center\">
                                                                    <p>Total Price:</p>
                                                                    <p><b>{{'\$CAD ' ~ category.costPerDay * booking.duration.d ~ '.00'}}</b></p>
                                                                </div>
                                                                <input type=\"submit\" name=\"continue\" value=\"CONTINUE\" class=\"btn-sm btn-primary\">
                                                            </div>
                                                    </form>
                                                </td>                                    
                                            </tr>
                                        {% endfor %}            
                                    {% endif %}   #}
                                </table>
                            </div>
                        {% endblock %}                    
                    {% elseif (stepNo == 3 or stepNo == 4) %}
                        {% block table3 %}     
                            <div class=\"col-8 mt-2 table-responsive\">
                                <table class=\"table table-sm\">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <b>Rental Duration</b>
                                            </td>
                                            <td class=\"text-right\">
                                                <p>{{booking.strDuration}}</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span><b>Price Per Day</b><span></br>
                                                {% if booking.duration.d != 0 %}
                                                    <p>{{booking.duration.d ~ \" Day X \$CAD \" ~ category.costPerDay}}</p>
                                                {% endif %}
                                            </td>
                                            <td class=\"text-right\">
                                                {{\"\$CAD \" ~ category.costPerDay * booking.duration.d ~ \".00\"}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span><b>Price Per Hour</b><span></br>
                                                {% if booking.duration.h != 0 %}
                                                    <p>{{booking.duration.h ~ \" Hour X \$CAD \" ~ category.costPerDay * 0.1 ~ \".00\"}}</p>
                                                {% endif %}
                                            </td>
                                            <td class=\"text-right\">
                                                {{\"\$CAD \" ~ category.costPerDay * 0.1 * booking.duration.h ~ \".00\"}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span><b>Car Rental Fee</b><span></br>
                                                <p>{{\"\$CAD \" ~ category.costPerDay * booking.duration.d ~ \".00\"}}
                                                {% if booking.duration.h != 0 %}
                                                    {{\" + \" ~ \"\$CAD \" ~ (category.costPerDay * 0.1 * booking.duration.h) ~ \".00\"}}
                                                {% endif %}        
                                                </p>    
                                            </td>
                                            <td class=\"text-right\">
                                                {{\"\$CAD \" ~ (category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h) ~ \".00\"}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span><b>Insurance Fee</b><span></br>
                                            </td>
                                            <td class=\"text-right\">                        
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span><b>Sub-total</b><span></br>
                                            </td>
                                            <td class=\"text-right\">
                                                {{\"\$CAD \" ~ (category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h) ~ \".00\"}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class=\"text-danger\"><b>Total Price<b><pan></br>
                                            </td>
                                            <td class=\"text-right\">
                                                {{\"\$CAD \" ~ (category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h) ~ \".00\"}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span><b>Required Deposit<b><pan></br>
                                                <p>{{\"10% of \" ~ \"\$CAD \" ~ (category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h) ~ \".00\"}}
                                            </td>
                                            <td class=\"text-right\">
                                                {{\"\$CAD \" ~ ((category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h) * 0.1) ~ \".00\"}}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                {% if stepNo == 3 %}
                                    <input type=\"submit\" name=\"continue\" value=\"CONTINUE\" class=\"btn-sm btn-primary\">
                                {% elseif stepNo == 4 %}
                                    <input type=\"hidden\" name=\"deposit\" value=\"{{(category.costPerDay * booking.duration.d + category.costPerDay * 0.1 * booking.duration.h) * 0.1}}\">              
                                {% endif %}
                            </div>
                        {% endblock %}
                    {% endif %}
                </div>  

                {% block personInfo %} 
                {% endblock %}
                          
            </form>
        </div>
        <div class=\"col\"></div>    
    </div>
{% endblock %}
", "booking/masterBooking2.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\booking\\masterBooking2.html.twig");
    }
}
