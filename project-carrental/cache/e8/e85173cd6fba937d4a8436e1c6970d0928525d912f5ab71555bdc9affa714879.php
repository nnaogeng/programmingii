<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /admin/car_list.html.twig */
class __TwigTemplate_a110b2f1b5fe8c3bf9a50e99b0ce984282dc77dea6fda2a33c76aec311e65d68 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'buttonAdd' => [$this, 'block_buttonAdd'],
            'table' => [$this, 'block_table'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/masterAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("admin/masterAdmin.html.twig", "/admin/car_list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    
    <script>
        var currPageNo = ";
        // line 6
        echo twig_escape_filter($this->env, ($context["pageNo"] ?? null), "html", null, true);
        echo ";
        var maxPages = ";
        // line 7
        echo twig_escape_filter($this->env, ($context["maxPages"] ?? null), "html", null, true);
        echo ";
    </script>
    <script src=\"/js/admin_pagination.js\"></script>
    <script>
        \$(document).ready(function() {
            \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
                console.log(\"Ajax error occured on \" + settings.url);
                alert(\"Ajax error occured\");
            });

            loadPage(";
        // line 17
        echo twig_escape_filter($this->env, ($context["pageNo"] ?? null), "html", null, true);
        echo ", true, '/admin/car/list/','/admin/car/list/singlepage/');
        });
    </script>
    
";
    }

    // line 23
    public function block_buttonAdd($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "    <a href=\"/admin/car/add\"><input type=\"submit\" name=\"addCar\" value=\"+ Add Car\"></input></a>
";
    }

    // line 27
    public function block_table($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "    <h2>Cars</h2>
    <div class=\"table-responsive\">
        <table class=\"table table-striped table-sm\">
            <thead>
                <tr>
                    <th>Registration Number</th>
                    <th>Make</th>
                    <th>Model</th>
                    <th>Mileage</th>
                    <th>Engine Size</th>
                    <th>Production Year</th>
                    <th>Color</th>
                    <th>Default Location</th>
                    <th>Car Type</th>
                </tr>
            </thead>
            <tbody id=\"tableBody\">
                                      
            </tbody>
        </table>

        <div class=\"pageNavigation\">
            <br>
            <span id=\"pageNavPrev\" onclick=\"loadPage(currPageNo-1, true, '/admin/car/list/','/admin/car/list/singlepage/' )\">Previous</span>
            ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, ($context["maxPages"] ?? null)));
        foreach ($context['_seq'] as $context["_key"] => $context["no"]) {
            // line 53
            echo "                <span id=\"pageNav";
            echo twig_escape_filter($this->env, $context["no"], "html", null, true);
            echo "\" onclick=\"loadPage(";
            echo twig_escape_filter($this->env, $context["no"], "html", null, true);
            echo ", true, '/admin/car/list/','/admin/car/list/singlepage/')\">";
            echo twig_escape_filter($this->env, $context["no"], "html", null, true);
            echo "</span>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['no'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "            <span  id=\"pageNavNext\" onclick=\"loadPage(currPageNo+1, true, '/admin/car/list/','/admin/car/list/singlepage/')\">Next</span>
        </div>

    </div>

";
    }

    public function getTemplateName()
    {
        return "/admin/car_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 55,  125 => 53,  121 => 52,  95 => 28,  91 => 27,  86 => 24,  82 => 23,  73 => 17,  60 => 7,  56 => 6,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"admin/masterAdmin.html.twig\" %}

{% block head %}
    
    <script>
        var currPageNo = {{pageNo}};
        var maxPages = {{maxPages}};
    </script>
    <script src=\"/js/admin_pagination.js\"></script>
    <script>
        \$(document).ready(function() {
            \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
                console.log(\"Ajax error occured on \" + settings.url);
                alert(\"Ajax error occured\");
            });

            loadPage({{pageNo}}, true, '/admin/car/list/','/admin/car/list/singlepage/');
        });
    </script>
    
{% endblock %}

{% block buttonAdd %}
    <a href=\"/admin/car/add\"><input type=\"submit\" name=\"addCar\" value=\"+ Add Car\"></input></a>
{% endblock %}

{% block table %}
    <h2>Cars</h2>
    <div class=\"table-responsive\">
        <table class=\"table table-striped table-sm\">
            <thead>
                <tr>
                    <th>Registration Number</th>
                    <th>Make</th>
                    <th>Model</th>
                    <th>Mileage</th>
                    <th>Engine Size</th>
                    <th>Production Year</th>
                    <th>Color</th>
                    <th>Default Location</th>
                    <th>Car Type</th>
                </tr>
            </thead>
            <tbody id=\"tableBody\">
                                      
            </tbody>
        </table>

        <div class=\"pageNavigation\">
            <br>
            <span id=\"pageNavPrev\" onclick=\"loadPage(currPageNo-1, true, '/admin/car/list/','/admin/car/list/singlepage/' )\">Previous</span>
            {% for no in 1 .. maxPages %}
                <span id=\"pageNav{{no}}\" onclick=\"loadPage({{no}}, true, '/admin/car/list/','/admin/car/list/singlepage/')\">{{no}}</span>
            {% endfor %}
            <span  id=\"pageNavNext\" onclick=\"loadPage(currPageNo+1, true, '/admin/car/list/','/admin/car/list/singlepage/')\">Next</span>
        </div>

    </div>

{% endblock %}", "/admin/car_list.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\admin\\car_list.html.twig");
    }
}
