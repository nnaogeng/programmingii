<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/customer_add_update.html.twig */
class __TwigTemplate_881b8a4ec2191f08c983982b3305d680f8672783acf576110b84d51159f327b6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'table' => [$this, 'block_table'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/masterAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("admin/masterAdmin.html.twig", "admin/customer_add_update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_table($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $context["action"] = (((($context["operation"] ?? null) == "add")) ? ("Add") : ("Update"));
        // line 5
        echo "    <h2>";
        echo twig_escape_filter($this->env, twig_join_filter([0 => ($context["action"] ?? null), 1 => "Customer"], " "), "html", null, true);
        echo "</h2>
    <div class=\"form-responsive\">
        <form method=\"post\" class=\"form form-sm\">                        
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">First Name:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"firstName\" class=\"form-control\" value=\"";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["customer"] ?? null), "firstName", [], "any", false, false, false, 11), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Last Name:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"lastName\" class=\"form-control\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["customer"] ?? null), "lastName", [], "any", false, false, false, 17), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Phone Number:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"phoneNo\" class=\"form-control\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["customer"] ?? null), "phoneNo", [], "any", false, false, false, 23), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Street:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"street\" class=\"form-control\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["customer"] ?? null), "street", [], "any", false, false, false, 29), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">City:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"city\" class=\"form-control\" value=\"";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["customer"] ?? null), "city", [], "any", false, false, false, 35), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Province:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"province\" class=\"form-control\" value=\"";
        // line 41
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["customer"] ?? null), "province", [], "any", false, false, false, 41), "html", null, true);
        echo "\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Postal Code:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"postalCode\" class=\"form-control\" value=\"";
        // line 47
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["customer"] ?? null), "postalCode", [], "any", false, false, false, 47), "html", null, true);
        echo "\"></input>
                </div>
            </div>                                   
            <div class=\"row\">
            <div class=\"col-sm-8 d-flex justify-content-around\">
                <input type=\"submit\" name=\"submit\" id=\"submitCustomer\" value=\"";
        // line 52
        echo twig_escape_filter($this->env, ($context["action"] ?? null), "html", null, true);
        echo "\" class=\"btn btn-secondary\" style=\"width: 100px;\"></input>
                <input type=\"submit\" name=\"cancel\" value=\"Cancel\" class=\"btn btn-secondary\" style=\"width: 100px;\"></input>
            </div>
            </div>
        </form>
    </div>

";
    }

    public function getTemplateName()
    {
        return "admin/customer_add_update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 52,  117 => 47,  108 => 41,  99 => 35,  90 => 29,  81 => 23,  72 => 17,  63 => 11,  53 => 5,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"admin/masterAdmin.html.twig\" %}

{% block table %}
    {% set action = (operation == 'add' ? \"Add\" : \"Update\") %}
    <h2>{{[action, 'Customer'] | join(' ') }}</h2>
    <div class=\"form-responsive\">
        <form method=\"post\" class=\"form form-sm\">                        
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">First Name:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"firstName\" class=\"form-control\" value=\"{{customer.firstName}}\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Last Name:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"lastName\" class=\"form-control\" value=\"{{customer.lastName}}\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Phone Number:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"phoneNo\" class=\"form-control\" value=\"{{customer.phoneNo}}\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Street:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"street\" class=\"form-control\" value=\"{{customer.street}}\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">City:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"city\" class=\"form-control\" value=\"{{customer.city}}\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Province:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"province\" class=\"form-control\" value=\"{{customer.province}}\"></input>
                </div>
            </div>
            <div class=\"form-group row\">
                <label class=\"col-sm-3 col-form-label\">Postal Code:</label>
                <div class=\"col-sm-5\">
                    <input type=\"text\" name=\"postalCode\" class=\"form-control\" value=\"{{customer.postalCode}}\"></input>
                </div>
            </div>                                   
            <div class=\"row\">
            <div class=\"col-sm-8 d-flex justify-content-around\">
                <input type=\"submit\" name=\"submit\" id=\"submitCustomer\" value=\"{{action}}\" class=\"btn btn-secondary\" style=\"width: 100px;\"></input>
                <input type=\"submit\" name=\"cancel\" value=\"Cancel\" class=\"btn btn-secondary\" style=\"width: 100px;\"></input>
            </div>
            </div>
        </form>
    </div>

{% endblock %}", "admin/customer_add_update.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\admin\\customer_add_update.html.twig");
    }
}
