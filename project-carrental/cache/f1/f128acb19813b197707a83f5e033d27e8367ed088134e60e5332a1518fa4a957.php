<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /admin/reservation_singlepage.html.twig */
class __TwigTemplate_15823c32c62f027afd04618c709aa2d9a26566acc70c9f500e7064318ce66085 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["reservationList"] ?? null)) {
            // line 2
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["reservationList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["reservation"]) {
                // line 3
                echo "        <tr>
            <td>";
                // line 4
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "customerId", [], "any", false, false, false, 4), "html", null, true);
                echo "</td>
            <td>";
                // line 5
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "carId", [], "any", false, false, false, 5), "html", null, true);
                echo "</td>
            <td>";
                // line 6
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "startDateTime", [], "any", false, false, false, 6), "html", null, true);
                echo "</td>
            <td>";
                // line 7
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "returnDateTime", [], "any", false, false, false, 7), "html", null, true);
                echo "</td>
            <td>";
                // line 8
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "actualReturnDT", [], "any", false, false, false, 8), "html", null, true);
                echo "</td>
            <td>";
                // line 9
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "pickupLocationName", [], "any", false, false, false, 9), "html", null, true);
                echo "</td>
            <td>";
                // line 10
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "returnLocationName", [], "any", false, false, false, 10), "html", null, true);
                echo "</td>
            <td>";
                // line 11
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "costPerDay", [], "any", false, false, false, 11), "html", null, true);
                echo "</td>
            <td><input type=\"checkbox\" ";
                // line 12
                echo (((twig_get_attribute($this->env, $this->source, $context["reservation"], "insurance", [], "any", false, false, false, 12) == 0)) ? ("") : ("checked"));
                echo " onclick=\"return false\"></td>             
            <td><a href=\"/admin/reservation/update/";
                // line 13
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "id", [], "any", false, false, false, 13), "html", null, true);
                echo "\"><img src=\"/image/edit.png\" width=\"20\", height=\"20\"></a>
                <a href=\"/admin/reservation/delete/";
                // line 14
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "id", [], "any", false, false, false, 14), "html", null, true);
                echo "\" class=\"confirmation\"><img src=\"/image/delete.jpg\" width=\"20\", height=\"20\"></a>
            </td>                         
        </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reservation'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "            
";
        }
        // line 18
        echo "     ";
    }

    public function getTemplateName()
    {
        return "/admin/reservation_singlepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 18,  97 => 17,  87 => 14,  83 => 13,  79 => 12,  75 => 11,  71 => 10,  67 => 9,  63 => 8,  59 => 7,  55 => 6,  51 => 5,  47 => 4,  44 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if reservationList %}
    {% for reservation in reservationList %}
        <tr>
            <td>{{reservation.customerId}}</td>
            <td>{{reservation.carId}}</td>
            <td>{{reservation.startDateTime}}</td>
            <td>{{reservation.returnDateTime}}</td>
            <td>{{reservation.actualReturnDT}}</td>
            <td>{{reservation.pickupLocationName}}</td>
            <td>{{reservation.returnLocationName}}</td>
            <td>{{reservation.costPerDay}}</td>
            <td><input type=\"checkbox\" {{ reservation.insurance == 0 ? '' : 'checked'}} onclick=\"return false\"></td>             
            <td><a href=\"/admin/reservation/update/{{reservation.id}}\"><img src=\"/image/edit.png\" width=\"20\", height=\"20\"></a>
                <a href=\"/admin/reservation/delete/{{reservation.id}}\" class=\"confirmation\"><img src=\"/image/delete.jpg\" width=\"20\", height=\"20\"></a>
            </td>                         
        </tr>
    {% endfor %}            
{% endif %}     ", "/admin/reservation_singlepage.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\admin\\reservation_singlepage.html.twig");
    }
}
