<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /admin/customer_singlepage.html.twig */
class __TwigTemplate_4de6378bbc25249e39609bea4a10645ce497538f68c36ef22e877e6f562d855c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["customerList"] ?? null)) {
            // line 2
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["customerList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["customer"]) {
                // line 3
                echo "        <tr>
            <td>";
                // line 4
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["customer"], "firstName", [], "any", false, false, false, 4), "html", null, true);
                echo "</td>
            <td>";
                // line 5
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["customer"], "lastName", [], "any", false, false, false, 5), "html", null, true);
                echo "</td>
            <td>";
                // line 6
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["customer"], "email", [], "any", false, false, false, 6), "html", null, true);
                echo "</td>
            <td>";
                // line 7
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["customer"], "phoneNo", [], "any", false, false, false, 7), "html", null, true);
                echo "</td>
            <td>";
                // line 8
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["customer"], "street", [], "any", false, false, false, 8), "html", null, true);
                echo "</td>
            <td>";
                // line 9
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["customer"], "city", [], "any", false, false, false, 9), "html", null, true);
                echo "</td>
            <td>";
                // line 10
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["customer"], "province", [], "any", false, false, false, 10), "html", null, true);
                echo "</td>
            <td>";
                // line 11
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["customer"], "postalCode", [], "any", false, false, false, 11), "html", null, true);
                echo "</td>
            <td>";
                // line 12
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["customer"], "createdTS", [], "any", false, false, false, 12), "html", null, true);
                echo "</td>
            <td>";
                // line 13
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["customer"], "userId", [], "any", false, false, false, 13), "html", null, true);
                echo "</td>                        
            <td><a href=\"/admin/customer/update/";
                // line 14
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["customer"], "id", [], "any", false, false, false, 14), "html", null, true);
                echo "\"><img src=\"../../image/edit.png\" width=\"20\", height=\"20\"></a>
                <a href=\"/admin/customer/delete/";
                // line 15
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["customer"], "id", [], "any", false, false, false, 15), "html", null, true);
                echo "\" class=\"confirmation\"><img src=\"../../image/delete.jpg\" width=\"20\", height=\"20\"></a>
            </td>                         
        </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['customer'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 18
            echo "            
";
        }
        // line 19
        echo "     ";
    }

    public function getTemplateName()
    {
        return "/admin/customer_singlepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 19,  101 => 18,  91 => 15,  87 => 14,  83 => 13,  79 => 12,  75 => 11,  71 => 10,  67 => 9,  63 => 8,  59 => 7,  55 => 6,  51 => 5,  47 => 4,  44 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if customerList %}
    {% for customer in customerList %}
        <tr>
            <td>{{customer.firstName}}</td>
            <td>{{customer.lastName}}</td>
            <td>{{customer.email}}</td>
            <td>{{customer.phoneNo}}</td>
            <td>{{customer.street}}</td>
            <td>{{customer.city}}</td>
            <td>{{customer.province}}</td>
            <td>{{customer.postalCode}}</td>
            <td>{{customer.createdTS}}</td>
            <td>{{customer.userId}}</td>                        
            <td><a href=\"/admin/customer/update/{{customer.id}}\"><img src=\"../../image/edit.png\" width=\"20\", height=\"20\"></a>
                <a href=\"/admin/customer/delete/{{customer.id}}\" class=\"confirmation\"><img src=\"../../image/delete.jpg\" width=\"20\", height=\"20\"></a>
            </td>                         
        </tr>
    {% endfor %}            
{% endif %}     ", "/admin/customer_singlepage.html.twig", "E:\\XAMPP\\htdocs\\project-carrental\\templates\\admin\\customer_singlepage.html.twig");
    }
}
