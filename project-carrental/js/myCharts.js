

var lineChartLabel = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul"
];
var lineChartData = [
    50000,
    30005,
    60000,
    45000,
    48094,
    37555,
    44999
];
var ctx1 = document.getElementById("lineChart");
var myLineChart = new Chart(ctx1, {
    type: 'line',
    data: {
    labels: lineChartLabel,
    datasets: [{
        data: lineChartData,
        lineTension: 0,
        backgroundColor: 'transparent',
        borderColor: '#007bff',
        borderWidth: 4,
        pointBackgroundColor: '#007bff'
        }]
    },
    options: {
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero: false
            }
        }]
    },
    legend: {
        display: false
    }
}
});

/*
var pieChartLabel = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul"
];
var pieChartData = [
    50000,
    30005,
    60000,
    45000,
    88094,
    37555,
    44999
];*/

//var pieChartLabel = '<?php echo json_encode($categoryList);?>';
//var pieChartData = '<?php echo json_encode($quantityList);?>';

var pieChartLabel = [];
var elems = document.getElementsByClassName('category');
for (var i = 0, l = elems.length; i < l; i++) {
    pieChartLabel[i] = elems[i].value;
}
var pieChartData = [];
var elems = document.getElementsByClassName('quantity');
for (var i = 0, l = elems.length; i < l; i++) {
    pieChartData[i] = elems[i].value;
}

var ctx2 = document.getElementById("pieChart");
var myPieChart = new Chart(ctx2, {
    type: 'pie',
    data: {
        labels: pieChartLabel,
        datasets: [{
            data: pieChartData,
            backgroundColor: ["pink", "yellow", "skyblue"]
        }]
    }
});


