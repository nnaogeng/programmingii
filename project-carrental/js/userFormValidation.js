
$(document).ready(function() {
    const userName = $('#userName');
    const userNameError = $('#userNameError');
    
    //const userNameError = document.querySelector('#userName + span.error');
    const email = $('#email');        
    const emailError = $('#emailError');
    
    const password = $('#password');
    const passwordError = $('#passwordError');

    const userNameRegExp = /^[a-zA-Z0-9 \\._\'"-]{4,50}$/;
    const emailRegExp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    const passwordRegExp1 = /[A-Z]/;
    const passwordRegExp2 = /[a-z]/;
    const passwordRegExp3 = /[0-9]/;

    $("#submitUser").click(function() {
        
        var userNameErrorMessage = "";
        var emailErrorMessage = "";
        var passwordErrorMessage = "";

        let test = true;
        if (userName.val().length === 0) {
            userNameErrorMessage = "You need to enter a user name.";
            test = false;
        }
        else if (userName.val().length < 4 || userName.val().length > 50) {
            userNameErrorMessage = "User name must be 4 to 50 characters.";
            test = false;
        } 
        else if (!userNameRegExp.test(userName.val())) {
            userNameErrorMessage = "User name must be 4-50 characters in numbers or letters.";
            test = false;
        }

        if (email.val().length === 0) {
            emailErrorMessage = "You need to enter an e-mail address.";
            test = false;
        } 
        else if (email.val().length < 3 || email.val().length > 20) {
            emailErrorMessage = "Email must be 3 to 10.";
            test = false;
        } 
        else if (!emailRegExp.test(email.val())) {
            emailErrorMessage = "bad pattern";
            test = false;
        }

        if (password.val().length === 0) {
            passwordErrorMessage = "You need to enter a password.";
            test = false;
        } 
        else if (password.val().length < 6 || password.val().length > 100) {
            passwordErrorMessage = "Password must be 6 to 100 characters.";
            test = false;
        } 
        else if ( !passwordRegExp1.test(password.val()) 
                || !passwordRegExp2.test(password.val())
                || !passwordRegExp3.test(password.val())) 
        {
            passwordErrorMessage = "bad pattern";
            test = false;
        }

        userNameError.html(userNameErrorMessage);
        emailError.html(emailErrorMessage);
        passwordError.html(passwordErrorMessage);

        //userNameError.className = 'error'; 
        //if(!userName.validity.valid || !email.validity.valide) {            
        if(!test) {                
            event.preventDefault();
            //userName.addClass("redBorder");
            //userName.className += "redBorder";
            // Then we prevent the form from being sent by canceling the event                
        }
    });
})