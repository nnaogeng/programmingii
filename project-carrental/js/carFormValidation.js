
$(document).ready(function() {
    const registrationNo = $('#registrationNo');
    const registrationNoError = $('#registrationNoError');
    
    //const userNameError = document.querySelector('#userName + span.error');
    const make = $('#make');        
    const makeError = $('#makeError');
    
    const model = $('#model');        
    const modelError = $('#modelError');

    const mileage = $('#mileage');        
    const mileageError = $('#mileageError');

    const engineSize = $('#engineSize');        
    const engineSizeError = $('#engineSizeError');

    const productionYear = $('#productionYear');        
    const productionYeaError = $('#productionYeaError');

    const color = $('#color');        
    const colorError = $('#colorError');

    const locationName = $('#locationName');        
    const locationNameError = $('#locationNameError');

    const categoryName = $('#categoryName');        
    const categoryNameError = $('#categoryNameError');

    const registrationNoRegExp = /^[a-zA-Z0-9 \\._\'"-]{4,50}$/;

    $("#submitCar").click(function() {
        
        var registrationNoErrorMessage = "";
        var makeErrorMessage = "";
        var modelErrorMessage = "";
        var mileageErrorMessage = "";
        var engineSizeErrorMessage = "";
        var productionYearErrorMessage = "";
        var colorErrorMessage = "";
        var locationNameErrorMessage = "";
        var categoryNameErrorMessage = "";

        let test = true;
        if (registrationNo.val().length === 0) {
            registrationNoErrorMessage = "You need to enter registration number.";
            test = false;
        }
        else if (registrationNo.val().length < 4 || registrationNo.val().length > 10) {
            registrationNoErrorMessage = "Registration number must be 4 to 10 characters.";
            test = false;
        } 
        else if (!registrationNoRegExp.test(registrationNo.val())) {
            registrationNoErrorMessage = "Registration number must be 4-50 characters in numbers or letters.";
            test = false;
        }

        if (make.val().length === 0) {
            makeErrorMessage = "You need to enter a mark.";
            test = false;
        } 
        else if (make.val().length < 3 || make.val().length > 20) {
            ErrorMessage = " must be 3 to 10.";
            test = false;
        } 

        if (model.val().length === 0) {
            modelErrorMessage = "You need to enter a model.";
            test = false;
        } 
        else if (model.val().length < 3 || model.val().length > 20) {
            modelErrorMessage = "Model must be 3 to 10.";
            test = false;
        } 
        
        if (mileage.val().length === 0) {
            ErrorMessage = "You need to enter mileage .";
            test = false;
        } 
        else if (mileage.val() < 0 || mileage.val() > 2000000) {
            mileageErrorMessage = "Mileage must between 0 to 200000.";
            test = false;
        } 
        
        
        if (engineSize.val().length === 0) {
            engineSizeErrorMessage = "You need to enter engine size .";
            test = false;
        } 
        else if (engineSize.val().length < 3 || engineSize.val().length > 20) {
            engineSizeErrorMessage = " must be 3 to 10.";
            test = false;
        } 
        
    /*    if (productionYear.val().length === 0) {
            productionYearErrorMessage = "You need to enter production year .";
            test = false;
        } 
    */    

        if (color.val().length === 0) {
            colorErrorMessage = "You need to choose a color.";
            test = false;
        } 

        if (locationName.val().length === 0) {
            locationNameErrorMessage = "You need to choose a location.";
            test = false;
        } 

        if (categoryName.val().length === 0) {
            categoryNameErrorMessage = "You need to choose a category.";
            test = false;
        } 

        registrationNoError.html(registrationNoErrorMessage);
        makeError.html(makeErrorMessage);
        modelError.html(modelErrorMessage);
         
        if(!test) {                
            event.preventDefault();               
        }
    });
})