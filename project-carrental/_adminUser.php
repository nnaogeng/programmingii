<?php
require_once '_setup.php';

/*
$app->get('/admin/user/list', function ($request, $response, $args) {
    $get = $request->getQueryParams();
    $pageInfo = getPageInfo("users", $get);    
    return $this->view->render($response, 'admin/user_list.html.twig', [ 'userList' => $pageInfo[0], 'currentPage' => $pageInfo[1], 'totalPages' => $pageInfo[2] ]);    
    //return $this->view->render($response, 'admin/user_list.html.twig');    
});
*/
/*$app->get('/adminUserAdd', function ($request, $response, $args) {                              
    return $this->view->render($response, 'admin/adminUserAdd.html.twig');  
});

$app->post('/adminUserAdd', function ($request, $response, $args) {
    if (isset($_POST['add'])) {      
        $checkboxStuatus = $request->getParam('isAdmin');
        $isAdmin = ($checkboxStuatus == 'on' ? 1 : 0);         
        $userName = $request->getParam('userName');
        $email = $request->getParam('email');
        $password = $request->getParam('password');
                       
        DB::insert("users", ['userName' => $userName, 'email' => $email, 'password' => $password, 'isAdmin' => $isAdmin]);                        
        $userList = DB::query("SELECT * FROM users"); 
        return $this->view->render($response, 'admin/user_listr.html.twig', [ 'userList' => $userList ]);  
    }
    if (isset($_POST['cancel'])) {
        $userList = DB::query("SELECT * FROM users"); 
        return $this->view->render($response, 'admin/user_list.html.twig', [ 'userList' => $userList ]); 
    }
});

$app->get('/adminUserUpdate/{id}', function ($request, $response, $args) {
    $id = $args['id'];
    $user = DB::queryFirstRow("SELECT * FROM users WHERE id = %d LIMIT 1", $id);                        
    return $this->view->render($response, 'admin/adminUserUpdate.html.twig', ['user' => $user]);  
});


$app->post('/adminUserUpdate/{id}', function ($request, $response, $args) {
    if (isset($_POST['update'])) {    
        $id = $args['id'];        
        $checkboxStuatus = $request->getParam('isAdmin');
        $isAdmin = $checkboxStuatus == 'on' ? 1 : 0;
        $userName = $request->getParam('userName');
        $email = $request->getParam('email');
        $password = $request->getParam('password');
        DB::update("users", ['isAdmin' => $isAdmin, 'userName' => $userName, 'email' => $email, 'password' => $password], "id = %d", $id);                        
        $userList = DB::query("SELECT * FROM users"); 
        return $this->view->render($response, 'admin/user_list.html.twig', [ 'userList' => $userList ]);  
    }
    if (isset($_POST['cancel'])) {
        $userList = DB::query("SELECT * FROM users"); 
        return $this->view->render($response, 'admin/user_list.html.twig', [ 'userList' => $userList ]); 
    }
});
*/

$app->get('/admin/user/list[/{pageNo:[0-9]+}]', function ($request, $response, $args) {
    //global $articlesPerPage;
    $pageNo = $args['pageNo'] ?? 1;
    $totalRecords = DB::queryFirstField("SELECT COUNT(*) AS COUNT FROM users");
    $totalPages = ceil($totalRecords / ROWS_PER_PAGE);
    return $this->view->render($response, '/admin/user_list.html.twig', [
            'maxPages' => $totalPages,
            'pageNo' => $pageNo,
        ]);
});

$app->get('/admin/user/list/singlepage/{pageNo:[0-9]+}', function ($request, $response, $args) {
    global $articlesPerPage;
    $pageNo = $args['pageNo'] ?? 1;
    $recordList = DB::query("SELECT * FROM users LIMIT %d OFFSET %d", ROWS_PER_PAGE, ($pageNo - 1) * ROWS_PER_PAGE);    
    return $this->view->render($response, '/admin/user_singlepage.html.twig', ['userList' => $recordList]);
});

$app->get('/admin/user/{operation:add|update}[/{id:[0-9]+}]', function ($request, $response, $args) {
    // either op is add and id is not given OR op is edit and id must be given
    $operation = $args['operation'];
    if (isset($args['id'])) {
        $id = $args['id'];
    }
    if ( ($operation == 'add' && !empty($id)) || ($operation == 'update' && empty($id)) ) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    if ($operation == 'update') {
        $selectedRecord = DB::queryFirstRow("SELECT * FROM users WHERE id = %d LIMIT 1", $id);
        if (!$selectedRecord) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }
    } else {
        $selectedRecord = [];
    }
    return $this->view->render($response, 'admin/user_add_update.html.twig', ['user' => $selectedRecord, 'operation' => $operation]);
});

$app->post('/admin/user/{operation:add|update}[/{id:[0-9]+}]', function ($request, $response, $args) {
    if (isset($_POST['submit'])) {        
        $operation = $args['operation'];

        // either op is add and id is not given OR op is edit and id must be given
        if ( ($operation == 'add' && !empty($args['id'])) || ($operation == 'update' && empty($args['id'])) ) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }

        $checkboxStuatus = $request->getParam('isAdmin');
        $isAdmin = ($checkboxStuatus == 'on' ? 1 : 0);         
        $userName = $request->getParam('userName');
        $email = $request->getParam('email');
        $password = $request->getParam('password');
        $passwordRepeat = $request->getParam('passwordRepeat');

        $errorList = array();

        $result = verifyUserName($userName);
        if ($result != TRUE) { $errorList[] = $result; }

        if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
            array_push($errorList, "Email does not look valid!");
            $email = "";
        } else {
            // is email already in use BY ANOTHER ACCOUNT???
            if ($operation == 'update') {
                $record = DB::queryFirstRow("SELECT * FROM users WHERE email=%s AND id != %d", $email, $args['id'] );
            } else { // add has no id yet
                $record = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
            }
            if ($record) {
                array_push($errorList, "This email is already registered.");
                $email = "";
            }
        }
        // verify password always on add, and on edit/update only if it was given
        if ($operation == 'add' || $password != '') {
            $result = verifyPasswordQuailty($password, $passwordRepeat);
            if (!$result) { 
                $errorList[] = $result; 
            }
        }
        
        if ($errorList) {
            return $this->view->render($response, 'admin/user_add_update.html.twig',
                    [ 'errorList' => $errorList, 'user' => ['userName' => $userName, 'email' => $email ]  ]);
        } else {
            if ($operation == 'add') {
                DB::insert("users", ['userName' => $userName, 'email' => $email, 'password' => $password, 'isAdmin' => $isAdmin]);                                            
                return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => $operation , 'object' => "user"]);
            } else {
                $data = ['userName' => $userName, 'email' => $email, 'isAdmin' => $isAdmin];
                if ($password != '') { // only update the password if it was provided
                    $data['password'] = $password;
                }
                DB::update('users', $data, "id = %d", $args['id']);
                return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => $operation , 'object' => "user"]);
            }
        }
    }
    if (isset($_POST['cancel'])) {        
        return $response->withRedirect("/admin/user/list");
    }
});

// STATE 1: first display
$app->get('/admin/user/delete/{id:[0-9]+}', function ($request, $response, $args) {
    $selectedRecord = DB::queryFirstRow("SELECT * FROM users WHERE id = %d", $args['id']);
    if (!$selectedRecord) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    return $this->view->render($response, 'admin/user_delete.html.twig', ['user' => $selectedRecord] );
});


$app->post('/admin/user/delete/{id:[0-9]+}', function ($request, $response, $args) {
    DB::delete('users', "id = %d", $args['id']);
    return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => "delete" , 'object' => "user"]);
});


function verifyUserName($name) {
    if (preg_match('/^[a-zA-Z0-9\ \\._\'"-]{4,50}$/', $name) != 1) { // no match
        return "Name must be 4-50 characters long and consist of letters, digits, "
            . "spaces, dots, underscores, apostrophies, or minus sign.";
    }
    return TRUE;
}

function verifyPasswordQuailty($pass1, $pass2) {
    if ($pass1 != $pass2) {
        return "Passwords do not match";
    } else {
        if ((strlen($pass1) < 6) || (strlen($pass1) > 100)
                || (preg_match("/[A-Z]/", $pass1) == FALSE )
                || (preg_match("/[a-z]/", $pass1) == FALSE )
                || (preg_match("/[0-9]/", $pass1) == FALSE )) {
            return "Password must be 6-100 characters long, "
                . "with at least one uppercase, one lowercase, and one digit in it";
        }
    }
    return TRUE;
}
