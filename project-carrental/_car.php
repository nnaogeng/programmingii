<?php
require_once '_setup.php';
/*
$app->get('/admin/car/list', function ($request, $response, $args) {
    //$carList = DB::query("SELECT c.*, l.locationName, cat.categoryName FROM cars AS c, locations AS l, categories AS cat WHERE c.defaultLocationId = l.id AND c.categoryId = cat.id");                        
    $get = $request->getQueryParams();
    $pageInfo = getPageInfo("cars", $get);
    return $this->view->render($response, 'admin/car_list.html.twig', [ 'carList' => $pageInfo[0], 'currentPage' => $pageInfo[1], 'totalPages' => $pageInfo[2] ]);    
});
*/
$app->get('/admin/car/list[/{pageNo:[0-9]+}]', function ($request, $response, $args) {
    $pageNo = $args['pageNo'] ?? 1;
    $totalRecords = DB::queryFirstField("SELECT COUNT(*) AS COUNT FROM cars");
    $totalPages = ceil($totalRecords / ROWS_PER_PAGE);
    return $this->view->render($response, '/admin/car_list.html.twig', ['maxPages' => $totalPages, 'pageNo' => $pageNo ]);
});

$app->get('/admin/car/list/singlepage/{pageNo:[0-9]+}', function ($request, $response, $args) {
    $pageNo = $args['pageNo'] ?? 1;
    $recordList = DB::query("SELECT c.*, l.locationName, cat.categoryName FROM cars AS c, locations AS l, categories AS cat WHERE c.defaultLocationId = l.id AND c.categoryId = cat.id LIMIT %d OFFSET %d", ROWS_PER_PAGE, ($pageNo - 1) * ROWS_PER_PAGE);
    return $this->view->render($response, '/admin/car_singlepage.html.twig', ['carList' => $recordList]);
});

$app->get('/admin/car/{operation:add|update}[/{id:[0-9]+}]', function ($request, $response, $args) { 
    $operation = $args['operation'];
    
    if ( ($operation == 'add' && !empty($args['id'])) || ($operation == 'update' && empty($args['id'])) ) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }

    $locationList = DB::query("SELECT * from locations"); 
    $categoryList = DB::query("SELECT id, categoryName from categories");

    if ($operation == 'update') {
        $selectedRecord = DB::queryFirstRow("SELECT c.*, l.street, l.city, cat.categoryName FROM cars AS c, locations AS l, categories AS cat WHERE c.defaultLocationId = l.id AND c.categoryId = cat.id AND c.id = %d", $args['id']);
        if (!$selectedRecord) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }
    } else {
        $selectedRecord = [];                             
    }
    //print_r($selectedRecord);
    return $this->view->render($response, 'admin/car_add_update.html.twig',
        ['car' => $selectedRecord, 'operation' => $operation, 'locationList' => $locationList, 'categoryList' => $categoryList]);  
});

$app->post('/admin/car/{operation:add|update}[/{id:[0-9]+}]', function ($request, $response, $args) { 

    if (isset($_POST['submit'])) {        
        $operation = $args['operation'];

        if ( ($operation == 'add' && !empty($args['id'])) || ($operation == 'update' && empty($args['id'])) ) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }            

        $registrationNo = $request->getParam('registrationNo');
        $make = $request->getParam('make');
        $model = $request->getParam('model');
        $mileage = $request->getParam('mileage'); 
        $engineSize = $request->getParam('engineSize');
        $productionYear = $request->getParam('productionYear');
        $color = $request->getParam('color');
        $locationId = $request->getParam('locationName'); 
        $categoryId = $request->getParam('categoryName');  
        
        //print_r($locationId);
        //print_r($categoryId);

        $car['registrationNo'] = $registrationNo;
        $car['make'] = $make;
        $car['model'] = $model;
        $car['mileage'] = $mileage; 
        $car['engineSize'] = $engineSize;
        $car['productionYear'] = $productionYear;
        $car['color'] = $color;
        $car['defaultLocationId'] = $locationId; 
        $car['categoryId'] = $categoryId;

        //    print_r($car);
        $errorList = array();
        // ADD VALIDATION LATER

        if ($errorList) {
            return $this->view->render($response, 'admin/car_add_update.html.twig',
                    [ 'errorList' => $errorList, 'car' => $car]);
        }
        else {
        
            //$locationId = DB::queryFirstRow("SELECT id FROM locations WHERE street = %s AND city = %s", $location[0], $location[1]);
            //$categoryId = DB::queryFirstRow("SELECT id FROM categories WHERE categoryName = %s LIMIT 1", $categoryName);

            if ($operation == 'add') {
                //DB::insert("cars", ['registrationNo' => $registrationNo, 'make' => $make, 'model' => $model, 'mileage' => $mileage, 'engineSize' => $engineSize, 'productionYear' => $productionYear, 'color' => $color, 'categoryId' => $categoryId['id'], 'defaultLocationId' => $locationId['id']]);                        
                DB::insert("cars", $car);
                return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => $operation, 'object' => "car" ]);
            }
            else {
                DB::update('cars', $car , "id = %d", $args['id']);
                return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => $operation, 'object' => "car" ]); 
            }
        }
    }
    if (isset($_POST['cancel'])) {        
        return $response->withRedirect("/admin/car/list");
    }            
});

$app->get('/admin/car/delete/{id:[0-9]+}', function ($request, $response, $args) {
    $selectedRecord = DB::queryFirstRow("SELECT * FROM cars WHERE id = %d", $args['id']);
    if (!$selectedRecord) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    return $this->view->render($response, 'admin/car_delete.html.twig', ['car' => $selectedRecord] );
});

$app->post('/admin/car/delete/{id:[0-9]+}', function ($request, $response, $args) {
    DB::delete('users', "id = %d", $args['id']);
    return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => "delete", 'object' => "car"]);
});
