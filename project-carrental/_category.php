<?php
require_once '_setup.php';
/*
$app->get('/admin/category/list', function ($request, $response, $args) {
    //$categoryList = DB::query("SELECT * FROM categories");                        
    $get = $request->getQueryParams();
    $pageInfo = getPageInfo("categories", $get);
    return $this->view->render($response, '/admin/category_list.html.twig', [ 'categoryList' => $pageInfo[0], 'currentPage' => $pageInfo[1], 'totalPages' => $pageInfo[2] ]);    
});
*/

$app->get('/admin/category/list[/{pageNo:[0-9]+}]', function ($request, $response, $args) {
    $pageNo = $args['pageNo'] ?? 1;
    $totalRecords = DB::queryFirstField("SELECT COUNT(*) AS COUNT FROM categories");
    $totalPages = ceil($totalRecords / ROWS_PER_PAGE);
    return $this->view->render($response, '/admin/category_list.html.twig', [
            'maxPages' => $totalPages,
            'pageNo' => $pageNo,
        ]);
});

$app->get('/admin/category/list/singlepage/{pageNo:[0-9]+}', function ($request, $response, $args) {
    $pageNo = $args['pageNo'] ?? 1;
    $recordList = DB::query("SELECT * FROM categories LIMIT %d OFFSET %d", ROWS_PER_PAGE, ($pageNo - 1) * ROWS_PER_PAGE);
    return $this->view->render($response, '/admin/category_singlepage.html.twig', ['categoryList' => $recordList]);
});

$app->get('/admin/category/{operation:add|update}[/{id:[0-9]+}]', function ($request, $response, $args) {
    // either op is add and id is not given OR op is edit and id must be given
    echo("in get");
    $operation = $args['operation'];
    if (isset($args['id'])) {
        $id = $args['id'];
    }
    if ( ($operation == 'add' && !empty($id)) || ($operation == 'update' && empty($id)) ) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    if ($operation == 'update') {
        $selectedRecord = DB::queryFirstRow("SELECT * FROM categories WHERE id = %d LIMIT 1", $id);
        if (!$selectedRecord) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }
    } else {
        $selectedRecord = [];
    }
    return $this->view->render($response, 'admin/category_add_update.html.twig', ['category' => $selectedRecord, 'operation' => $operation]);
});

$app->post('/admin/category/{operation:add|update}[/{id:[0-9]+}]', function ($request, $response, $args) {    

    if (isset($_POST['submit'])) {        
        $operation = $args['operation'];

        if ( ($operation == 'add' && !empty($args['id'])) || ($operation == 'update' && empty($args['id'])) ) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }            
            
        $categoryName = $request->getParam('categoryName');
        $passengersNo = $request->getParam('passengersNo');
        $doorsNo = $request->getParam('doorsNo');
        $costPerDay = $request->getParam('costPerDay'); 

        $category['categoryName'] = $categoryName;
        $category['passengersNo'] = $passengersNo;
        $category['doorsNo'] = $doorsNo;
        $category['costPerDay'] = $costPerDay;

        $errorList = array();
        // ADD VALIDATION LATER

        if ($errorList) {
            return $this->view->render($response, 'admin/category_add_update.html.twig',
                    [ 'errorList' => $errorList, 'category' => $category]);
        }
        else {
            if ($operation == 'add') {
                DB::insert("categories", ['categoryName' => $categoryName, 'passengersNo' => $passengersNo, 'doorsNo' => $doorsNo, 'costPerDay' => $costPerDay]);                        
                return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => $operation, 'object' => "category" ]);
            }
            else {            
                DB::update("categories", ['categoryName' => $categoryName, 'passengersNo' => $passengersNo, 'doorsNo' => $doorsNo, 'costPerDay' => $costPerDay], "id = %d", $args['id']);                                    
                return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => $operation, 'object' => "category" ]); 
            }
        } 
    }
    if (isset($_POST['cancel'])) {        
        return $response->withRedirect("/admin/category/list");
    }       
});

$app->get('/admin/category/delete/{id:[0-9]+}', function ($request, $response, $args) {
    $selectedRecord = DB::queryFirstRow("SELECT * FROM categories WHERE id = %d", $args['id']);
    if (!$selectedRecord) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    return $this->view->render($response, 'admin/category_delete.html.twig', ['category' => $selectedRecord] );
});

$app->post('/admin/category/delete/{id:[0-9]+}', function ($request, $response, $args) {
    DB::delete('categories', "id = %d", $args['id']);
    return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => "delete", 'object' => "category"]);
});